<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ceco extends Model
{
    protected $table = "cecos";
    protected $fillable = ['name'];

	public function overtimes()
    {
        return $this->hasMany('App\Overtime');
    }

}

