<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Codenomine extends Model
{
    protected $table = "codenomines";
    protected $fillable = ['name'];

    public function overtimes()
    {
        return $this->hasMany('App\Overtime');
    }

}
