<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table = "locations";
    protected $fillable = ['name'];

	public function user()
    {
    	return $this->hasMany('App\User');
    }
    
    public function overtimes()
    {
        return $this->hasMany('App\Overtime');
    }

    public function permises()
    {
        return $this->hasMany('App\Permise');
    }

}
