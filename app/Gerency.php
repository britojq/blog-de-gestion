<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gerency extends Model
{
    protected $table = "gerencys";
    protected $fillable = ['name'];

	public function user()
    {
    	return $this->hasMany('App\User');
    }

    public function overtimes()
    {
        return $this->hasMany('App\Overtime');
    }

}
