<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    protected $table = "divisions";
    protected $fillable = ['name'];

	public function user()
    {
    	return $this->hasMany('App\User');
    }

    public function overtimes()
    {
        return $this->hasMany('App\Overtime');
    }

    public function vacations()
    {
        return $this->hasMany('App\Vacation');
    }
    
}
