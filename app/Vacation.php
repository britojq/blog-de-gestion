<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vacation extends Model
{
	protected $table = "vacations";
    protected $fillable = ['solicitdate', 'user_id', 'unit_id', 'division_id', 'zone', 'typenomine_id', 'option', 'start_dateA', 'end_dateA', 'start_dateB', 'end_dateB', 'start_dateC1', 'end_dateC1', 'start_dateC2', 'end_dateC2', 'observ_just'];

    public function users()
    {
    	return $this->hasMany('App\User');
    }

    public function units()
    {
    	return $this->belongsTo('App\Unit');
    }

    public function divisions()
    {
    	return $this->belongsTo('App\Division');
    }

    public function typenomines()
    {
    	return $this->belongsTo('App\Typenomine');
    }

    

}
