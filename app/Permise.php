<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permise extends Model
{
	protected $table = "permises";
    protected $fillable = ['location_id', 'datesolicit', 'user_id', 'carge_id','unit_id', 'unitcode_id', 'permisemotive_id','observmotiv', 'start_date', 'start_hour', 'end_date', 'end_hour', 'permisetype_id'];

    public function location()
    {
        return $this->belongsTo('App\Location');
    }

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function carge()
    {
        return $this->belongsTo('App\Carge');
    }

    public function unit()
    {
        return $this->belongsTo('App\Unit');
    }

    public function unitcode()
    {
        return $this->belongsTo('App\Unitcode');
    }

    public function permisemotive()
    {
        return $this->belongsTo('App\Permisemotive');
    }

    public function permisetype()
    {
        return $this->belongsTo('App\Permisetype');
    }
    //
}
