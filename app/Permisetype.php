<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permisetype extends Model
{
    protected $table = "permisetypes";
    protected $fillable = ['name'];


    public function permises()
    {
        return $this->hasMany('App\Permise');
    }
    
}
