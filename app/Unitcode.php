<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unitcode extends Model
{
    protected $table = "unitcodes";
    protected $fillable = ['name'];

	public function users()
    {
    	return $this->hasMany('App\User');
    }

    public function permises()
    {
        return $this->hasMany('App\Permise');
    }

}
