<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Overtime extends Model
{
	protected $table = "overtimes";
    protected $fillable = ['location_id', 'currentdate', 'ceco_id', 'gerency_id','unit_id', 'division_id', 'departament_id','nominearea_id', 'start_date1', 'end_date1', 'user_id', 'dateofwork', 'dayofweek', 'start_hour', 'end_hour', 'totalhours', 'codenomine_id', 'justification', 'loadonsap', 'aprove'];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
    
    public function location()
    {
        return $this->belongsTo('App\Location');
    }

    public function ceco()
    {
    	return $this->belongsTo('App\Ceco');
    }

    public function gerency()
    {
    	return $this->belongsTo('App\Gerency');
    }

    public function unit()
    {
    	return $this->belongsTo('App\Unit');
    }

    public function division()
    {
    	return $this->belongsTo('App\Division');
    }

    public function departament()
    {
    	return $this->belongsTo('App\Departament');
    }

    public function typenomine()
    {
    	return $this->belongsTo('App\Typenomine');
    }

    public function codenomine()
    {
    	return $this->belongsTo('App\Codenomine');
    }

    public function nominearea()
    {
        return $this->belongsTo('App\Nominearea');
    }
}
