<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nominearea extends Model
{
    
    protected $table = "nomineareas";
    protected $fillable = ['name'];

	public function users()
    {
    	return $this->hasMany('App\User');
    }
    
    public function overtimes()
    {
        return $this->hasMany('App\Overtime');
    }

}//
