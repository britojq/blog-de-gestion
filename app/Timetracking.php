<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Timetracking extends Model
{
	protected $table = "timetracking";
    protected $fillable = ['user_id', 'current_date', 'time_in', 'check_in', 'time_out', 'check_out', 'ipaddress', 'hostname'];

   public function user()
    {
    	return $this->belongsTo('App\User');
    }
    
    //
}
