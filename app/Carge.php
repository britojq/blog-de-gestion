<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carge extends Model
{
    protected $table = "carges";
    protected $fillable = ['name'];

	public function users()
    {
    	return $this->hasMany('App\User');
    }

    public function permises()
    {
        return $this->hasMany('App\Permise');
    }
    
}
