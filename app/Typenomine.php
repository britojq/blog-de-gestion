<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Typenomine extends Model
{
    protected $table = "typenomines";
    protected $fillable = ['name'];

	public function users()
    {
    	return $this->hasMany('App\User');
    }

    public function overtimes()
    {
        return $this->hasMany('App\Overtime');
    }

    public function vacations()
    {
        return $this->hasMany('App\Vacation');
    }
    
}
