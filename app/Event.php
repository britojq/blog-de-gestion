<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model

{
	protected $table = "events";
    protected $fillable = ['type','title','user_id','guard_id','permise_id','vacation_id','start_date','end_date'];

	public function user()
    {
    	return $this->belongsTo('App\User');
    }
  
    public function articles()
    {
        return $this->BelongsToMany('App\Guard');
    }
/*
    public function guards()
    {
        return $this->hasMany('App\Guard');
    }
    */
}