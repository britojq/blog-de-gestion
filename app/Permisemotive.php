<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permisemotive extends Model
{
    protected $table = "permisemotives";
    protected $fillable = ['name'];

    public function permises()
    {
        return $this->hasMany('App\Permise');
    }

}
