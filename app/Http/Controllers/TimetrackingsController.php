<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;
use Laracast\Flash\Flash;
use Illuminate\Support\Facades\Redirect;
//use App\Http\Requests\TypenominesRequest;
use Auth;
use App\Timetracking;
use App\User;

class TimetrackingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
        {
            Carbon::setlocale('es');    
        }

    public function index()
    {
        $users = User::orderBy('name', 'ASC')->pluck('name', 'id');
        $timetrackings = Timetracking::orderBy('id','ASC')->paginate(6);
        $timetrackings->each(function($timetrackings){
                $timetrackings->user;
        });

            return view('admin.timetrackings.index')->with('timetrackings', $timetrackings);



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $users = User::orderBy('name', 'ASC')->pluck('name', 'id');

        return view('admin.timetrackings.create')->with('users', $users);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $timetracking = new Timetracking($request->all());
            $timetracking->user_id = \Auth::user()->id;
            $timetracking->current_date = Carbon::createFromFormat('d/m/Y', $request->current_date)->toDateString();
            $timetracking->time_in = Carbon::parse($request->time_in);
            $timetracking->time_out = Carbon::parse($request->time_out);
            $timetracking->save();

            flash('Asistencia ' . $timetracking->id . ' ha sido Agregada Correctamente')->success();

            return redirect('admin/timetrackings');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
            $users = User::orderBy('name', 'ASC')->pluck('name', 'id');
            $timetrackings = Timetracking::find($id);
            $timetrackings->users;
            return view('admin.timetrackings.edit')->with('timetrackings', $timetrackings)->with('users', $users);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
            $timetracking = Timetracking::find($id);
            $timetracking->fill($request->all());
            $timetracking->user_id = \Auth::user()->id;
            $timetracking->current_date = Carbon::createFromFormat('d/m/Y', $request->current_date)->toDateString();
            $timetracking->time_in = Carbon::parse($request->time_in);
            $timetracking->time_out = Carbon::parse($request->time_out);
            $timetracking->save();
            flash('Asistencia ' . $timetracking->id . ' ha sido Editado Correctamente')->warning();
            return redirect('admin/timetrackings');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $timetracking = Timetracking::find($id);
        $timetracking -> delete();
        flash('Horario  ' . $timetracking->id . ' Borrado Correctamente')->warning();
        return redirect('admin/timetrackings');
    }
}
