<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Laracast\Flash\Flash;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\CargeRequest;
use Auth;
use App\Carge;
use App\User;


class CargesController extends Controller
{
    
public function index()
{
	$carges = Carge::orderBy('id','DESC')->paginate(5);
	return view('admin.carges.index')->with('carges', $carges);
}

public function store(CargeRequest $request)
{
	$carge = new carge($request->all());
	$carge->save();
	flash('El Cargo ' . $carge->name . ' fue agregado correctamente');
	return redirect('admin/carges');
}


public function create()
{
	return view('admin.carges.create');
}


public function show($id)
{
	//
}


public function destroy($id)
{
	$carge = carge::find($id);
	$carge->delete();
	flash('La Categoria ' . $carge->name . ' ha sido Borrada Correctamente')->warning();
	return redirect('admin/carges');
}

public function edit($id)
{
	$carge = carge::find($id);
	return view('admin.carges.edit')->with('carge', $carge);
}

public function update(Request $request, $id)
    {
        $carge = carge::find($id);
        $carge->fill($request->all());
        $carge->save();
        flash('El Cargo ' . $carge->name . ' ha sido Editado Correctamente')->warning();
        return redirect('admin/carges');
    }


}
