<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Laracast\Flash\Flash;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\PermisetypesRequest;
use Auth;
use App\Permise;
use App\Permisetype;
use App\User;

class PermisetypesController extends Controller
{
    

     public function index()
		{
			$permisetypes = Permisetype::orderBy('id','DESC')->paginate(5);
			return view('admin.permisetypes.index')->with('permisetypes', $permisetypes);
		}

	public function store(PermisetypesRequest $request)
		{
			$permisetype = new permisetype($request->all());
			$permisetype->save();
			flash('La Ubicacion ' . $permisetype->name . ' fue agregada correctamente');
			return redirect('admin/permisetypes');
		}


	public function create()
		{
			return view('admin.permisetypes.create');
		}


	public function show($id)
		{
			//
		}


	public function destroy($id)
		{
			$permisetype = Permisetype::find($id);
			$permisetype->delete();
			flash('La Ubicacion ' . $permisetype->name . ' ha sido Borrada Correctamente')->warning();
			return redirect('admin/permisetypes');
		}

	public function edit($id)
		{
			$permisetype = Permisetype::find($id);
			return view('admin.permisetypes.edit')->with('permisetype', $permisetype);
		}

	public function update(Request $request, $id)
	    {
	        $permisetype = Permisetype::find($id);
	        $permisetype->fill($request->all());
	        $permisetype->save();
	        flash('La Ubicacion ' . $permisetype->name . ' ha sido Editada Correctamente')->warning();
	        return redirect('admin/permisetypes');
	    }



}
