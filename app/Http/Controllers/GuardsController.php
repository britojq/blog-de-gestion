<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Laracast\Flash\Flash;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\GuardsRequest;
use Calendar;

use Auth;
use App\User;
use App\Guard;
use App\Event;


class GuardsController extends Controller
{
    public function __construct()
		{
			Carbon::setlocale('es');	
		}

    public function index()
		{
			$guards = Guard::orderBy('id','DESC')->paginate(5);
			return view('admin.guards.index')->with('guards', $guards);
		}

	public function store(Request $request)
		{
			
			
			//dd($request);

			if($request->users_id1 == $request->users_id2) 
		    {
			    // AGREGA LA GUARDIA A LA BD
			    $guard = new Guard($request->all());
			    $guard->user_id = $request->users_id1;
				$guard->from_date = Carbon::createFromFormat('d/m/Y', $request->from_date)->toDateString();
				$guard->to_date = Carbon::createFromFormat('d/m/Y', $request->to_date)->toDateString();
				$guard->save();

				//AGREGA A LA TABLA DE EVENTOS PARA SER MOSTRADA EN CALENDARIO
				
				$event = new Event();
				$event->type = 'guards';
				$event->title = 'Guardia';
				$event->user_id = $request->users_id1;
				
				//$event->guard()->associate($guard);

				$event->start_date = Carbon::createFromFormat('d/m/Y', $request->from_date)->toDateString();
				$event->end_date = Carbon::createFromFormat('d/m/Y', $request->to_date)->toDateString();
				
				$event->save();

				// INDICA MENSAJE DE GUARDADO
				flash('La Guardia se ha creado de forma satisfactoria')->success();
	        	return redirect('admin/guards');
		    }
		    else
		    {
		        flash('Por favor Verificar, Nombre y Apellido, NO COINCIDEN')->warning();
        		return redirect('admin/guards/create');
		    }

		}


	public function create()
		{
			$users = User::orderBy('name', 'ASC')->pluck('name', 'id');
			$users2 = User::orderBy('name', 'ASC')->pluck('lastname', 'id');

			return view('admin.guards.create')->with('users', $users)->with('users2', $users2);

		}


	public function show($id)
		{
			//
		}


	public function destroy($id)
		{
			$guard = Guard::find($id);
			$guard->delete();

			flash('La Guardia ' . $guard->name . ' ha sido Borrada Correctamente')->warning();

			return redirect('admin/guards');
		}

	public function edit($id)
		{
			$users = User::orderBy('name', 'ASC')->pluck('name', 'id');
			$guard = guard::find($id);
			$guard ->user;

			return view('admin.guards.edit')->with('guard', $guard)->with('users', $users);

			//return view('admin.guards.edit')->with('guard', $guard);
			
		}

	public function update(Request $request, $id)
	    {
	        $guard = guard::find($id);
	        $guard->fill($request->all());
	        //dd($guard);
	        
	        $guard->from_date = Carbon::createFromFormat('d/m/Y', $request->from_date)->toDateString();
			$guard->to_date = Carbon::createFromFormat('d/m/Y', $request->to_date)->toDateString();
	        $guard->save();


			//AGREGA A LA TABLA DE EVENTOS LA NUEVA FECHA PARA SER MOSTRADA EN CALENDARIO

			$events = new Event();
			$events->type = 'guards';
			$events->title = 'Guardia';
	        $events->user_id = $guard->user_id;
			$events->start_date = Carbon::createFromFormat('d/m/Y',$request->from_date)->toDateString();
			$events->end_date = Carbon::createFromFormat('d/m/Y', $request->to_date)->toDateString();
				
			$events->save();
	        


	        flash('La Guardia ' . $guard->name . ' ha sido Editada Correctamente')->warning();
	        return redirect('admin/guards');
	    }


}
