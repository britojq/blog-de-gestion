<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Calendar;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;

use App\Event;
use App\User;
use App\Guard;

class EventController extends Controller

{

      public function index()
          {
              $events = [];
              $data = Event::all();
              if($data->count()) {
                  foreach ($data as $key => $value) {
                      $events[] = Calendar::event(
                          $value->type,
                          true,
                          new \DateTime($value->start_date),
                          new \DateTime($value->end_date.' +1 day'),
                          null,
                          // Add color and link on event
                        [
                            'color' => '#f05050',
                            //'url' => 'events',
                            
                        ]
                      );
                      

                  }
              }
              $calendar = Calendar::addEvents($events);
              return view('mycalender', compact('calendar'));
          }
}