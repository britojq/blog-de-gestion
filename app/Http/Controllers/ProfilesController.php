<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

use Carbon\Carbon;
use App\User;
use App\Carge;
use App\Typenomine;
use App\Location;
use App\Gerency;
use App\Division;
use App\Departament;
use App\Unit;
use App\Unitcode;

use Laracast\Flash\Flash;
use App\Http\Requests\PersoRequest;

class ProfilesController extends Controller
{
    
    public function __construct()
        {
            Carbon::setlocale('es');    
        }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $users = User::Search($request->uid)->orderBy('id','DESC')->paginate(5);
            $users = User::orderBy('id','ASC')->paginate(6);
            $users->each(function($users){
                $users->carge;
                $users->typenomine;
                $users->location;
                $users->gerency;
                $users->division;
                $users->departament;
                $users->unit;
                $users->unitcode;
            });
            //dd($users);
            return view('admin.profiles.index')->with('users', $users);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $carges = Carge::orderBy('name', 'ASC')->pluck('name', 'id');
        $typenomines = Typenomine::orderBy('name', 'ASC')->pluck('name', 'id');
        $locations = Location::orderBy('name', 'ASC')->pluck('name', 'id');
        $gerencys = Gerency::orderBy('name', 'ASC')->pluck('name', 'id');
        $divisions = Division::orderBy('name', 'ASC')->pluck('name', 'id');
        $departements = Departament::orderBy('name', 'ASC')->pluck('name', 'id');
        $units = Unit::orderBy('name', 'ASC')->pluck('name', 'id');
        $unitcodes = Unitcode::orderBy('name', 'ASC')->pluck('name', 'id');

        $users = User::find($id);
        $users->each(function($users){
            $users->carge;
            $users->typenomine;
            $users->location;
            $users->gerency;
            $users->division;
            $users->departament;
            $users->unit;
            $users->unitcode;
        });

            // convertir la fecha de ingreso a un formato legible al usuario
            $now = Carbon::parse($users->dateingres);
            $date = $now->format('d/m/Y');

            return view('admin.profiles.show')->with('users', $users)->with('date', $date);



    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $carges = Carge::orderBy('name', 'ASC')->pluck('name', 'id');
            $typenomines = Typenomine::orderBy('name', 'ASC')->pluck('name', 'id');
            $locations = Location::orderBy('name', 'ASC')->pluck('name', 'id');
            $gerencys = Gerency::orderBy('name', 'ASC')->pluck('name', 'id');
            $divisions = Division::orderBy('name', 'ASC')->pluck('name', 'id');
            $departaments = Departament::orderBy('name', 'ASC')->pluck('name', 'id');
            $units = Unit::orderBy('name', 'ASC')->pluck('name', 'id');
            $unitcodes = Unitcode::orderBy('name', 'ASC')->pluck('name', 'id');
            
            $users = User::find($id);
            $users->carge;
            $users->typenomine;
            $users->location;
            $users->gerency;
            $users->division;
            $users->departament;
            $users->unit;
            $users->unitcode;

            return view('admin.profiles.edit')->with('users', $users)->with('carges', $carges)->with('typenomines', $typenomines)->with('locations', $locations)->with('gerencys', $gerencys)->with('divisions', $divisions)->with('departaments', $departaments)->with('units', $units)->with('unitcodes', $unitcodes);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->fill($request->all());
        $user->password = bcrypt($request->password);
        $user->save();
        flash('Usuario ' . $user->name . ' ha sido Editado Correctamente')->warning();
        return redirect('admin/profiles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
