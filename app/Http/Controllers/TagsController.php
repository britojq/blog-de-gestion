<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;
use Laracast\Flash\Flash;
use App\Http\Requests\TagRequest;

class TagsController extends Controller
{
    //

public function index(Request $request)
{
	$tags = Tag::search($request->name)->orderBy('id','DESC')->paginate(5);
	return view('admin.tags.index')->with('tags', $tags);
}

public function store(TagRequest $request)
{
	$tag = new tag($request->all());
	$tag->save();
	flash('El tag ' . $tag->name . ' fue creada Correctamente');
	return redirect('admin/tags');
}


public function create()
{
	return view('admin.tags.create');
}


public function show($id)
{
	//
}


public function destroy($id)
{
	$tag = Tag::find($id);
	$tag->delete();
	flash('El Tag ' . $tag->name . ' ha sido Borrada Correctamente')->warning();
	return redirect('admin/tags');
}

public function edit($id)
{
	$tag = Tag::find($id);
	return view('admin.tags.edit')->with('tag', $tag);
}

public function update(Request $request, $id)
    {
       $tag = Tag::find($id);
       $tag->fill($request->all());
       $tag->save();
       flash('El tag ' . $tag->name . ' ha sido Editado Correctamente')->warning();
        return redirect('admin/tags');
    }









}
