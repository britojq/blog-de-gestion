<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use Laracast\Flash\Flash;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\NomineareasRequest;
use Auth;
use App\Nominearea;
use App\User;
use App\Overtime;


class NomineareasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $nomineareas = Nominearea::orderBy('id','DESC')->paginate(5);
            return view('admin.nomineareas.index')->with('nomineareas', $nomineareas);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.nomineareas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NomineareasRequest $request)
    {
            $nominearea = new Nominearea($request->all());
            $nominearea->save();
            flash('La Ubicacion ' . $nominearea->name . ' fue agregada correctamente');
            return redirect('admin/nomineareas');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $nominearea = Nominearea::find($id);
        return view('admin.nomineareas.edit')->with('nominearea', $nominearea);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $nominearea = Nominearea::find($id);
            $nominearea->fill($request->all());
            $nominearea->save();
            flash('El Area de Nomina ' . $nominearea->name . ' ha sido Editada Correctamente')->warning();
            return redirect('admin/nomineareas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            $nominearea = Nominearea::find($id);
            $nominearea->delete();
            flash('El Area de nomina ' . $nominearea->name . ' ha sido Borrada Correctamente')->warning();
            return redirect('admin/nomineareas');
    }
}
