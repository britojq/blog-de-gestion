<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use App\Category;
use Laracast\Flash\Flash;
use App\Http\Requests\CategoRequest;

class CategoriesController extends Controller
{
 
     
public function index()
{
	$categories = Category::orderBy('id','DESC')->paginate(5);
	return view('admin.categories.index')->with('categories', $categories);
}

public function store(CategoRequest $request)
{
	$category = new Category($request->all());
	$category->save();
	flash('La Categoria ' . $category->name . ' fue creada Correctamente');
	return redirect('admin/categories');
}


public function create()
{
	return view('admin.categories.create');
}


public function show($id)
{
	//
}


public function destroy($id)
{
	$category = Category::find($id);
	$category->delete();
	flash('La Categoria ' . $category->name . ' ha sido Borrada Correctamente')->warning();
	return redirect('admin/categories');
}

public function edit($id)
{
	$category = Category::find($id);
	return view('admin.categories.edit')->with('category', $category);
}

public function update(Request $request, $id)
    {
        $category = Category::find($id);
        $category->fill($request->all());
        $category->save();
        flash('La Categoria ' . $category->name . ' ha sido Editada Correctamente')->warning();
        return redirect('admin/categories');
    }

}
