<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;
use App\Permisetype;
use App\Permisemotive;
use App\User;
use App\Carge;
use App\Typenomine;
use App\Location;
use App\Gerency;
use App\Division;
use App\Departament;
use App\Unit;
use App\Unitcode;
use App\Permise;
use Laracast\Flash\Flash;
use App\Http\Requests\PermisesRequest;

class PermisesController extends Controller
{
 
 	public function __construct()
		{
			Carbon::setlocale('es');	
		}

	public function index()
		{
			$users = User::orderBy('name', 'ASC')->pluck('name', 'id');
			$carges = Carge::orderBy('name', 'ASC')->pluck('name', 'id');
			$typenomines = Typenomine::orderBy('name', 'ASC')->pluck('name', 'id');
			$locations = Location::orderBy('name', 'ASC')->pluck('name', 'id');
			$gerencys = Gerency::orderBy('name', 'ASC')->pluck('name', 'id');
			$divisions = Division::orderBy('name', 'ASC')->pluck('name', 'id');
			$departements = Departament::orderBy('name', 'ASC')->pluck('name', 'id');
			$units = Unit::orderBy('name', 'ASC')->pluck('name', 'id');
			$unitcodes = Unitcode::orderBy('name', 'ASC')->pluck('name', 'id');
			$permisemotives = Permisemotive::orderBy('name', 'ASC')->pluck('name', 'id');
			$permisetypes = Permisetype::orderBy('name', 'ASC')->pluck('name', 'id');
			
			$permises = Permise::orderBy('id','ASC')->paginate(6);
			$permises->each(function($permises){
				$permises->user;
				$permises->carge;
				$permises->typenomine;
				$permises->location;
				$permises->gerency;
				$permises->division;
				$permises->departament;
				$permises->unit;
				$permises->unitcode;
			});

			return view('admin.permises.index')->with('permises', $permises);
		}

	public function store(Request $request)
		//public function store(UserRequest $request)
		{
			//dd($request);
			$permise = new Permise($request->all());
			$permise->user_id = \Auth::user()->id;
			$permise->carge_id = \Auth::user()->carge_id;
			$permise->unit_id = \Auth::user()->unit_id;
			$permise->unitcode_id = \Auth::user()->unitcode_id;
			$permise->datesolicit = Carbon::createFromFormat('d/m/Y', $request->datesolicit)->toDateString();
			$permise->start_date = Carbon::createFromFormat('d/m/Y', $request->start_date)->toDateString();
			$permise->end_date = Carbon::createFromFormat('d/m/Y', $request->end_date)->toDateString();
			
			
			$permise->save();

			flash('Permiso  ' . $permise->id . ' Creado Correctamente')->success();

			return redirect('admin/permises');
			
		}
		//

	public function create()
		{

			$users = User::orderBy('name', 'ASC')->pluck('name', 'id');
			$carges = Carge::orderBy('name', 'ASC')->pluck('name', 'id');
			$typenomines = Typenomine::orderBy('name', 'ASC')->pluck('name', 'id');
			$locations = Location::orderBy('name', 'ASC')->pluck('name', 'id');
			$gerencys = Gerency::orderBy('name', 'ASC')->pluck('name', 'id');
			$divisions = Division::orderBy('name', 'ASC')->pluck('name', 'id');
			$departements = Departament::orderBy('name', 'ASC')->pluck('name', 'id');
			$units = Unit::orderBy('name', 'ASC')->pluck('name', 'id');
			$unitcodes = Unitcode::orderBy('name', 'ASC')->pluck('name', 'id');
			$permisemotives = Permisemotive::orderBy('name', 'ASC')->pluck('name', 'id');
			$permisetypes = Permisetype::orderBy('name', 'ASC')->pluck('name', 'id');

			return view('admin.permises.create')->with('permisetypes', $permisetypes)->with('permisemotives', $permisemotives)->with('carges', $carges)->with('typenomines', $typenomines)->with('locations', $locations)->with('gerencys', $gerencys)->with('divisions', $divisions)->with('departements', $departements)->with('units', $units)->with('unitcodes', $unitcodes);

			
		}


	public function show($id)
		{
			//
		}


	public function destroy($id)
		{
			$permise = Permise::find($id);
			$permise -> delete();
			flash('Permiso  ' . $permise->id . ' Borrado Correctamente')->warning();
			return redirect('admin/permises');
		}

	public function edit($id)
		{
			
			$users = User::orderBy('name', 'ASC')->pluck('name', 'id');
			$carges = Carge::orderBy('name', 'ASC')->pluck('name', 'id');
			$typenomines = Typenomine::orderBy('name', 'ASC')->pluck('name', 'id');
			$locations = Location::orderBy('name', 'ASC')->pluck('name', 'id');
			$gerencys = Gerency::orderBy('name', 'ASC')->pluck('name', 'id');
			$divisions = Division::orderBy('name', 'ASC')->pluck('name', 'id');
			$departaments = Departament::orderBy('name', 'ASC')->pluck('name', 'id');
			$units = Unit::orderBy('name', 'ASC')->pluck('name', 'id');
			$unitcodes = Unitcode::orderBy('name', 'ASC')->pluck('name', 'id');
			$permisemotives = Permisemotive::orderBy('name', 'ASC')->pluck('name', 'id');
			$permisetypes = Permisetype::orderBy('name', 'ASC')->pluck('name', 'id');
			
			$permise = Permise::find($id);
			$permise->user;
			$permise->carge;
			$permise->typenomine;
			$permise->location;
			$permise->gerency;
			$permise->division;
			$permise->departament;
			$permise->unit;
			$permise->unitcode;
			$permise->permisemotive;
			$permise->permisetypes;

			return view('admin.permises.edit')->with('permise', $permise)->with('permisetypes', $permisetypes)->with('permisemotives', $permisemotives)->with('carges', $carges)->with('typenomines', $typenomines)->with('locations', $locations)->with('gerencys', $gerencys)->with('divisions', $divisions)->with('departaments', $departaments)->with('units', $units)->with('unitcodes', $unitcodes);
		}

	public function update(Request $request, $id)
	    {
	        $permise = Permise::find($id);
	        $permise->fill($request->all());
	        $permise->user_id = \Auth::user()->id;
	        $permise->user_id = \Auth::user()->id;
			$permise->carge_id = \Auth::user()->carge_id;
			$permise->unit_id = \Auth::user()->unit_id;
			$permise->unitcode_id = \Auth::user()->unitcode_id;
			$permise->datesolicit = Carbon::createFromFormat('d/m/Y', $request->datesolicit)->toDateString();
			$permise->start_date = Carbon::createFromFormat('d/m/Y', $request->start_date)->toDateString();
			$permise->end_date = Carbon::createFromFormat('d/m/Y', $request->end_date)->toDateString();

	        $permise->save();
	        flash('Permiso ' . $permise->id . ' ha sido Editado Correctamente')->warning();
	        return redirect('admin/permises');
	    }





}
