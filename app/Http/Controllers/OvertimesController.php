<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Foundation\Http\FormRequest;
use App\User;
use App\Overtime;
use App\Location;
use App\Ceco;
use App\Gerency;
use App\Unit;
use App\Division;
use App\Departament;
use App\Nominearea;
use App\Codenomine;
use Carbon\Carbon;


        

use Laracast\Flash\Flash;
use App\Http\Requests\PersoRequest;

class OvertimesController extends Controller
{

    public function __construct()
        {
            Carbon::setlocale('es');    
        }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('name', 'ASC')->pluck('name', 'id');
        $overtimes = Overtime::orderBy('id','ASC')->paginate(6);
        $overtimes->each(function($overtimes){
                $overtimes->users;
        });

            return view('admin.overtimes.index')->with('overtimes', $overtimes)->with('users', $users);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $users = User::orderBy('name', 'ASC')->pluck('name', 'id');
        $locations = Location::orderBy('name', 'ASC')->pluck('name', 'id');
        $cecos = Ceco::orderBy('name', 'ASC')->pluck('name', 'id');
        $gerencys = Gerency::orderBy('name', 'ASC')->pluck('name', 'id');
        $units = Unit::orderBy('name', 'ASC')->pluck('name', 'id');
        $divisions = Division::orderBy('name', 'ASC')->pluck('name', 'id');
        $departaments = Departament::orderBy('name', 'ASC')->pluck('name', 'id');
        $nomineareas = Nominearea::orderBy('name', 'ASC')->pluck('name', 'id');
        $codenomines = Codenomine::orderBy('name', 'ASC')->pluck('name', 'id');

        return view('admin.overtimes.create')->with('users', $users)->with('locations', $locations)->with('cecos', $cecos)->with('gerencys', $gerencys)->with('units', $units)->with('divisions', $divisions)->with('departaments', $departaments)->with('nomineareas', $nomineareas)->with('codenomines', $codenomines);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
            $overtimes = new Overtime($request->all());
            $overtimes->user_id = \Auth::user()->id;
            $overtimes->currentdate = Carbon::createFromFormat('d/m/Y', $request->currentdate)->toDateString();
            $overtimes->dateofwork = Carbon::createFromFormat('d/m/Y', $request->dateofwork)->toDateString();
            $overtimes->start_date1 = Carbon::createFromFormat('d/m/Y', $request->start_date1)->toDateString();
            $overtimes->end_date1 = Carbon::createFromFormat('d/m/Y', $request->end_date1)->toDateString();

            $overtimes->start_hour = Carbon::parse($request->start_hour);
            $overtimes->end_hour = Carbon::parse($request->end_hour);
            $overtimes->save();

            return redirect('admin/overtimes');



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        

        $users = User::orderBy('name', 'ASC')->pluck('name', 'id');
        $locations = Location::orderBy('name', 'ASC')->pluck('name', 'id');
        $cecos = Ceco::orderBy('name', 'ASC')->pluck('name', 'id');
        $gerencys = Gerency::orderBy('name', 'ASC')->pluck('name', 'id');
        $units = Unit::orderBy('name', 'ASC')->pluck('name', 'id');
        $divisions = Division::orderBy('name', 'ASC')->pluck('name', 'id');
        $departaments = Departament::orderBy('name', 'ASC')->pluck('name', 'id');
        $nomineareas = Nominearea::orderBy('name', 'ASC')->pluck('name', 'id');
        $codenomines = Codenomine::orderBy('name', 'ASC')->pluck('name', 'id');
        
        $overtimes = Overtime::find($id);
        $overtimes->users;
        $overtimes->locations;
        $overtimes->cecos;
        $overtimes->gerencys;
        $overtimes->units;
        $overtimes->divisions;
        $overtimes->departaments;
        $overtimes->codenomines;

            
            return view('admin.overtimes.edit')->with('overtimes', $overtimes)->with('users', $users)->with('locations', $locations)->with('cecos', $cecos)->with('gerencys', $gerencys)->with('units', $units)->with('divisions', $divisions)->with('departaments', $departaments)->with('nomineareas', $nomineareas)->with('codenomines', $codenomines);


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
            $overtimes = Overtime::find($id);
            $overtimes->fill($request->all());
            $overtimes->user_id = \Auth::user()->id;
            $overtimes->currentdate = Carbon::createFromFormat('d/m/Y', $request->currentdate)->toDateString();
            $overtimes->dateofwork = Carbon::createFromFormat('d/m/Y', $request->dateofwork)->toDateString();
            $overtimes->start_date1 = Carbon::createFromFormat('d/m/Y', $request->start_date1)->toDateString();
            $overtimes->end_date1 = Carbon::createFromFormat('d/m/Y', $request->end_date1)->toDateString();
            $overtimes->start_hour = Carbon::parse($request->start_hour);
            $overtimes->end_hour = Carbon::parse($request->end_hour);
            $overtimes->save();
            flash('Asistencia ' . $overtimes->id . ' ha sido Editado Correctamente')->warning();
            return redirect('admin/overtimes');



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
        $overtimes = Overtime::find($id);
        $overtimes -> delete();
        flash('Horario  ' . $overtimes->id . ' Borrado Correctamente')->warning();
        return redirect('admin/overtimes');

    }
}
