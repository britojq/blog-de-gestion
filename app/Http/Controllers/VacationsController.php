<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;
use App\Vacation;
use App\User;
use App\Carge;
use App\Typenomine;
use App\Location;
use App\Gerency;
use App\Division;
use App\Departament;
use App\Unit;
use App\Unitcode;
use App\Permise;
use Laracast\Flash\Flash;
use App\Http\Requests\VacationsRequest;

class VacationsController extends Controller
{
    
    public function __construct()
        {
            Carbon::setlocale('es');    
        }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
            $users = User::orderBy('name', 'ASC')->pluck('name', 'id');
            $carges = Carge::orderBy('name', 'ASC')->pluck('name', 'id');
            $typenomines = Typenomine::orderBy('name', 'ASC')->pluck('name', 'id');
            $locations = Location::orderBy('name', 'ASC')->pluck('name', 'id');
            $gerencys = Gerency::orderBy('name', 'ASC')->pluck('name', 'id');
            $divisions = Division::orderBy('name', 'ASC')->pluck('name', 'id');
            $departements = Departament::orderBy('name', 'ASC')->pluck('name', 'id');
            $units = Unit::orderBy('name', 'ASC')->pluck('name', 'id');
            $unitcodes = Unitcode::orderBy('name', 'ASC')->pluck('name', 'id');
            
            
            $vacations = Vacation::orderBy('id','ASC')->paginate(6);
            $vacations->each(function($vacations){
                $vacations->user;
                $vacations->carge;
                $vacations->typenomine;
                $vacations->location;
                $vacations->gerency;
                $vacations->division;
                $vacations->departament;
                $vacations->unit;
                $vacations->unitcode;
            });

            return view('admin.vacations.index')->with('vacations', $vacations);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         
            $users = User::orderBy('name', 'ASC')->pluck('name', 'id');
            $carges = Carge::orderBy('name', 'ASC')->pluck('name', 'id');
            $typenomines = Typenomine::orderBy('name', 'ASC')->pluck('name', 'id');
            $locations = Location::orderBy('name', 'ASC')->pluck('name', 'id');
            $gerencys = Gerency::orderBy('name', 'ASC')->pluck('name', 'id');
            $divisions = Division::orderBy('name', 'ASC')->pluck('name', 'id');
            $departements = Departament::orderBy('name', 'ASC')->pluck('name', 'id');
            $units = Unit::orderBy('name', 'ASC')->pluck('name', 'id');
            $unitcodes = Unitcode::orderBy('name', 'ASC')->pluck('name', 'id');
            

            return view('admin.vacations.create')->with('carges', $carges)->with('typenomines', $typenomines)->with('locations', $locations)->with('gerencys', $gerencys)->with('divisions', $divisions)->with('departements', $departements)->with('units', $units)->with('unitcodes', $unitcodes);



    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
