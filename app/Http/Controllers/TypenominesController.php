<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Laracast\Flash\Flash;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\TypenominesRequest;
use Auth;
use App\Permise;
use App\Typenomine;
use App\User;

class TypenominesController extends Controller
{
    
     public function index()
		{
			$typenomines = Typenomine::orderBy('id','DESC')->paginate(5);
			return view('admin.typenomines.index')->with('typenomines', $typenomines);
		}

	public function store(TypenominesRequest $request)
		{
			$typenomine = new Typenomine($request->all());
			$typenomine->save();
			flash('La Ubicacion ' . $typenomine->name . ' fue agregada correctamente');
			return redirect('admin/typenomines');
		}


	public function create()
		{
			return view('admin.typenomines.create');
		}


	public function show($id)
		{
			//
		}


	public function destroy($id)
		{
			$typenomine = Typenomine::find($id);
			$typenomine->delete();
			flash('La Ubicacion ' . $typenomine->name . ' ha sido Borrada Correctamente')->warning();
			return redirect('admin/typenomines');
		}

	public function edit($id)
		{
			$typenomine = Typenomine::find($id);
			return view('admin.typenomines.edit')->with('typenomine', $typenomine);
		}

	public function update(Request $request, $id)
	    {
	        $typenomine = Typenomine::find($id);
	        $typenomine->fill($request->all());
	        $typenomine->save();
	        flash('La Ubicacion ' . $typenomine->name . ' ha sido Editada Correctamente')->warning();
	        return redirect('admin/typenomines');
	    }


}
