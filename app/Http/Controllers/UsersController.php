<?php
//
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

use Carbon\Carbon;
use App\User;
use App\Carge;
use App\Typenomine;
use App\Location;
use App\Gerency;
use App\Division;
use App\Departament;
use App\Unit;
use App\Unitcode;


use Laracast\Flash\Flash;
use App\Http\Requests\PersoRequest;

class UsersController extends Controller
{
	public function __construct()
		{
			Carbon::setlocale('es');	
		}
	 
	public function index(Request $request)
		{
			$users = User::Search($request->uid)->orderBy('id','DESC')->paginate(5);
			$users = User::orderBy('id','ASC')->paginate(6);
			$users->each(function($users){
				$users->carge;
				$users->typenomine;
				$users->location;
				$users->gerency;
				$users->division;
				$users->departament;
				$users->unit;
				$users->unitcode;
			});
			//dd($users);
			return view('admin.users.index')->with('users', $users);
		}

	public function store(PersoRequest $request)
		
		{
			//dd($request);
			//dd($request);$user->dob->format('Y-m-d')
			$user = new User($request->all());
			//$user->dob = Carbon::parse($request->dob)->format('Y/d/m');
			//$user->dateingres = Carbon::parse($request->dateingres)->format('Y/d/m');
			$user->password = bcrypt($request->password);
			$user->save();
			flash('Usuario ' . $user->name . ' ha siso Registrado Correctamente');
			return redirect('admin/users');
			
		}
		//

	public function create()
		{

			$carges = Carge::orderBy('name', 'ASC')->pluck('name', 'id');
			$typenomines = Typenomine::orderBy('name', 'ASC')->pluck('name', 'id');
			$locations = Location::orderBy('name', 'ASC')->pluck('name', 'id');
			$gerencys = Gerency::orderBy('name', 'ASC')->pluck('name', 'id');
			$divisions = Division::orderBy('name', 'ASC')->pluck('name', 'id');
			$departements = Departament::orderBy('name', 'ASC')->pluck('name', 'id');
			$units = Unit::orderBy('name', 'ASC')->pluck('name', 'id');
			$unitcodes = Unitcode::orderBy('name', 'ASC')->pluck('name', 'id');

			return view('admin.users.create')->with('carges', $carges)->with('typenomines', $typenomines)->with('locations', $locations)->with('gerencys', $gerencys)->with('divisions', $divisions)->with('departements', $departements)->with('units', $units)->with('unitcodes', $unitcodes);

			//return view('admin.users.create');
		}


	public function show($id)
		{
			$carges = Carge::orderBy('name', 'ASC')->pluck('name', 'id');
			$typenomines = Typenomine::orderBy('name', 'ASC')->pluck('name', 'id');
			$locations = Location::orderBy('name', 'ASC')->pluck('name', 'id');
			$gerencys = Gerency::orderBy('name', 'ASC')->pluck('name', 'id');
			$divisions = Division::orderBy('name', 'ASC')->pluck('name', 'id');
			$departements = Departament::orderBy('name', 'ASC')->pluck('name', 'id');
			$units = Unit::orderBy('name', 'ASC')->pluck('name', 'id');
			$unitcodes = Unitcode::orderBy('name', 'ASC')->pluck('name', 'id');

 			// Buscar  el usuario para mostrar los datos
			$users = User::find($id);
			$users->each(function($users){
				$users->carge;
				$users->typenomine;
				$users->location;
				$users->gerency;
				$users->division;
				$users->departament;
				$users->unit;
				$users->unitcode;
			});

			// convertir la fecha de ingreso a un formato legible al usuario
			//$now = Carbon::parse($users->dateingres);
			//$date = $now->format('d/m/Y');

			return view('admin.users.show')->with('users', $users);
		}


	public function destroy($id)
		{
			$user = User::find($id);
			$user -> delete();
			flash('Usuario ' . $user->name . ' Borrado Correctamente')->warning();
			return redirect('admin/users');
		}

	public function edit($id)
		{
			
			$carges = Carge::orderBy('name', 'ASC')->pluck('name', 'id');
			$typenomines = Typenomine::orderBy('name', 'ASC')->pluck('name', 'id');
			$locations = Location::orderBy('name', 'ASC')->pluck('name', 'id');
			$gerencys = Gerency::orderBy('name', 'ASC')->pluck('name', 'id');
			$divisions = Division::orderBy('name', 'ASC')->pluck('name', 'id');
			$departaments = Departament::orderBy('name', 'ASC')->pluck('name', 'id');
			$units = Unit::orderBy('name', 'ASC')->pluck('name', 'id');
			$unitcodes = Unitcode::orderBy('name', 'ASC')->pluck('name', 'id');
			
			$users = User::find($id);
			$users->carge;
			$users->typenomine;
			$users->location;
			$users->gerency;
			$users->division;
			$users->departament;
			$users->unit;
			$users->unitcode;

			return view('admin.users.edit')->with('users', $users)->with('carges', $carges)->with('typenomines', $typenomines)->with('locations', $locations)->with('gerencys', $gerencys)->with('divisions', $divisions)->with('departaments', $departaments)->with('units', $units)->with('unitcodes', $unitcodes);

		}

	public function update(Request $request, $id)
	    {
	        $user = User::find($id);
	        //$user->dob = Carbon::parse($request->dob)->format('Y/d/m');
			//$user->dateingres = Carbon::parse($request->dateingres)->format('Y/d/m');
	        $user->fill($request->all());
	        $user->password = bcrypt($request->password);
	        $user->save();
	        flash('Usuario ' . $user->name . ' ha sido Editado Correctamente')->warning();
	        return redirect('admin/users');
	    }


}