<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Calendar;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;

use App\Event;
use App\User;
use App\Guard;

class CalendersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = [];
              $data = Event::all();
              if($data->count()) {
                  foreach ($data as $key => $value) {
                      $events[] = Calendar::event(
                          $value->title,
                          true,
                          new \DateTime($value->start_date),
                          new \DateTime($value->end_date.' +1 day'),
                          null,
                          // Add color and link on event
                        [
                            'color' => '#f05050',
                            //'url' => 'events',
                        ]
                      );
                  }
              }
              $calendar = Calendar::addEvents($events);
              return view('admin.calenders.index', compact('calendar'));



       // return view('admin.calenders.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
