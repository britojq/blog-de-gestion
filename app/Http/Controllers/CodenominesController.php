<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Laracast\Flash\Flash;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\CodenominesRequest;
use Auth;
use App\Codenomine;
use App\User;

class CodenominesController extends Controller
{
        public function index()
		{
			$codenomines = Codenomine::orderBy('id','DESC')->paginate(5);
			return view('admin.codenomines.index')->with('codenomines', $codenomines);
		}

	public function store(CodenominesRequest $request)
		{
			$codenomine = new codenomine($request->all());
			$codenomine->save();
			flash('El codigo de nomina ' . $codenomine->name . ' fue agregado correctamente');
			return redirect('admin/codenomines');
		}


	public function create()
		{
			return view('admin.codenomines.create');
		}


	public function show($id)
		{
			//
		}


	public function destroy($id)
		{
			$codenomine = codenomine::find($id);
			$codenomine->delete();
			flash('El codigo de nomina ' . $codenomine->name . ' ha sido Borrado Correctamente')->warning();
			return redirect('admin/codenomines');
		}

	public function edit($id)
		{
			$codenomine = codenomine::find($id);
			return view('admin.codenomines.edit')->with('codenomine', $codenomine);
		}

	public function update(Request $request, $id)
	    {
	        $codenomine = codenomine::find($id);
	        $codenomine->fill($request->all());
	        $codenomine->save();
	        flash('El codigo de nomina ' . $codenomine->name . ' ha sido Editado Correctamente')->warning();
	        return redirect('admin/codenomines');
	    }

}
