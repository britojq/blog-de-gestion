<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Laracast\Flash\Flash;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\UnitcodesRequest;
use Auth;
use App\Permise;
use App\Unitcode;
use App\User;

class unitcodesController extends Controller
{
        
     public function index()
		{
			$unitcodes = Unitcode::orderBy('id','DESC')->paginate(5);
			return view('admin.unitcodes.index')->with('unitcodes', $unitcodes);
		}

	public function store(UnitcodesRequest $request)
		{
			$unitcode = new Unitcode($request->all());
			$unitcode->save();
			flash('La Ubicacion ' . $unitcode->name . ' fue agregada correctamente');
			return redirect('admin/unitcodes');
		}


	public function create()
		{
			return view('admin.unitcodes.create');
		}


	public function show($id)
		{
			//
		}


	public function destroy($id)
		{
			$unitcode = Unitcode::find($id);
			$unitcode->delete();
			flash('La Ubicacion ' . $unitcode->name . ' ha sido Borrada Correctamente')->warning();
			return redirect('admin/unitcodes');
		}

	public function edit($id)
		{
			$unitcode = Unitcode::find($id);
			return view('admin.unitcodes.edit')->with('unitcode', $unitcode);
		}

	public function update(Request $request, $id)
	    {
	        $unitcode = Unitcode::find($id);
	        $unitcode->fill($request->all());
	        $unitcode->save();
	        flash('La Ubicacion ' . $unitcode->name . ' ha sido Editada Correctamente')->warning();
	        return redirect('admin/unitcodes');
	    }

}
