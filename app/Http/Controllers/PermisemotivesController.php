<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Laracast\Flash\Flash;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\PermisemotivesRequest;
use Auth;
use App\Permise;
use App\Permisemotive;
use App\User;

class PermisemotivesController extends Controller
{
	
     public function index()
		{
			$permisemotives = Permisemotive::orderBy('id','DESC')->paginate(5);
			return view('admin.permisemotives.index')->with('permisemotives', $permisemotives);
		}

	public function store(PermisemotivesRequest $request)
		{
			$permisemotive = new permisemotive($request->all());
			$permisemotive->save();
			flash('La Ubicacion ' . $permisemotive->name . ' fue agregada correctamente');
			return redirect('admin/permisemotives');
		}


	public function create()
		{
			return view('admin.permisemotives.create');
		}


	public function show($id)
		{
			//
		}


	public function destroy($id)
		{
			$permisemotive = Permisemotive::find($id);
			$permisemotive->delete();
			flash('La Ubicacion ' . $permisemotive->name . ' ha sido Borrada Correctamente')->warning();
			return redirect('admin/permisemotives');
		}

	public function edit($id)
		{
			$permisemotive = Permisemotive::find($id);
			return view('admin.permisemotives.edit')->with('permisemotive', $permisemotive);
		}

	public function update(Request $request, $id)
	    {
	        $permisemotive = Permisemotive::find($id);
	        $permisemotive->fill($request->all());
	        $permisemotive->save();
	        flash('La Ubicacion ' . $permisemotive->name . ' ha sido Editada Correctamente')->warning();
	        return redirect('admin/permisemotives');
	    }


}
