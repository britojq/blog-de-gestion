<?php
//
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laracast\Flash\Flash;
use App\Article;
use App\Category;
use App\Tag;
use App\Image;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Http\Requests\ArticleRequest;



class ArticlesController extends Controller
{
    
public function index(Request $request)
	{

		$articles = Article::Search($request->title)->orderBy('id','DESC')->paginate(5);
		$articles->each(function($articles){
			$articles->category;
			$articles->user;
		});

		//dd($articles);

		return view ('admin.articles.index')->with('articles', $articles);
	}

public function store(ArticleRequest $request)
	{

		//Manipulacion de Imagenes
		if ($request->file('image')) 
			
			{
				$file = $request->file('image');
				$name = 'proyecto_' . time() . '.' . $file->getClientOriginalExtension();
				$path = public_path() . '/images/articles/';
				$file->move($path, $name);
			}

		$article = new Article($request->all());
		$article->user_id = \Auth::user()->id;
		$article->save();

		$article->tags()->sync($request->tags);

		$image = new Image();
		$image->name = $name;
		$image->article()->associate($article);
		$image->save();

		flash('El ArticuloSe ha creado el articulo  ' . $article->title . ' de forma satisfactoria')->success();
	        return redirect('admin/articles');
		
	}


public function create()
	{
		$categories = Category::orderBy('name', 'ASC')->pluck('name', 'id');
		$tags = Tag::orderBy('name', 'ASC')->pluck('name', 'id');

		return view('admin.articles.create')->with('categories', $categories)->with('tags', $tags);

	}


public function show($id)
	{
		//
	}


public function destroy($id)
	{
		$article = Article::find($id);
		$article->delete();
		flash('La Categoria ' . $article->name . ' ha sido Borrado Correctamente')->warning();
		return redirect('admin/articles');
	}

public function edit($id)
	{
		$article = Article::find($id);
		$article->category;
		
		$categories = Category::orderBy('name', 'DESC')->pluck('name', 'id');
		$tags = Tag::orderBy('name', 'DESC')->pluck('name', 'id');

		$my_tags = $article->tags->pluck('id')->ToArray();
		
		return view('admin.articles.edit')->with('categories', $categories)->with('article', $article)->with('tags', $tags)->with('my_tags', $my_tags);
	}

public function update(Request $request, $id)
    {
        
        $article = Article::find($id);
        $article->fill($request->all());
        $article->save();

        $article->tags()->sync($request->tags);
        //$article->tags->sync($request->tags);

        flash('El articulo ' . $article->title . ' ha sido editado correctamente')->warning();
		return redirect('admin/articles');
    }


}
