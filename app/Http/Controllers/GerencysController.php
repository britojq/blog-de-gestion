<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Laracast\Flash\Flash;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\GerencysRequest;
use Auth;
use App\Gerency;
use App\User;

class GerencysController extends Controller
{
    public function index()
		{
			$gerencys = Gerency::orderBy('id','DESC')->paginate(5);
			return view('admin.gerencys.index')->with('gerencys', $gerencys);
		}

	public function store(GerencysRequest $request)
		{
			$gerency = new gerency($request->all());
			$gerency->save();
			flash('La Gerencia ' . $gerency->name . ' fue agregada correctamente');
			return redirect('admin/gerencys');
		}


	public function create()
		{
			return view('admin.gerencys.create');
		}


	public function show($id)
		{
			//
		}


	public function destroy($id)
		{
			$gerency = gerency::find($id);
			$gerency->delete();
			flash('La Gerencia ' . $gerency->name . ' ha sido Borrada Correctamente')->warning();
			return redirect('admin/gerencys');
		}

	public function edit($id)
		{
			$gerency = gerency::find($id);
			return view('admin.gerencys.edit')->with('gerency', $gerency);
		}

	public function update(Request $request, $id)
	    {
	        $gerency = gerency::find($id);
	        $gerency->fill($request->all());
	        $gerency->save();
	        flash('La Gerencia ' . $gerency->name . ' ha sido Editada Correctamente')->warning();
	        return redirect('admin/gerencys');
	    }
}
