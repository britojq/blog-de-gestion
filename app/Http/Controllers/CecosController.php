<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Laracast\Flash\Flash;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\CecosRequest;
use Auth;
use App\Ceco;
use App\Overtime;

class CecosController extends Controller
{
    
    public function index()
		{
			$cecos = Ceco::orderBy('id','DESC')->paginate(5);
			return view('admin.cecos.index')->with('cecos', $cecos);
		}

	public function store(CecosRequest $request)
		{
			$ceco = new ceco($request->all());
			$ceco->save();
			flash('El centro de Costo ' . $ceco->name . ' fue agregado correctamente');
			return redirect('admin/cecos');
		}


	public function create()
		{
			return view('admin.cecos.create');
		}


	public function show($id)
		{
			//
		}


	public function destroy($id)
		{
			$ceco = ceco::find($id);
			$ceco->delete();
			flash('El centro de costo ' . $ceco->name . ' ha sido Borrada Correctamente')->warning();
			return redirect('admin/cecos');
		}

	public function edit($id)
		{
			$ceco = ceco::find($id);
			return view('admin.cecos.edit')->with('ceco', $ceco);
		}

	public function update(Request $request, $id)
	    {
	        $ceco = ceco::find($id);
	        $ceco->fill($request->all());
	        $ceco->save();
	        flash('El centro de costo ' . $ceco->name . ' ha sido Editado Correctamente')->warning();
	        return redirect('admin/cecos');
	    }


}
