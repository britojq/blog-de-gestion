<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Laracast\Flash\Flash;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\UnitsRequest;
use Auth;
use App\Permise;
use App\Unit;
use App\User;

class UnitsController extends Controller
{
            
     public function index()
		{
			$units = Unit::orderBy('id','DESC')->paginate(5);
			return view('admin.units.index')->with('units', $units);
		}

	public function store(UnitsRequest $request)
		{
			$unit = new unit($request->all());
			$unit->save();
			flash('La Ubicacion ' . $unit->name . ' fue agregada correctamente');
			return redirect('admin/units');
		}


	public function create()
		{
			return view('admin.units.create');
		}


	public function show($id)
		{
			//
		}


	public function destroy($id)
		{
			$unit = Unit::find($id);
			$unit->delete();
			flash('La Ubicacion ' . $unit->name . ' ha sido Borrada Correctamente')->warning();
			return redirect('admin/units');
		}

	public function edit($id)
		{
			$unit = Unit::find($id);
			return view('admin.units.edit')->with('unit', $unit);
		}

	public function update(Request $request, $id)
	    {
	        $unit = Unit::find($id);
	        $unit->fill($request->all());
	        $unit->save();
	        flash('La Ubicacion ' . $unit->name . ' ha sido Editada Correctamente')->warning();
	        return redirect('admin/units');
	    }


    
}
