<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Article;
use App\Category;
use App\Tag;
use App\Image;
use Auth;
use Carbon\Carbon;

class FrontController extends Controller
{
    
	public function __construct()
	{
		Carbon::setlocale('es');	
	}


    public function index()
	{
		$articles = Article::orderBy('id', 'DESC')->paginate(4);
		/*$articles->each(function($articles){
			$article->category;
			$article->image;
		})*/
		return view ('front.index')->with('articles', $articles);
	}

	public function SearchCategory($name)
	{
		$category = Category::SearchCategory($name)->first();
		$articles = $category->articles()->paginate(4);
		$articles->each(function($articles){
			$articles->category;
			$articles->image;
			});
		return view ('front.index')->with('articles', $articles);
		
	}

	public function SearchTag($name)
	{
		$tag = Tag::SearchTag($name)->first();
		$articles = $tag->articles()->paginate(4);
		$articles->each(function($articles){
			$articles->category;
			$articles->image;
			});

		return view ('front.index')->with('articles', $articles);
	}

	public function viewArticle($id)
	{
		$article = Article::find($id);
		$article->category;
		$article->user;
		$article->tags;
		$article->images;

		return view('front.article')->with('article', $article);
	}

}
