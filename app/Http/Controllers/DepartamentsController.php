<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Laracast\Flash\Flash;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\DepartamentsRequest;
use Auth;
use App\User;
use App\Departament;

class DepartamentsController extends Controller
{
        public function index()
		{
			$departaments = Departament::orderBy('id','DESC')->paginate(5);
			return view('admin.departaments.index')->with('departaments', $departaments);
		}

	public function store(departamentsRequest $request)
		{
			$departament = new departament($request->all());
			$departament->save();
			flash('El Departamento ' . $departament->name . ' fue agregado correctamente');
			return redirect('admin/departaments');
		}


	public function create()
		{
			return view('admin.departaments.create');
		}


	public function show($id)
		{
			//
		}


	public function destroy($id)
		{
			$departament = departament::find($id);
			$departament->delete();
			flash('El Departamento ' . $departament->name . ' ha sido Borrada Correctamente')->warning();
			return redirect('admin/departaments');
		}

	public function edit($id)
		{
			$departament = departament::find($id);
			return view('admin.departaments.edit')->with('departament', $departament);
		}

	public function update(Request $request, $id)
	    {
	        $departament = departament::find($id);
	        $departament->fill($request->all());
	        $departament->save();
	        flash('El Departamento ' . $departament->name . ' ha sido Editado Correctamente')->warning();
	        return redirect('admin/departaments');
	    }


}
