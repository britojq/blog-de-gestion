<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Laracast\Flash\Flash;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\JournalcodesRequest;
use Auth;
use App\Journalcode;
use App\User;


class JournalcodesController extends Controller
{
       public function index()
		{
			
		return view('admin.journalcodes.index');
		}

	public function store(JournalcodesRequest $request)
		{
			$journalcode = new journalcode($request->all());
			$journalcode->save();
			flash('El Codigo de Horario ' . $journalcode->name . ' fue agregada correctamente');
			return redirect('admin/journalcodes');
		}


	public function create()
		{
			return view('admin.journalcodes.create');
		}


	public function show($id)
		{
			//
		}


	public function destroy($id)
		{
			$journalcode = Journalcode::find($id);
			$journalcode->delete();
			flash('El Codigo de Horario ' . $journalcode->name . ' ha sido Borrada Correctamente')->warning();
			return redirect('admin/journalcodes');
		}

	public function edit($id)
		{
			$journalcode = Journalcode::find($id);
			return view('admin.journalcodes.edit')->with('journalcode', $journalcode);
		}

	public function update(Request $request, $id)
	    {
	        $journalcode = Journalcode::find($id);
	        $journalcode->fill($request->all());
	        $journalcode->save();
	        flash('El Codigo de Horario ' . $journalcode->name . ' ha sido Editada Correctamente')->warning();
	        return redirect('admin/journalcodes');
	    }
}
