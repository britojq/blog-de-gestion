<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Laracast\Flash\Flash;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\LocationsRequest;
use Auth;
use App\Location;
use App\User;


class LocationsController extends Controller
{
    public function index()
		{
			$locations = Location::orderBy('id','DESC')->paginate(5);
			return view('admin.locations.index')->with('locations', $locations);
		}

	public function store(LocationsRequest $request)
		{
			$location = new Location($request->all());
			$location->save();
			flash('La Ubicacion ' . $location->name . ' fue agregada correctamente');
			return redirect('admin/locations');
		}


	public function create()
		{
			return view('admin.locations.create');
		}


	public function show($id)
		{
			//
		}


	public function destroy($id)
		{
			$location = Location::find($id);
			$location->delete();
			flash('La Ubicacion ' . $location->name . ' ha sido Borrada Correctamente')->warning();
			return redirect('admin/locations');
		}

	public function edit($id)
		{
			$location = Location::find($id);
			return view('admin.locations.edit')->with('location', $location);
		}

	public function update(Request $request, $id)
	    {
	        $location = Location::find($id);
	        $location->fill($request->all());
	        $location->save();
	        flash('La Ubicacion ' . $location->name . ' ha sido Editada Correctamente')->warning();
	        return redirect('admin/locations');
	    }
	    
}
