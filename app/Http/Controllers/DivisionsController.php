<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Laracast\Flash\Flash;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\DivisionsRequest;
use Auth;
use App\Division;
use App\User;

class DivisionsController extends Controller
{
    public function index()
		{
			$divisions = Division::orderBy('id','DESC')->paginate(5);
			return view('admin.divisions.index')->with('divisions', $divisions);
		}

	public function store(DivisionsRequest $request)
		{
			$division = new division($request->all());
			$division->save();
			flash('La Division ' . $division->name . ' fue agregada correctamente');
			return redirect('admin/divisions');
		}


	public function create()
		{
			return view('admin.divisions.create');
		}


	public function show($id)
		{
			//
		}


	public function destroy($id)
		{
			$division = division::find($id);
			$division->delete();
			flash('La Division ' . $division->name . ' ha sido Borrada Correctamente')->warning();
			return redirect('admin/divisions');
		}

	public function edit($id)
		{
			$division = division::find($id);
			return view('admin.divisions.edit')->with('division', $division);
		}

	public function update(Request $request, $id)
	    {
	        $division = division::find($id);
	        $division->fill($request->all());
	        $division->save();
	        flash('La Division ' . $division->name . ' ha sido Editada Correctamente')->warning();
	        return redirect('admin/divisions');
	    }

}
