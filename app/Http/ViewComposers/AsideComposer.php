<?php

namespace App\Http\ViewComposers;
use Illuminate\Support\ServiceProvider;

use Illuminate\Contracts\View\View;
use App\Category;
use App\Tag;

class AsideComposer {

	public function compose(View $View)
	{
		$categories = Category::orderBy('name', 'DESC')->get();
		$tags = Tag::orderBy('name', 'DESC')->get();
		
		$View->with('categories', $categories)->with('tags', $tags);

	}	

	
}