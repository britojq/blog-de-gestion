<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guard extends Model
{
    protected $table = "guards";
    protected $fillable = ['user_id', 'from_date', 'to_date'];

	public function user()
    {
    	return $this->belongsTo('App\User');
    }



    public function articles()
    {
        return $this->BelongsToMany('App\Event');
    }

    /*
    public function event()
    {
        return $this->belongsTo('App\Event');
    }*/

    
}
