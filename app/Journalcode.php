<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Journalcode extends Model
{
    protected $table = "journalcodes";
    protected $fillable = ['name'];

	public function users()
    {
    	return $this->hasMany('App\User');
    }

}
