<?php
//
namespace App;
//
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "users";
    protected $fillable = ['name', 'lastname', 'cedula', 'email', 'uid', 'dob', 'password', 'personalnumber', 'dateingres', 'carge_id', 'phonehab', 'phonecel', 'phoneofc', 'typenomine_id', 'location_id', 'gerency_id', 'division_id', 'departament_id', 'unit_id', 'unitcode_id', 'type'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function admin()
    {
        return $this->type === 'admin';
    }

    public function articles()
    {
        return $this->hasMany('App\Article');
    }

     public function guards()
    {
        return $this->hasMany('App\Guard');
    }

     public function overtimes()
    {
        return $this->hasMany('App\Overtime');
    }


    public function vacations()
    {
        return $this->hasMany('App\Vacation');
    }

    public function permises()
    {
        return $this->hasMany('App\Permise');
    }
    
    public function timetrakings()
    {
        return $this->hasMany('App\Timetraking');
    }

    public function events()
    {
        return $this->hasMany('App\Event');
    }
   
    ////////////////////////////////////////////////////

    public function carge()
    {
        return $this->belongsTo('App\Carge');
    }

    public function ceco()
    {
        return $this->belongsTo('App\Ceco');
    }
//
    public function codenomine()
    {
        return $this->belongsTo('App\Codenomine');
    }

    public function departament()
    {
        return $this->belongsTo('App\Departament');
    }
    
    public function division()
    {
        return $this->belongsTo('App\Division');
    }

    public function gerency()
    {
        return $this->belongsTo('App\Gerency');
    }

    public function journalcode()
    {
        return $this->belongsTo('App\Journalcode');
    }

    public function location()
    {
        return $this->belongsTo('App\Location');
    }

    public function permisemotive()
    {
        return $this->belongsTo('App\Permisemotive');
    }

    public function permisetype()
    {
        return $this->belongsTo('App\Permisetype');
    }

    public function typenomine()
    {
        return $this->belongsTo('App\Typenomine');
    }

    public function unit()
    {
        return $this->belongsTo('App\Unit');
    }

    public function unitcode()
    {
        return $this->belongsTo('App\Unitcode');
    }

    public function nominearea()
    {
        return $this->belongsTo('App\Nominearea');
    }

/*
    public function scopeSearch($query, $name)
    {
            return $query->where('name', '=', $name);
            dd($name);
    }

*/
/*
    public function scopeSearch($query, $uid)
    {
        return $query->where('uid', 'LIKE', "%$uid%");
    }
*/
    public function scopeSearch($query, $uid)
    {
            return $query->where('uid', '=', $uid);
            dd($uid);
    }
}
