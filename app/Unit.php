<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $table = "units";
    protected $fillable = ['name'];

	public function users()
    {
    	return $this->hasMany('App\User');
    }

    public function overtimes()
    {
        return $this->hasMany('App\Overtime');
    }

    public function vacations()
    {
        return $this->hasMany('App\Vacation');
    }
    
    public function permises()
    {
        return $this->hasMany('App\Permise');
    }
}

