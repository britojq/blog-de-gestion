# PROYECTO DE BLOG CON GESTION Y ADMINISTRACION DE RECURSOS

### Aplicacion diseñada con 

<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>



## Acerca del Sistema 

Esta aplicacion fue diseñada para solventar ciertas debilidades en el departamento y unir diferentes metodos de gestion de infraestructura en un solo aplicativo, facilitando asi la administracion de los recursos del departamento entre las cosas que se pueden hacer destacan las siguientes:

-  Solicitud de Permisos por parte del personal
-  Carga de Horas extras por parte del personal
-  Solicitud de Vacaciones (para que sea armado el programa anual de vacaciones)
-  Control de Horario del Personal
-  Gestion de Viaticos (personal carga los viaticos generados)
-  Carga de Guardias (Mostrar en Calendario las guardias)
-  Proyectos (gestionar de manera eficiente los proyectos usando el calendario integrado que incluye las vacaciones, guardias y permisos)

### Security Vulnerabilities

Si descubres alguna vulneravilidad o error, por favor enviame un correo a: britojq@gmail.com

### License

The aplication is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).

## Autor
Jose A. Brito H.
