<?php

use Illuminate\Database\Seeder;
use App\Tag;

class AddDummyTags extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [

        	['name'=>'LINUX'],

        	['name'=>'WINDOWS'],

        	['name'=>'SEGURIDAD'],

            ['name'=>'CISCO'],

            ['name'=>'ROUTERS'],

            ['name'=>'PROXY'],
            
            ['name'=>'INTERNET'],

            ['name'=>'NAVEGADORES'],

            ['name'=>'FIREFOX'],
            
            ['name'=>'CHROME'],

            ['name'=>'DEBIAN'],

            ['name'=>'RED HAT'],

            ['name'=>'NOTICIAS'],
            
            ['name'=>'NOTICIAS DE SEGURIDAD'],

            ['name'=>'NOTICIAS ULTIMA HORA'],

            ['name'=>'ULTIMA HORA'],

            ['name'=>'ANDROID'],
            
            ['name'=>'NOTICIAS ANDROID'],

            ['name'=>'MAQUINAS VIRTUALES'],

            ['name'=>'SERVIDORES'],

        ];

        foreach ($data as $key => $value) {

        	Tag::create($value);

        }
    }
}
