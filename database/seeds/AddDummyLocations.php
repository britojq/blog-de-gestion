<?php

use Illuminate\Database\Seeder;
use App\Location;


class AddDummyLocations extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [

        	['name'=>'VALLE SECO (EDF. ADMINISTRATIVO'],

        	['name'=>'TORRE 4'],

        	['name'=>'PLANTA CENTRO'],

            ['name'=>'GTII'],

            ['name'=>'OTRA'],

        ];

        foreach ($data as $key => $value) {

        	Location::create($value);

        }
    }
}
