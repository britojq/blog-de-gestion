<?php

use Illuminate\Database\Seeder;
use App\Category;

class AddDummyCategories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    	$data = [

        	['name'=>'LO MAS NUEVO'],
            
            ['name'=>'CURIOSIDADES DE INTERNET'],

            ['name'=>'NOTICIAS DESTACADAS'],
            
            ['name'=>'NOTICIAS DE SEGURIDAD'],

            ['name'=>'NOTICIAS DE ULTIMA HORA'],

        ];

        foreach ($data as $key => $value) {

        	Category::create($value);

        }
    }
}
