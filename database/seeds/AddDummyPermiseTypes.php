<?php

use Illuminate\Database\Seeder;

use App\Permisetype;

class AddDummyPermiseTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $data = [

        	['name'=>'REMUNERADO'],

        	['name'=>'NO REMUNERADO'],

        ];

        foreach ($data as $key => $value) {

        	Permisetype::create($value);

        }
    }
}
