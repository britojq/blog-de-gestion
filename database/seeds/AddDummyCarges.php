<?php

use Illuminate\Database\Seeder;
use App\Carge;

class AddDummyCarges extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [

        	['name'=>'PROFESIONAL IA'],

        	['name'=>'TECNICO IA'],

            ['name'=>'TECNICO IIA'],

        ];

        foreach ($data as $key => $value) {

        	Carge::create($value);

        }
    }
}
