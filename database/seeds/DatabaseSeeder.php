<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(UserSeeder::class);
        //$this->call(AddDummyEvent::class);
        $this->call(AddDummyPermiseMotives::class);
        $this->call(AddDummyPermiseTypes::class);
        $this->call(AddDummyCategories::class);
        $this->call(AddDummyTags::class);
        $this->call(AddDummyLocations::class);
        $this->call(AddDummyCarges::class);
        
    }
}
