<?php

use Illuminate\Database\Seeder;

use App\Permisemotive;

class AddDummyPermiseMotives extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [

        	['name'=>'MATRIMONIO (ENTIDAD FEDERAL)'],

        	['name'=>'MATRIMONIO (FUERA DE LA ENTIDAD)'],

        	['name'=>'RENDIR DECLARACIONES'],

            ['name'=>'ACTIVIDADES DEPORTIVAS'],

            ['name'=>'DETENCION POLICIAL'],

            ['name'=>'SERVICO MILITAR'],
            
            ['name'=>'FALLECIMIENTO DE FAMILIAR (ENTIDAD FEDERAL)'],

            ['name'=>'FALLECIMIENTO DE FAMILIAR (FUERA DE LA ENTIDAD FEDERAL)'],

            ['name'=>'NACIMIENTO DE HIJOS'],
            
            ['name'=>'ENFERMEDAD DE FAMILIAR'],

            ['name'=>'RENOVACION DE CEDULA DE IDENTIDAD'],

            ['name'=>'CERTIFICADO DE SALUD'],

            ['name'=>'LIBRETA MILITAR'],
            
            ['name'=>'LICENCIA DE CONDUCIR'],

            ['name'=>'CERTIFICADO MEDICO'],

            ['name'=>'FIRMA DOCUMENTO DE VIVIENDA'],

            ['name'=>'INSCRIPCION DE HIJO EN COLEGIO'],
            
            ['name'=>'DESCANSO MATERNAL POR ADOPCION'],

            ['name'=>'CONSULTA MEDICA'],

            ['name'=>'OTROS MOTIVOS: '],

        ];

        foreach ($data as $key => $value) {

        	Permisemotive::create($value);

        }

    }
}
