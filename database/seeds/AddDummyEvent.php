<?php

use Illuminate\Database\Seeder;

use App\Event;

class AddDummyEvent extends Seeder

{

    /**

     * Run the database seeds.

     *

     * @return void

     */

    public function run()

    {

        $data = [

        	['title'=>'PROYECTO: Plugins GLPI ', 'start_date'=>'2018-01-08', 'end_date'=>'2018-02-15'],

        	['title'=>'VACACIONES: Jose A. Brito H.', 'start_date'=>'2018-02-01', 'end_date'=>'2018-03-17'],

        	['title'=>'GUARDIA: Nombre Guardia Disponib', 'start_date'=>'2018-01-08', 'end_date'=>'2018-01-15'],

        ];

        foreach ($data as $key => $value) {

        	Event::create($value);

        }

    }

}