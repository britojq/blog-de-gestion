<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Carga datos necesarios para poder crear usuario principal
        DB::table('carges')->insert([
            'name' => 'PROFESIONAL IIA',
        ]);

        DB::table('gerencys')->insert([
            'name' => 'GERENCIA GENERAL DE AUTOMATIZACIÓN, TECNOLOGÍA DE INFORMACIÓN Y TELECOMUNICACIONES',
        ]);

        DB::table('typenomines')->insert([
            'name' => 'Empleado',
        ]);

        DB::table('locations')->insert([
            'name' => 'VALLE SECO',
        ]);

        DB::table('divisions')->insert([
            'name' => 'AP - CARABOBO',
        ]);

        DB::table('departaments')->insert([
            'name' => 'DIV ATIT CARABOBO ARAGUA',
        ]);

        DB::table('units')->insert([
            'name' => 'GPO TRAB INFRA TECNOL CARABOBO CENTRO',
        ]);

        DB::table('unitcodes')->insert([
            'name' => '40005580',
        ]);

        DB::table('cecos')->insert([
            'name' => '40005580',
        ]);

        DB::table('codenomines')->insert([
            'name' => '40005580',
        ]);

        DB::table('nomineareas')->insert([
            'name' => 'Mensual',
        ]);
        

    //Carga el usuario Principal (mi usuario =D )
        DB::table('users')->insert([
            'name' => 'Jose A.',
            'lastname' => 'Brito H.',
            'cedula' => '11746281',
            'uid' => 'A1746281',
            'personalnumber' => '00144306',
            'dateingres' => '2006/10/11',
            'carge_id' => '1',
            'phonehab' => '02424012466',
            'phonecel' => '04244019124',
            'phoneofc' => '02424012481',
            'typenomine_id' => '1',
            'location_id' => '1',
            'gerency_id' => '1',
            'unit_id' => '1',
            'division_id' => '1',
            'departament_id' => '1',
            'unitcode_id' => '1',
            'email' => 'britojq@gmail.com',
            'password' => bcrypt('12345678'),
            'type' => 'admin'
        ]);

    }
}
