<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVacationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacations', function (Blueprint $table) {
            $table->increments('id');
            $table->date('solicitdate');
            $table->integer('user_id')->unsigned();
            $table->integer('unit_id')->unsigned();
            $table->integer('division_id')->unsigned();
            $table->enum('zone',['Caracas', 'Lara', 'Zulia', 'Tachira', 'Bolivar', 'Anzoategui', 'Carabobo', 'Apure', 'Nueva Esparta'])->default('Carabobo');
            $table->integer('typenomine_id')->unsigned();
            $table->enum('option',['A', 'B', 'C'])->default('B');
            $table->date('start_dateA');
            $table->date('end_dateA');
            
            $table->date('start_dateB');
            $table->date('end_dateB');
            
            $table->date('start_dateC1');
            $table->date('end_dateC1');
            $table->date('start_dateC2');
            $table->date('end_dateC2');
            $table->string('observ_just');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->foreign('unit_id')->references('id')->on('units')->onDelete('cascade');

            $table->foreign('division_id')->references('id')->on('divisions')->onDelete('cascade');

            $table->foreign('typenomine_id')->references('id')->on('typenomines')->onDelete('cascade');

            $table->timestamps();
        });


        // PIVOTES
        Schema::create('user_vacations', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('vacation_id')->unsigned();
            
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('vacation_id')->references('id')->on('vacations')->onDelete('cascade');
            
            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacations');
        Schema::dropIfExists('user_vacations');
    }
}
