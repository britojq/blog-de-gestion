<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOvertimeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        

        Schema::create('overtimes', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('location_id')->unsigned();
            $table->date('currentdate');
            $table->integer('ceco_id')->unsigned();
            $table->integer('gerency_id')->unsigned();
            $table->integer('unit_id')->unsigned();
            $table->integer('division_id')->unsigned();
            $table->integer('departament_id')->unsigned();
            //$table->integer('typenomine_id')->unsigned();
            $table->date('start_date1');
            $table->date('end_date1');
            $table->integer('user_id')->unsigned();
            $table->date('dateofwork');
            $table->enum('dayofweek',['Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'])->default('Lunes');
            $table->time('start_hour');
            $table->time('end_hour');
            $table->string('totalhours');
            $table->integer('nominearea_id')->unsigned();
            $table->integer('codenomine_id')->unsigned();
            $table->string('justification');
            $table->string('loadonsap')->nullable();
            $table->string('aprove')->nullable();


            $table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade');
            $table->foreign('ceco_id')->references('id')->on('cecos')->onDelete('cascade');
            $table->foreign('gerency_id')->references('id')->on('divisions')->onDelete('cascade');
            $table->foreign('unit_id')->references('id')->on('divisions')->onDelete('cascade');
            $table->foreign('division_id')->references('id')->on('divisions')->onDelete('cascade');
            $table->foreign('departament_id')->references('id')->on('departaments')->onDelete('cascade');
            //$table->foreign('typenomine_id')->references('id')->on('typenomines')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('codenomine_id')->references('id')->on('codenomines')->onDelete('cascade');
            $table->foreign('nominearea_id')->references('id')->on('nomineareas')->onDelete('cascade');
            $table->timestamps();
        });


        // PIVOTES
        Schema::create('user_overtimes', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('overtime_id')->unsigned();
            
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('overtime_id')->references('id')->on('overtimes')->onDelete('cascade');
            
            $table->timestamps();

        });




    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('overtime');
        Schema::dropIfExists('user_overtimes');
        
    }
}
