<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPermisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        

        Schema::create('permises', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('location_id')->unsigned();
            $table->date('datesolicit');
            $table->integer('user_id')->unsigned();
            $table->integer('carge_id')->unsigned();
            $table->integer('unit_id')->unsigned();
            $table->integer('unitcode_id')->unsigned();
            $table->integer('permisemotive_id')->unsigned();
            $table->string('observmotiv')->nullable();
            $table->date('start_date');
            $table->time('start_hour');
            $table->date('end_date');
            $table->time('end_hour');
            $table->enum('status',['aprovado','pendiente','negado'])->default('pendiente');
            $table->integer('permisetype_id')->unsigned();
            // REFERENCIAS A OTRAS TABLAS
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade'); 
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('carge_id')->references('id')->on('carges')->onDelete('cascade'); 
            $table->foreign('unit_id')->references('id')->on('units')->onDelete('cascade');
            $table->foreign('unitcode_id')->references('id')->on('unitcodes')->onDelete('cascade'); 

            $table->foreign('permisemotive_id')->references('id')->on('permisemotives')->onDelete('cascade');

            $table->foreign('permisetype_id')->references('id')->on('permisetypes')->onDelete('cascade'); 
            $table->timestamps();
        });
    
        // PIVOTES
        Schema::create('users_permises', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('permise_id')->unsigned();
            
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('permise_id')->references('id')->on('permises')->onDelete('cascade');
            $table->timestamps();

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permises');
        Schema::dropIfExists('users_permises');
        

    }
}
