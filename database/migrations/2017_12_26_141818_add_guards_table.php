<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGuardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guards', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->date('from_date');
            $table->date('to_date');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    
    // Pivote para Guardia y Usuarios (tabla PIVOTE)
        Schema::create('user_guards', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('users_id')->unsigned();
            $table->integer('guards_id')->unsigned();
            
            $table->foreign('users_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('guards_id')->references('id')->on('guards')->onDelete('cascade');
            $table->timestamps();

        });

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guards');
        Schema::dropIfExists('user_guards');
    }
}
