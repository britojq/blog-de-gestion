<?php

use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration

{

    /**

     * Run the migrations.

     *

     * @return void

     */

    public function up()

    {

        Schema::create('events', function (Blueprint $table) {

            $table->increments('id');
            $table->enum('type',['proyects','guards','vacations','permises'])->default('proyects');
            $table->string('title');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('guard_id')->unsigned()->nullable();
            $table->foreign('guard_id')->references('id')->on('guards')->onDelete('cascade');
            

            $table->integer('permise_id')->unsigned()->nullable();
            $table->foreign('permise_id')->references('id')->on('permises')->onDelete('cascade');

            $table->integer('vacation_id')->unsigned()->nullable();
            $table->foreign('vacation_id')->references('id')->on('vacations')->onDelete('cascade');


            $table->date('start_date');
            $table->date('end_date');

            

            
            $table->timestamps();

        });

    }

    /**

     * Reverse the migrations.

     *

     * @return void

     */

    public function down()

    {

        Schema::drop("events");

    }

}