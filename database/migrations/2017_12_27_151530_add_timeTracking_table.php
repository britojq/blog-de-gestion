<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimeTrackingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timetracking', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->date('current_date');
            $table->time('time_in');
            $table->boolean('check_in')->nullable();
            $table->time('time_out');
            $table->boolean('check_out')->nullable();
            $table->ipAddress('ipaddress')->nullable();
            $table->ipAddress('hostname')->nullable();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });

        // PIVOTES
        Schema::create('user_timetrack', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('timetracking_id')->unsigned();
            
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->foreign('timetracking_id')->references('id')->on('timetracking')->onDelete('cascade');
            
            $table->timestamps();

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timeTracking');
        Schema::dropIfExists('user_timeTrack');
    }
}
