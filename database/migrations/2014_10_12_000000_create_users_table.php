<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        // APROVECHO ESPACIO PARA CREAR TABLAS ADICIONALES QUE REQUIERE USER

        Schema::create('carges', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();

        });

        Schema::create('gerencys', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();

        });

        Schema::create('units', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();

        });

        Schema::create('unitcodes', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();

        });
       
        Schema::create('divisions', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();

        });

        Schema::create('departaments', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();

        });

        Schema::create('typenomines', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();

        });
        
        Schema::create('locations', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();

        });

        Schema::create('permisemotives', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();

        });

        Schema::create('permisetypes', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();

        });

       Schema::create('nomineareas', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();

        });

        Schema::create('cecos', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();

        });

        Schema::create('codenomines', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();

        });

        

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('lastname');
            $table->string('cedula')->unique()->nullable();
            $table->string('email')->unique();
            $table->string('uid')->unique()->nullable();
            $table->string('dob')->nullable();
            $table->enum('gender',['male','female'])->default('male');
            $table->string('password');
            $table->string('personalnumber')->unique()->nullable();
            $table->string('dateingres')->nullable();
            $table->integer('carge_id')->unsigned()->nullable();
            $table->string('phonehab')->nullable();
            $table->string('phonecel')->nullable();
            $table->string('phoneofc')->nullable();
            $table->integer('typenomine_id')->unsigned()->nullable();
            $table->integer('location_id')->unsigned()->nullable();
            $table->integer('gerency_id')->unsigned()->nullable();       //
            $table->integer('division_id')->unsigned()->nullable();      //
            $table->integer('departament_id')->unsigned()->nullable();   //
            $table->integer('unit_id')->unsigned()->nullable();          //
            $table->integer('unitcode_id')->unsigned()->nullable();      //
            $table->enum('type',['member','admin'])->default('member');
            //RELACIONES CON OTRAS TABLAS
            $table->foreign('carge_id')->references('id')->on('carges')->onDelete('cascade'); // referencia hacia otra tabla
            $table->foreign('typenomine_id')->references('id')->on('typenomines')->onDelete('cascade'); // referencia hacia otra tabla
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade'); // referencia hacia otra tabla
            $table->foreign('gerency_id')->references('id')->on('gerencys')->onDelete('cascade'); // referencia hacia otra tabla
            $table->foreign('division_id')->references('id')->on('divisions')->onDelete('cascade'); // referencia hacia otra tabla
            $table->foreign('departament_id')->references('id')->on('departaments')->onDelete('cascade'); // referencia hacia otra tabla
            $table->foreign('unit_id')->references('id')->on('units')->onDelete('cascade'); //FALTA CREAR TABLA
            $table->foreign('unitcode_id')->references('id')->on('unitcodes')->onDelete('cascade'); //FALTA CREAR TABLA
            $table->rememberToken();
            $table->timestamps();
        });
    
        

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('carges');
        Schema::dropIfExists('gerencys');
        Schema::dropIfExists('units');
        Schema::dropIfExists('divisions');
        Schema::dropIfExists('departaments');
        Schema::dropIfExists('typenomines');
        Schema::dropIfExists('locations');
        Schema::dropIfExists('unitcodes');
        Schema::dropIfExists('permisemotives');
        Schema::dropIfExists('permisetypes');
        Schema::dropIfExists('cecos');
        Schema::dropIfExists('codenomines');
        Schema::dropIfExists('nomineareas');

    }
}
