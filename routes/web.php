<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// RUTAS DEL FRONT 

/*Route::get('/', function () {
    return view('welcome');

});*/

// Rutas para Pagina Principal (FRONT)

Route::resource('tasks', 'TasksController');

Route::get('/', [
		'uses' => 'FrontController@index',
		'as' => 'front.index'
	]);

Route::get('categories/{name}', [
	'uses' => 'FrontController@SearchCategory',
	'as' => 'front.search.category'
]);

Route::get('tags/{name}', [
	'uses' => 'FrontController@SearchTag',
	'as' => 'front.search.tag'
]);

Route::get('articles/{id}', [
	'uses' => 'FrontController@viewArticle',
	'as' => 'front.view.article'
]);



// RUTAS DE PRUEBAS

Route::get('test/datepicker', [
		'uses' => 'probandoelcontroller@index',
		'as' => 'test.datepicker'
	]);


Route::post('test/save', ['as' => 'save-date',
                           'uses' => 'DateController@showDate', 
                            function () {
                                return '';
                            }]);




//FIN RUTAS DE PRUEBAS



// SECCION DONDE SE RESTRINGEN LAS RUTAS PARA SOLO USUARIO ADMIN
Route::group (['prefix' => 'admin', 'middleware' => 'auth'], function(){

	//Ruta para restringir acceso solo a usuarios admin
	Route::group (['middleware' => 'admin'], function(){

		Route::resource('users','UsersController');
			Route::get('users/{id}/destroy', [
				'uses' => 'UsersController@destroy',
				'as' => 'admin.users.destroy'
			]);

		// RUTAS PARA COMPLEMENTOS ADICIONALES

		Route::resource('carges','CargesController');
			Route::get('carges/{id}/destroy', [
				'uses' => 'CargesController@destroy',
				'as' => 'admin.carges.destroy'
			]);

		Route::resource('carges','CargesController');
			Route::get('carges/{id}/destroy', [
				'uses' => 'CargesController@destroy',
				'as' => 'admin.carges.destroy'
			]);

		Route::resource('cecos','CecosController');
			Route::get('cecos/{id}/destroy', [
				'uses' => 'CecosController@destroy',
				'as' => 'admin.cecos.destroy'
			]);
		
		Route::resource('codenomines','CodenominesController');
			Route::get('codenomines/{id}/destroy', [
				'uses' => 'CodenominesController@destroy',
				'as' => 'admin.codenomines.destroy'
			]);

		Route::resource('departaments','DepartamentsController');
			Route::get('departaments/{id}/destroy', [
				'uses' => 'DepartamentsController@destroy',
				'as' => 'admin.departaments.destroy'
			]);

		Route::resource('divisions','DivisionsController');
			Route::get('divisions/{id}/destroy', [
				'uses' => 'DivisionsController@destroy',
				'as' => 'admin.divisions.destroy'
			]);
		
		Route::resource('gerencys','GerencysController');
			Route::get('gerencys/{id}/destroy', [
				'uses' => 'GerencysController@destroy',
				'as' => 'admin.gerencys.destroy'
			]);

		Route::resource('journalcodes','JournalcodesController');
			Route::get('journalcodes/{id}/destroy', [
				'uses' => 'JournalcodesController@destroy',
				'as' => 'admin.journalcodes.destroy'
			]);

		Route::resource('locations','LocationsController');
			Route::get('locations/{id}/destroy', [
				'uses' => 'LocationsController@destroy',
				'as' => 'admin.locations.destroy'
			]);

		Route::resource('permisemotives','PermisemotivesController');
			Route::get('permisemotives/{id}/destroy', [
				'uses' => 'PermisemotivesController@destroy',
				'as' => 'admin.permisemotives.destroy'
			]);

		Route::resource('permisetypes','PermisetypesController');
			Route::get('permisetypes/{id}/destroy', [
				'uses' => 'PermisetypesController@destroy',
				'as' => 'admin.permisetypes.destroy'
			]);

		Route::resource('typenomines','TypenominesController');
			Route::get('typenomines/{id}/destroy', [
				'uses' => 'TypenominesController@destroy',
				'as' => 'admin.typenomines.destroy'
			]);

		Route::resource('unitcodes','UnitcodesController');
			Route::get('unitcodes/{id}/destroy', [
				'uses' => 'UnitcodesController@destroy',
				'as' => 'admin.unitcodes.destroy'
			]);

		Route::resource('units','UnitsController');
			Route::get('units/{id}/destroy', [
				'uses' => 'UnitsController@destroy',
				'as' => 'admin.units.destroy'
			]);

		Route::resource('nomineareas','NomineareasController');
			Route::get('nomineareas/{id}/destroy', [
				'uses' => 'NomineareasController@destroy',
				'as' => 'admin.nomineareas.destroy'
			]);

		Route::resource('components','ComponentsController');
			Route::get('components/{id}/destroy', [
				'uses' => 'ComponentsController@destroy',
				'as' => 'admin.components.destroy'
			]);

		Route::resource('guards','GuardsController');
			Route::get('guards/{id}/destroy', [
				'uses' => 'GuardsController@destroy',
				'as' => 'admin.guards.destroy'
			]);
		

		// FIN DE RUTAS PARA COMPLEMENTOS ADICIONALES

	});
	
	// FIN DE SECCION DONDE SE RESTRINGEN LAS RUTAS PARA SOLO USUARIO ADMIN

	//RUTAS DE ACCESO SOLO USUARIO

	Route::get('/', ['as' => 'admin.index', function(){
		return view('admin.index');
	}
	]);

	/*
	Route::resource('admin','AdminsController');
		Route::get('admin/{id}/destroy', [
			'uses' => 'AdminsController@destroy',
			'as' => 'admin.admins.destroy'
		]);
	*/

	Route::resource('categories','CategoriesController');
		Route::get('categories/{id}/destroy', [
			'uses' => 'CategoriesController@destroy',
			'as' => 'admin.categories.destroy'
		]);

	Route::resource('tags','TagsController');
		Route::get('tags/{id}/destroy', [
			'uses' => 'TagsController@destroy',
			'as' => 'admin.tags.destroy'
		]);

	Route::resource('articles','ArticlesController');
	Route::get('articles/{id}/destroy', [
		'uses' => 'ArticlesController@destroy',
		'as' => 'admin.articles.destroy'
	]);

	Route::get('image',[
		'uses' => 'ImagesController@index',
		'as' =>	'admin.image.index'
	]);

	

	Route::resource('permises','PermisesController');
	Route::get('permises/{id}/destroy', [
		'uses' => 'PermisesController@destroy',
		'as' => 'admin.permises.destroy'
	]);

	Route::resource('timetrackings','TimetrackingsController');
	Route::get('timetrackings/{id}/destroy', [
		'uses' => 'TimetrackingsController@destroy',
		'as' => 'admin.timetrackings.destroy'
	]);

	Route::resource('overtimes','OvertimesController');
	Route::get('overtimes/{id}/destroy', [
		'uses' => 'OvertimesController@destroy',
		'as' => 'admin.overtimes.destroy'
	]);

	Route::resource('vacations','VacationsController');
	Route::get('vacations/{id}/destroy', [
		'uses' => 'VacationsController@destroy',
		'as' => 'admin.vacations.destroy'
	]);

	Route::resource('profiles','ProfilesController');
			Route::get('profiles/{id}/destroy', [
				'uses' => 'ProfilesController@destroy',
				'as' => 'admin.profiles.destroy'
			]);

	Route::resource('solicits','SolicitsController');
			Route::get('solicits/{id}/destroy', [
				'uses' => 'SolicitsController@destroy',
				'as' => 'admin.solicits.destroy'
			]);

	Route::get('events', 'EventController@index');
	
	Route::resource('calenders','CalendersController');
			Route::get('calender/{id}/destroy', [
				'uses' => 'CalendersController@destroy',
				'as' => 'admin.calenders.destroy'
			]);

	Route::resource('utilities','UtilitiesController');
			Route::get('utilities/{id}/destroy', [
				'uses' => 'UtilitiesController@destroy',
				'as' => 'admin.utilities.destroy'
			]);
	Route::resource('projects','ProjectsController');
			Route::get('projects/{id}/destroy', [
				'uses' => 'ProjectsController@destroy',
				'as' => 'admin.projects.destroy'
			]);
});


Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');


