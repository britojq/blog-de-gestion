
@extends('admin.template.main')

@section('title', 'Pagina Principal del Proyecto')

@section('content')

<h3 class="title-front left"> Ultimos Articulos</h3>
<div class="row">
	<div class="col-md-8">
		<div class="row">
			
			<div class="col-md-6">
				<div class="panel panel-default">
					<a href="#" class="thumbnail" >
						<img class="img-responsive img-article" src="" alt="...">
					</a>
					<h3 class="text-center">Titulo de este articulo</h3>
					<br>
					<i class="fa fa-holder-open-o"></i> <a href="">Category</a>
					<div class="pull-right">
						<i class="fa fa-clock-o"> Hace 3 Minutos</i>
					</div>
				</div>
			</div>


		</div>	
	</div>
</div>              

@endsection