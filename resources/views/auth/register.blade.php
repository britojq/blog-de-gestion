<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <!-- Favicons -->
    <link rel="apple-touch-icon" href="{{ asset('plugins/auth/assets/img/kit/free/apple-icon.png') }}">
    <link rel="icon" href="{{ asset('plugins/auth/assets/img/kit/free/favicon.png') }}">
    <title>
        Registrarse - Sistema
    </title>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <link rel="stylesheet" href="{{ asset('plugins/auth/assets/css/material-kit.css?v=2.0.2') }}">
    
</head>

<body class="signup-page ">
    <nav class="navbar  navbar-transparent    navbar-absolute  navbar-expand-lg " color-on-scroll="100" id="sectionsNav">
        <div class="container">
            <div class="navbar-translate">
                <a class="navbar-brand" href="/">Blog </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    <span class="navbar-toggler-icon"></span>
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" rel="tooltip" title="" data-placement="bottom" href="/" data-original-title="Volver al Blog">
                            <i class="fa fa-home"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" rel="tooltip" title="" data-placement="bottom" href="login" data-original-title="Iniciar Sesion">
                            <i class="fa fa-user-circle"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="page-header header-filter" filter-color="purple" style="background-image: url(&apos;plugins/auth/assets/img/kit/entrada.jpg&apos;); background-size: cover; background-position: top center;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 ml-auto mr-auto">
                    <div class="card card-signup">
                        <h2 class="card-title text-center">Registrar Usuario</h2>
                        
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6 ml-auto">
                                    <div class="info info-horizontal">
                                        <div class="icon icon-rose">
                                            <i class="fa fa-line-chart"></i>
                                        </div>
                                        <div class="description">
                                            <h4 class="info-title">Proyectos</h4>
                                            <p class="description">
                                                Podras administrar y gestionar los proyectos. Hacer seguimiento al personal con sus respectivas tareas asignadas.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="info info-horizontal">
                                        <div class="icon icon-info">
                                            <i class="fa fa-users"></i>
                                        </div>
                                        <div class="description">
                                            <h4 class="info-title">Gestion</h4>
                                            <p class="description">
                                                Guardias, Permisos, Vacaciones, Sobretiempos, Viaticos, Asistencia. Cada analista podra gestionar sus necesidades
                                            </p>
                                        </div>
                                    </div>
                                    <div class="info info-horizontal">
                                        <div class="icon icon-primary">
                                            <i class="fa fa-server"></i>
                                        </div>
                                        <div class="description">
                                            <h4 class="info-title">Monitoreo</h4>
                                            <p class="description">
                                                Realiza un seguimiento en tiempo real de los servicios corporativos.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 mr-auto">
                                   <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                                    {{ csrf_field() }}
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-user-o"></i>
                                                    </span>
                                                </div>
                                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus placeholder="Escriba su Nombre...">
                                                @if ($errors->has('name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-user-o"></i>
                                                    </span>
                                                </div>
                                                <input id="lastname" type="text" class="form-control" name="lastname" value="{{ old('lastname') }}" required autofocus placeholder="Escriba su Apellido...">

                                                @if ($errors->has('lastname'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('lastname') }}</strong>
                                                    </span>
                                                @endif
                                                
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-envelope-o"></i>
                                                    </span>
                                                </div>
                                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="Escriba su Correo Electronico...">

                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-lock"></i>
                                                    </span>
                                                </div>
                                                <input id="password" type="password" class="form-control" name="password" required placeholder="Introduzca su Clave...">

                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <br>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-lock"></i>
                                                    </span>
                                                </div>
                                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Confirmar su Clave...">
                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        
                                        <div class="pull-right margin-top-20">
                                            <a class="btn btn-primary btn btn-secondary" href="{{ route('front.index') }}" role="button">Regresar</a>     
                                            
                                            <button type="reset" class="btn btn-secondary">Resetear
                                                <i class="fa fa-refresh position-right"></i>
                                            </button>

                                            <button type="submit" class="btn btn-primary">Registrar
                                                <i class="fa fa-arrow-right position-right"></i>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer ">
            <div class="container">
                <nav class="pull-left">
                    <ul>
                        <li>
                            <a href="#">
                                Inicio
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Acerca de
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Licencia
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright pull-right">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>, 
                    <a href="#">Jose A. Brito H.</a> diseñado para una mejor gestion.
                </div>
            </div>
        </footer>
    </div>
    
</body>

</html>