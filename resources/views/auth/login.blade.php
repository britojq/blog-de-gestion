<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <!-- Favicons -->
    <link rel="apple-touch-icon" href="{{ asset('plugins/auth/assets/img/kit/free/apple-icon.png') }}">
    <link rel="icon" href="{{ asset('plugins/auth/assets/img/kit/free/favicon.png') }}">
    <title>
        Registrarse - Sistema
    </title>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <link rel="stylesheet" href="{{ asset('plugins/auth/assets/css/material-kit.css?v=2.0.2') }}">
    
</head>

<body class="signup-page ">
    <nav class="navbar  navbar-transparent    navbar-absolute  navbar-expand-lg " color-on-scroll="100" id="sectionsNav">
        <div class="container">
            <div class="navbar-translate">
                <a class="navbar-brand" href="/">Blog </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    <span class="navbar-toggler-icon"></span>
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" rel="tooltip" title="" data-placement="bottom" href="/" data-original-title="Volver al Blog">
                            <i class="fa fa-home"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" rel="tooltip" title="" data-placement="bottom" href="register" data-original-title="Registrarse">
                            <i class="fa fa-user-plus"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="page-header header-filter" data-parallax="true" style=" background-image: url('plugins/auth/assets/img/kit/entrada.jpg'); ">
        <div class="container">
            <div class="row">
                <div class="col-md-4 ml-auto mr-auto">
                    <div class="card card-signup">
                        <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                            <div class="card-header card-header-primary text-center">
                            <h4>Ingreso al Sistema</h4>
                                <div class="social-line">
                                    
                                    <a href="#" class="btn btn-link btn-just-icon">
                                        <i class="fa fa-line-chart"></i>
                                    </a>
                                    <a href="#" class="btn btn-link btn-just-icon">
                                        <i class="fa fa-users"></i>
                                    </a>
                                    <a href="#" class="btn btn-link btn-just-icon">
                                        <i class="fa fa-server"></i>
                                    </a>
                                </div>
                            </div>
                            <br>
                            <p class="text-divider">Escriba sus datos para Ingresar al Sistema</p>
                            <br>
                            <div class="card-body">
                                <div class="input-group">
                                    
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="Escriba su Correo Electronico...">
                                    <span class="input-group-addon">
                                        <i class="fa fa-envelope-o"></i>
                                    </span>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="input-group">
                                    
                                    <input id="password" type="password" class="form-control" name="password" required placeholder="Introduzca su Clave...">
                                    <span class="input-group-addon">
                                        <i class="fa fa-lock"></i>
                                    </span>

                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>

                            </div>
                            <br>
                            <div class="margin-top-20">
                                <center>
                                    <button type="submit" class="btn btn-primary btn-xs">Ingresar
                                    </button>
                                </center>
                            </div>
                            <br>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>