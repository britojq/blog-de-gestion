
@extends('admin.template.calendar')

@section('title', 'Agregar tarea') 
    
@section('content')

  {!! Form::open(['route' => 'tasks.store', 'metod' => 'POST']) !!}
    
    <div class="form-group">
      {!! Form::Label('name', 'Tarea', ['class' => 'col-sm-2 control-label']) !!}
    </div>  
     
     <div class="col-sm-10">
      {!! Form::Text('name', null, ['class' => 'form-control', 'placeholder' => 'Escriba la Tarea', 'required']) !!}
    
    </div>

    <div class="form-group">
      {!! Form::Label('description', 'Descripcion de la Tarea', ['class' => 'col-sm-2 control-label']) !!}
    </div>  
     
     <div class="col-sm-10">
      {!! Form::Textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Descripcion de la Tarea', 'required']) !!} 
    </div>

    <div class="form-group">
      {!! Form::Label('start_date', 'Fecha Inicio', ['class' => 'col-sm-2 control-label']) !!}
    </div>  
     
    <div class="col-sm-10">
        {!! Form::Text('start_date', null, ['class' => 'form-control datepicker', 'required']) !!}
    </div>

    <div class="form-group">
      {!! Form::Label('end_date', 'Fecha Fin', ['class' => 'col-sm-2 control-label']) !!}
    </div>  
     
    <div class="col-sm-10">
        {!! Form::Text('end_date', null, ['class' => 'form-control datepicker', 'required']) !!}
    </div>
      
      <button type="submit" class="btn btn-default btn-primary">Enviar</button>
                
  {!! Form::close() !!}


  <script>
    $('.datepicker').datepicker({
        format: "yyyy/mm/dd",
        language: "es",
        autoclose: true
    });
  </script>

@endsection