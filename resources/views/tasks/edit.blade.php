
@extends('admin.template.calendar')

@section('title', 'Agregar tarea') 
    
@section('content')
  
  {!! Form::open(['action' => ['TasksController@update', $tasks], 'method' => 'PUT']) !!}
  
      <div class="form-group">
      {!! Form::Label('name', 'Nombre', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
        {!! Form::Text('name', $tasks->name, ['class' => 'form-control', 'placeholder' => 'Nombre', 'required']) !!}
        </div>  
      </div>
      

      <div class="form-group">
      {!! Form::Label('name', 'Descripcion', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
        {!! Form::Textarea('name', $tasks->description, ['class' => 'form-control', 'placeholder' => 'Descripcion', 'required']) !!}
        </div>  
      </div>

      <div class="form-group">
        {!! Form::Label('task_date', 'Fecha', ['class' => 'col-sm-2 control-label']) !!} 
      <div class="col-sm-10">
          {!! Form::Text('task_date', null, ['class' => 'form-control datepicker', 'required']) !!}
      </div>
      </div>  
      
      <button type="submit" class="btn btn-default btn-primary">Guardar</button>
 

  {!! Form::close() !!}

 <script>
    $('.datepicker').datepicker({
        format: "yyyy/mm/dd",
        language: "es",
        autoclose: true
    });
  </script>

@endsection