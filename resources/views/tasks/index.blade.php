

@extends('admin.template.calendar')

@section('title', 'CALENDARIO') 
    
@section('content')

            <div id='calendar'></div>
            <script src="{{ asset('plugins/admin/fullcalender/jquery-1.11.3.min.js') }}"></script>
            <script src="{{ asset('plugins/admin/fullcalender/moment.min.js') }}"></script>
            <script src="{{ asset('plugins/fullcalender/fullcalendar.min.js') }}"></script>
            <script src="{{ asset('plugins/admin/fullcalender/locale-all.js') }}"></script>

            <script>
                $(document).ready(function() {
                    // page is now ready, initialize the calendar...
                    $('#calendar').fullCalendar({
                        // put your options and callbacks here
                        locale: 'es',
                        buttonIcons: false,
                        weekNumbers: true,
                        navLinks: true,
                        //editable: true,
                        //eventLimit: true,
                        events : [
                            @foreach($tasks as $task)
                            {
                                title : '{{ $task->name }}',
                                start : '{{ $task->task_date }}',
                                url : '{{ route('tasks.edit', $task->id) }}'
                            },
                            @endforeach
                        ]
                    })
                });
            </script>

        
@endsection