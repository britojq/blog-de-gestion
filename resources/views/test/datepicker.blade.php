<html>
<head>
    <title>Datepicker</title>
 
    <link href="{{ asset('plugins/font/lato.css') }}" rel="stylesheet" type="text/css">
    

    <link href="{{ asset('plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet"> 
    
    <!-- Optional theme -->
    <link rel="stylesheet" href="{{asset('plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet')}}">
    
    <!-- Latest compiled and minified JavaScript -->
    <script src="{{asset('plugins/bootstrap/js/bootstrap.min.js" rel="stylesheet')}}"></script>
    
    <!-- Jquery -->
    <script src="{{asset('plugins/jquery/js/jquery-1.11.3.min.js')}}"></script>
    
    <!-- Datepicker Files -->
    <link rel="stylesheet" href="{{asset('plugins/datepicker/css/bootstrap-datepicker3.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/datepicker/css/bootstrap-standalone.css')}}">
    <script src="{{asset('plugins/datepicker/js/bootstrap-datepicker.js')}}"></script>
    
    <!-- Languaje -->
    <script src="{{asset('plugins/datepicker/locales/bootstrap-datepicker.es.min.js')}}"></script>
 
</head>
<body>
<div class="container">
    <div class="content">
 
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="col-md-4 col-md-offset-4">
{!! Form::open(['route' => 'save-date', 'method' => 'POST']) !!}
                   <!-- <form action="test/save" method="post"> -->
                        <div class="form-group">
                            <label for="date">Fecha</label>
                            <div class="input-group">
                                <input type="text" class="form-control datepicker" name="date">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-default btn-primary">Enviar</button>
                    <!-- </form> -->
 {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
 
<script>
    $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        language: "es",
        autoclose: true
    });
</script>
</body>
</html>