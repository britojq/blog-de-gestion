<!doctype html>

<html lang="es">

<head>

    <script src="{{ asset('plugins/admin/assets/js/jquery-3.2.1.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/admin/assets/css/bootstrap.min.css') }}">
<!-- -->
    <script src="{{ asset('plugins/admin/fullcalender/moment.min.js') }}"></script>

    <script src="{{ asset('plugins/admin/fullcalender/fullcalendar.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('plugins/admin/fullcalender/fullcalendar.min.css') }}"/>

</head>

<body>



<div class="container">
    <div class="row">
        <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Calendario 
                    </div>

                    <div class="panel-body" >
                        {!! $calendar->calendar() !!}
                        {!! $calendar->script() !!}
                    </div>

                </div>
            
        </div>
    </div>
</div>

</body>

</html>