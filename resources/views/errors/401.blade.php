
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> | Sistema </title>
    <link rel="stylesheet" href="{{asset('plugins/chosen/chosen.css') }}">
    <link rel="stylesheet" href="{{asset('plugins/trumbowyg/ui/trumbowyg.css') }}">
    <link href="{{ asset('plugins/css/app.css') }}" rel="stylesheet"> 
</head>
<body>
<div class="box-admin">
	<div class="col-md-4 col-md-offset-4">
		<div class="panel panel-warning">
			
            <div class="panel-heading">
                <div class="panel-title"> Acceso Restringido </div>
            </div>

			<div class="panel-body">
				<a href="{{ route ('front.index') }}"> Volver a la Pagina Principal</a>
			</div>
		
        </div>
	</div>	
</div>

</body>
</html>