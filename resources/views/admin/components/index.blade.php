@extends('admin.template.main')

@section('title', 'Panel de Complementos')

@section('paneladmin')
    class="active"
@endsection

@section('content')

<!-- CONTENIDO -->
            <div class="content">
                <div class="container-fluid">
                    <!-- BLOQUE -->
                    <div class="card card-nav-tabs">
                        <div class="card-header" data-background-color="purple" style="height: 6rem;">
                            <h5>ADMINISTRACION</h5>
                        </div>
                        <div class="card-body ">
                            <div class="tab-content text-center">
                                <br>
                                <div class="row col-lg-12">
                                    <div class="col-lg-2 col-md-4 col-sm-4">
                                        <div class="card card-stats">
                                            <div class="card-header" data-background-color="green">
                                                <i class="fa fa-user-plus"></i>
                                            </div>
                                            <div class="card-footer">
                                                <div class="stats">
                                                    <a href="{{ route('users.index') }}">USUARIOS</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-4 col-sm-4">
                                        <div class="card card-stats">
                                            <div class="card-header" data-background-color="green">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <div class="card-footer">
                                                <div class="stats">
                                                    <a href="{{ route('guards.index') }}">GUARDIAS</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-4 col-sm-4">
                                        <div class="card card-stats">
                                            <div class="card-header" data-background-color="green">
                                                <i class="fa fa-reorder"></i>
                                            </div>
                                            <div class="card-footer">
                                                <div class="stats">
                                                    <a href="{{ route('vacations.index') }}">VACACIONES</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-4 col-sm-4">
                                        <div class="card card-stats">
                                            <div class="card-header" data-background-color="green">
                                                <i class="fa fa-file-text"></i>
                                            </div>
                                            <div class="card-footer">
                                                <div class="stats">
                                                    <a href="{{ route('permises.index') }}">PERMISOS</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-4 col-sm-4">
                                        <div class="card card-stats">
                                            <div class="card-header" data-background-color="green">
                                                <i class="fa fa-clock-o"></i>
                                            </div>
                                            <div class="card-footer">
                                                <div class="stats">
                                                    <a href="{{ route('overtimes.index') }}">SOBRETIEMPOS</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-4 col-sm-4">
                                        <div class="card card-stats">
                                            <div class="card-header" data-background-color="green">
                                                <i class="fa fa-check-square-o"></i>
                                            </div>
                                            <div class="card-footer">
                                                <div class="stats">
                                                    <a href="{{ route('timetrackings.index') }}">ASISTENCIA</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                        </div>
                    </div>
                    <!-- BLOQUE -->
                </div>
            </div>
            <!-- FIN CONTENIDO -->
            </div>

            <!-- CONTENIDO -->
            <div class="content">
                <div class="container-fluid">
                    <!-- BLOQUE -->
                    <div class="card card-nav-tabs">
                        <div class="card-header" data-background-color="purple" style="height: 6rem;">
                            <h5>ADMINISTRAR BLOG</h5>
                        </div>
                        <div class="card-body ">
                            <div class="tab-content text-center">
                                <br>
                                <div class="row col-lg-12">
                                    <div class="col-lg-2 col-md-4 col-sm-4">
                                        <div class="card card-stats">
                                            <div class="card-header" data-background-color="orange">
                                                <i class="fa fa-wikipedia-w"></i>
                                            </div>
                                            <div class="card-footer">
                                                <div class="stats">
                                                    <a href="{{ route('articles.index') }}">ARTICULOS</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-4 col-sm-4">
                                        <div class="card card-stats">
                                            <div class="card-header" data-background-color="orange">
                                                <i class="fa fa-list-ul"></i>
                                            </div>
                                            <div class="card-footer">
                                                <div class="stats">
                                                    <a href="{{ route('categories.index') }}">CATEGORIAS</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-4 col-sm-4">
                                        <div class="card card-stats">
                                            <div class="card-header" data-background-color="orange">
                                                <i class="fa fa-tags"></i>
                                            </div>
                                            <div class="card-footer">
                                                <div class="stats">
                                                    <a href="{{ route('tags.index') }}">TAGS</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-4 col-sm-4">
                                        <div class="card card-stats">
                                            <div class="card-header" data-background-color="orange">
                                                <i class="fa fa-file-image-o"></i>
                                            </div>
                                            <div class="card-footer">
                                                <div class="stats">
                                                    <a href="{{ route('admin.image.index') }}">IMAGENES</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                        </div>
                    </div>
                    <!-- BLOQUE -->
                </div>
            </div>
            <!-- FIN CONTENIDO -->
            </div>

            
            <!-- CONTENIDO -->
            <div class="content">
                <div class="container-fluid">
                    <!-- BLOQUE -->
                    <div class="card card-nav-tabs">
                        <div class="card-header" data-background-color="purple" style="height: 6rem;">
                            <h5>CONFIGURACIONES ADICIONALES USUARIO</h5>
                        </div>
                        <div class="card-body ">
                            <div class="tab-content text-center">
                                <br>
                                <div class="row col-lg-12">

                                    <div class="col-lg-2 col-md-4 col-sm-4">
                                        <div class="card card-stats">
                                            <div class="card-header" data-background-color="blue">
                                                <i class="fa fa-id-badge"></i>
                                            </div>
                                            <div class="card-footer">
                                                <div class="stats">
                                                    <a href="{{ route('carges.index') }}">CARGOS</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-4 col-sm-4">
                                        <div class="card card-stats">
                                            <div class="card-header" data-background-color="blue">
                                                <i class="fa fa-circle"></i>
                                            </div>
                                            <div class="card-footer">
                                                <div class="stats">
                                                    <a href="{{ route('gerencys.index') }}">GERENCIAS</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-4 col-sm-4">
                                        <div class="card card-stats">
                                            <div class="card-header" data-background-color="blue">
                                                <i class="fa fa-circle-o"></i>
                                            </div>
                                            <div class="card-footer">
                                                <div class="stats">
                                                    <a href="{{ route('divisions.index') }}">DIVISIONES</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-4 col-sm-4">
                                        <div class="card card-stats">
                                            <div class="card-header" data-background-color="blue">
                                                <i class="fa fa-circle-o-notch"></i>
                                            </div>
                                            <div class="card-footer">
                                                <div class="stats">
                                                    <a href="{{ route('units.index') }}">UNIDADES</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-4 col-sm-4">
                                        <div class="card card-stats">
                                            <div class="card-header" data-background-color="blue">
                                                <i class="fa fa-circle-thin"></i>
                                            </div>
                                            <div class="card-footer">
                                                <div class="stats">
                                                    <a href="{{ route('departaments.index') }}">DEPARTAMENTOS</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                            
                        </div>
                    </div>
                    <!-- BLOQUE -->
                </div>
            </div>
            <!-- FIN CONTENIDO -->
            </div>

            <!-- CONTENIDO -->
            <div class="content">
                <div class="container-fluid">
                    
                    <!-- BLOQUE -->
                    <div class="card card-nav-tabs">
                        <div class="card-header" data-background-color="purple" style="height: 6rem;">
                            <h5>COMPLEMENTOS ASOCIADOS A  | GUARDIAS | PERMISOS | VACACIONES</h5>
                        </div>
                        <div class="card-body ">
                            <div class="tab-content text-center">
                            <br>
                                <div class="row col-lg-12">

                                    <div class="col-lg-2 col-md-4 col-sm-4">
                                        <div class="card card-stats">
                                            <div class="card-header" data-background-color="green">
                                                <i class="fa fa-map-marker"></i>
                                            </div>
                                            <div class="card-footer">
                                                <div class="stats">
                                                    <a href="{{ route('locations.index') }}">UBICACIONES</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-4 col-sm-4">
                                        <div class="card card-stats">
                                            <div class="card-header" data-background-color="green">
                                                <i class="fa fa-barcode"></i>
                                            </div>
                                            <div class="card-footer">
                                                <div class="stats">
                                                    <a href="{{ route('codenomines.index') }}">CODIGOS DE NOMINA</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-4 col-sm-4">
                                        <div class="card card-stats">
                                            <div class="card-header" data-background-color="green">
                                                <i class="fa fa-user-circle"></i>
                                            </div>
                                            <div class="card-footer">
                                                <div class="stats">
                                                    <a href="{{ route('typenomines.index') }}">TIPOS DE NOMINA</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-4 col-sm-4">
                                        <div class="card card-stats">
                                            <div class="card-header" data-background-color="green">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <div class="card-footer">
                                                <div class="stats">
                                                    <a href="{{ route('nomineareas.index') }}">AREAS DE NOMINA</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-4 col-sm-4">
                                        <div class="card card-stats">
                                            <div class="card-header" data-background-color="green">
                                                <i class="fa fa-bars"></i>
                                            </div>
                                            <div class="card-footer">
                                                <div class="stats">
                                                    <a href="{{ route('unitcodes.index') }}">CODIGOS DE UNIDAD</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-4 col-sm-4">
                                        <div class="card card-stats">
                                            <div class="card-header" data-background-color="green">
                                                <i class="fa fa-calculator"></i>
                                            </div>
                                            <div class="card-footer">
                                                <div class="stats">
                                                    <a href="{{ route('cecos.index') }}">CENTROS DE COSTO</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-4 col-sm-4">
                                        <div class="card card-stats">
                                            <div class="card-header" data-background-color="green">
                                                <i class="fa fa-calendar-o"></i>
                                            </div>
                                            <div class="card-footer">
                                                <div class="stats">
                                                    <a href="{{ route('journalcodes.index') }}">CPDIGOS DE JORNADA</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-4 col-sm-4">
                                        <div class="card card-stats">
                                            <div class="card-header" data-background-color="green">
                                                <i class="fa fa-edit"></i>
                                            </div>
                                            <div class="card-footer">
                                                <div class="stats">
                                                    <a href="{{ route('permisetypes.index') }}">TIPOS DE PERMISO</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2 col-md-4 col-sm-4">
                                        <div class="card card-stats">
                                            <div class="card-header" data-background-color="green">
                                                <i class="fa fa-pencil-square"></i>
                                            </div>
                                            <div class="card-footer">
                                                <div class="stats">
                                                    <a href="{{ route('permisemotives.index') }}">MOTIVOS DE PERMISO</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                            
                        </div>
                    </div>
                    <!-- BLOQUE -->
                </div>
            </div>
            <!-- FIN CONTENIDO -->
        </div>


@endsection