@extends('admin.template.main')

@section('blog')
    class="active"
@endsection

@section('title', 'Listado de Articulos')

@section('content')
<div class="content">
  <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header" data-background-color="purple">
              <h4 class="title">Listado de Articulos</h4>
            </div>
            <div class="card-content">
              <!--  Inicio del contenido -->

              <a href="{{ route('articles.create') }}" class="btn btn-primary">Registrar un Articulo</a>
               @include('flash::message')
               @include('admin.template.partials.errors')

              <!-- BUSCADOR DE articulos-->
              {!! Form::open(['route' => 'articles.index', 'method' => 'GET', 'class' => 'nav-form pull-right']) !!}

                <div class="input-group">
                    {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Buscador de Articulos', 'aria-describedby' => 'search']) !!}
                 <span class="input-group-addon" id="search" >
                    <span class="fa fa-search" id="search" aria-hidden="true"></span>
                  </span>
                </div>

              {!! Form::close() !!}

              <!-- FIN BUSCADOR DE articulos-->


              <table class="table table-striped">
                <!-- Encabezado de la Tabla -->
                <thead>
                    <th>ID</th>
                    <th>Titulo</th>
                    <th>Categoria</th>
                    <td>Usuario</td>
                    <td>Accion</td>
                </thead>
                <thead>
                <!-- Cuerpo de la Tabla -->
                <tbody>
                    @foreach($articles as $article)
                        <tr>
                          <td>{{ $article->id }}</td>
                          <td>{{ $article->title }}</td>
                          <td>{{ $article->category->name }}</td>
                          <td>{{ $article->user->name }}</td>
                          <td>
                            <!-- Botones Editar / Borrar -->
                            <a href="{{ route('articles.edit', $article->id) }}" class="fa fa-pencil"> </a> | <a href="{{ route('admin.articles.destroy', $article->id) }}" onclick="return confirm('Segruro deseas Borrarlo')" class="fa fa-trash"></a>
                          </td>
                        </tr>
                      @endforeach
                  </tbody>
              </table>
              
            <div class="text-center">
              {!! $articles->render() !!}
            </div>
            <!--  Fin del Contenido -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection