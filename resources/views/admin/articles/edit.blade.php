@extends('admin.template.main')

@section('title', 'Editar Articulo | ' . $article->title) 

@section('blog')
    class="active"
@endsection

@section('content')
	@include('admin.template.partials.errors')
	{!! Form::open(['route' => ['articles.update', $article], 'method' => 'PUT']) !!}

		<div class="form-group">
			{!! Form::Label('title', 'Titulo del Articulo', ['class' => 'col-sm-3 control-label']) !!}
		 
			{!! Form::Text('title', $article->title, ['class' => 'form-control', 'placeholder' => 'Titulo del Articulo', 'required']) !!}
		</div>
				
		<div class="form-group">
			{!! Form::Label('category_id', 'Categoria') !!}
		
			{!! Form::Select('category_id', $categories, $article->category->id, ['class' => 'form-control select-category', 'required']) !!}
		</div>

		<div class="form-group">
			{!! Form::Label('content', 'Contenido', ['class' => 'col-sm-3 control-label']) !!}
		 
			{!! Form::TextArea('content', $article->content, ['class' => 'form-control textarea-content']) !!}
		</div>

		<div class="form-group">
			{!! Form::Label('tags', 'Tags') !!}
		
			{!! Form::Select('tags[]', $tags, $my_tags, ['class' => 'form-control select-tag', 'multiple', 'required']) !!}
		</div>

		<div class="form-group">
		{!! Form::Submit('Editar Articulo', ['class' => 'btn btn-primary']) !!}
		</div>

	{!! Form::close() !!}

@endsection

@section('js')
	<script>
		$('.select-tag').chosen({
			placeholder_text_multiple: 'Seleccione un maximo de 3 Tags', max_selected_options: 3, no_results_text: "No se encuentra este tags"
		});

		$('.select-category').chosen({
			placeholder_text_single: 'Seleccione una Categoria', no_results_text: "No se encuentra esta Categoria"
		});
	
		$('.textarea-content').trumbowyg({
			lang: 'es',
		});

	</script>

@endsection