@extends('admin.template.main')

@section('title', 'Agregar Articulo') 

@section('blog')
    class="active"
@endsection

@section('content')
	@if (session('status'))
    	<div class="alert alert-success">
        	{{ session('status') }}
    	</div>
    @endif

<div class="content">
	<div class="container-fluid">
	<!-- PAGE CONTENT -->
        <div class="row">
        <!-- ALERTS & ERRORS -->
        @include('flash::message')
        @include('admin.template.partials.errors')
        <!-- END ALERTS & ERRORS -->

        {!! Form::open(['route' => 'articles.store', 'method' => 'POST', 'files' => true]) !!}
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                    <h4 class="title">Agregar Articulo al Blog</h4>
                    </div>

                    <div class="card-content">
                    	<div class="row">

							<div class="form-group">
								{!! Form::Label('title', 'Titulo del Articulo', ['class' => 'col-sm-3 control-label']) !!}
							 
								{!! Form::Text('title', null, ['class' => 'form-control', 'placeholder' => 'Titulo del Articulo', 'required']) !!}
							</div>
				
							<div class="form-group">
								{!! Form::Label('category_id', 'Categoria') !!}
							
								{!! Form::Select('category_id', $categories, null, ['class' => 'form-control select-category', 'required']) !!}
							</div>

							<div class="form-group">
								{!! Form::Label('content', 'Contenido', ['class' => 'col-sm-3 control-label']) !!}
							 
								{!! Form::TextArea('content', null, ['class' => 'form-control textarea-content']) !!}
							</div>

							<div class="form-group">
								{!! Form::Label('tags', 'Tags') !!}
							
								{!! Form::Select('tags[]', $tags, null, ['class' => 'form-control select-tag', 'multiple', 'required']) !!}
							</div>

							<div class="form-group">
								{!! Form::Label('image', 'Imagen') !!}
							
								{!! Form::file('image') !!}
							</div>

							<div class="form-group">
						
							{!! Form::Submit('Agregar Articulo', ['class' => 'btn btn-primary']) !!}
						</div>
					</div>
				</div>
			</div>

		{!! Form::close() !!}
		</div>

@endsection

@section('js')
	<script>
		$('.select-tag').chosen({
			placeholder_text_multiple: 'Seleccione un maximo de 3 Tags', max_selected_options: 3, no_results_text: "No se encuentra este tags"
		});

		$('.select-category').chosen({
			placeholder_text_single: 'Seleccione una Categoria', no_results_text: "No se encuentra esta Categoria"
		});
	
		$('.textarea-content').trumbowyg({
			lang: 'es',
		});

	</script>

@endsection