@extends('admin.template.maintimepicker')

@section('title', 'Agregar Sobretiempo') 

@section('solicitudes')
    class="active"
@endsection

@section('content')

<div class="content">
      <div class="container-fluid">
	<!-- PAGE CONTENT -->
	<!-- ALERTS & ERRORS -->
        @include('flash::message')
        @include('admin.template.partials.errors')
        <!-- END ALERTS & ERRORS -->
        <div class="row">
        {!! Form::open(['route' => 'overtimes.store', 'method' => 'POST', 'files' => true]) !!}
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                    <h4 class="title">Cargar Sobretiempo</h4>
                    </div>
                    <div class="card-content">
                        <div class="row">
		<div class="form-group">
			{!! Form::Label('location_id', 'Lugar de Solicitud', ['class' => 'col-sm-2 control-label']) !!}
		 	<div class="col-sm-4">
				{!! Form::Select('location_id', $locations, null, ['class' => 'form-control select-locations', 'placeholder' => 'Selecione una Opcion..',  'required']) !!}
			</div>	
			
			{!! Form::Label('currentdate', 'Fecha de Solicitud', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
					{!! Form::Date('currentdate',  null, ['class' => 'form-control datetimepickerF', 'placeholder' => 'seleccione', 'required']) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
			</div>	
		</div>
</div>
<!-- -->
<br>
<!--  -->
 <div class="row">
		<div class="form-group">
			{!! Form::Label('ceco_id', 'Centro de Costo', ['class' => 'col-sm-2 control-label']) !!}
		 	<div class="col-sm-4">
				{!! Form::Select('ceco_id', $cecos, null, ['class' => 'form-control select-locations', 'placeholder' => 'Selecione una Opcion..',  'required']) !!}
			</div>	
			
			{!! Form::Label('gerency_id', 'Gerencia', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
					{!! Form::Select('gerency_id', $gerencys, null, ['class' => 'form-control select-locations', 'placeholder' => 'Selecione una Opcion..',  'required']) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
			</div>	
		</div>
</div>
<!-- -->
<br>
<!--  -->
 <div class="row">
		<div class="form-group">
			{!! Form::Label('unit_id', 'Unidad', ['class' => 'col-sm-2 control-label']) !!}
		 	<div class="col-sm-4">
				{!! Form::Select('unit_id', $units, null, ['class' => 'form-control select-locations', 'placeholder' => 'Selecione una Opcion..',  'required']) !!}
			</div>	
			
			{!! Form::Label('division_id', 'Division', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
					{!! Form::Select('division_id', $divisions, null, ['class' => 'form-control select-locations', 'placeholder' => 'Selecione una Opcion..',  'required']) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
			</div>	
		</div>
</div>
<!-- -->
<br>

<!--  -->
 <div class="row">
		<div class="form-group">
			{!! Form::Label('departament_id', 'Departamento', ['class' => 'col-sm-2 control-label']) !!}
		 	<div class="col-sm-4">
				{!! Form::Select('departament_id', $departaments, null, ['class' => 'form-control select-locations', 'placeholder' => 'Selecione una Opcion..',  'required']) !!}
			</div>	
			
			{!! Form::Label('nominearea_id', 'Area de Nomina', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
					{!! Form::Select('nominearea_id', $nomineareas, null, ['class' => 'form-control select-nomineareas', 'placeholder' => 'Selecione una Opcion..',  'required']) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
			</div>	
		</div>
</div>
<!-- -->
<br>

<!-- -->
	<div class="row">	
		<div class="form-group">
 			{!! Form::Label('start_date1', 'Fecha Inicio:', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
				 		{!! Form::Date('start_date1',  null, ['class' => 'form-control datetimepickerF', 'placeholder' => 'seleccione', 'required']) !!}
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
				</div>

 			{!! Form::Label('end_date1', 'Fecha Culminacion:', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
				 		{!! Form::Date('end_date1',  null, ['class' => 'form-control datetimepickerF', 'placeholder' => 'seleccione', 'required']) !!}
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
				</div>	
		</div>
	</div>
<!-- -->
<br>

<!-- 
Auth::user()->lastname 
-->
 <div class="row">
		<div class="form-group">
			{!! Form::Label('name', 'Cedula', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
						{!! Form::Text('name', Auth::user()->cedula, null, ['class' => 'form-control', 'placeholder' => 'Escriba Nombre', 'required', 'disabled']) !!}
						
					</div>
				</div>
		
			{!! Form::Label('lastname', 'Numero Personal', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
						{!! Form::Text('lastname',  Auth::user()->personalnumber, null, ['class' => 'form-control', 'placeholder' => 'Escriba Apellido', 'required', 'disabled']) !!}
						
					</div>
				</div>	
		</div>
</div>
<!-- -->
<br>
<!-- 
Auth::user()->lastname 
-->
 <div class="row">
		<div class="form-group">
			{!! Form::Label('name', 'Nombre', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
						{!! Form::Text('name', Auth::user()->name, null, ['class' => 'form-control', 'placeholder' => 'Escriba Nombre', 'required', 'disabled']) !!}
						
					</div>
				</div>
		
			{!! Form::Label('lastname', 'Apellido', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
						{!! Form::Text('lastname',  Auth::user()->lastname, null, ['class' => 'form-control', 'placeholder' => 'Escriba Apellido', 'required', 'disabled']) !!}
						
					</div>
				</div>	
		</div>
</div>
<!-- -->
<br>
<!-- -->
 <div class="row">	
		<div class="form-group">
			{!! Form::Label('dateofwork', 'Fecha Trabajo:', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
				 		{!! Form::Date('dateofwork',  null, ['class' => 'form-control datetimepickerF', 'placeholder' => 'seleccione', 'required']) !!}
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
				</div>
		
			{!! Form::Label('dayofweek', 'Dia de la Semana', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
						{!! Form::select('type', ['lunes' => 'Lunes', 'martes' => 'Martes', 'miercoles' => 'Miercoles', 'jueves' => 'Jueves', 'viernes' => 'Viernes', 'sabado' => 'Sabado', 'domingo' => 'Domingo'], null, ['class' => 'form-control', 'placeholder' => 'Selecione una Opcion..', 'required']) !!}
					</div>
				</div>	
		</div>
</div>
<!-- -->
<br>

<!-- -->
	<div class="row">	
		<div class="form-group">
 			{!! Form::Label('start_hour', 'Hora Inicio:', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
				 	<div class="input-group">
				 		{!! Form::Time('start_hour',  null, ['class' => 'form-control datetimepickerT', 'placeholder' => 'seleccione', 'id' => 'timepicker', 'required']) !!}
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
				</div>

				{!! Form::Label('end_hour', 'Hora Fin', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
				 		{!! Form::Time('end_hour',  null, ['class' => 'form-control datetimepickerT', 'placeholder' => 'seleccione', 'id' => 'timepicker2', 'required']) !!}
						
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
				</div>
						
		</div>
	</div>
<!-- -->
<br>
<!--  -->
 <div class="row">
		<div class="form-group">
			{!! Form::Label('totalhours', 'Total de Horas', ['class' => 'col-sm-2 control-label']) !!}
		 	<div class="col-sm-4">
				{!! Form::Select('totalhours', $departaments, null, ['class' => 'form-control select-locations', 'placeholder' => 'Selecione una Opcion..',  'required']) !!}
			</div>	
			
			{!! Form::Label('codenomine_id', 'Codigo de Nomina', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
					{!! Form::Select('codenomine_id', $codenomines, null, ['class' => 'form-control select-locations', 'placeholder' => 'Selecione una Opcion..',  'required']) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
			</div>	
		</div>
</div>
<!-- -->
<br>

<!-- -->
	<div class="row">	
		<div class="form-group">
 			{!! Form::Label('justification', 'Justificacion:', ['class' => 'col-sm-2 control-label']) !!}
			 	
			 		<div class="input-group">
				 		{!! Form::TextArea('justification',  null, ['class' => 'form-control', 'placeholder' => 'seleccione', 'rows' => '5' , 'required', ]) !!}
					</div>
				

 			
		</div>
	</div>
<!-- -->

<br>
<center><div class="form-group">
		{!! Form::Submit('Registrar Sobretiempo', ['class' => 'btn btn-primary']) !!}
</div></center>
<br>
	{!! Form::close() !!}

	
	

@endsection

