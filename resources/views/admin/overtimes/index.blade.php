@extends('admin.template.main')

@section('title', 'Listado de Sobretiempos')

@section('solicitudes')
    class="active"
@endsection

@section('content')

<div class="content">
  <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header" data-background-color="purple">
              <h4 class="title">Listado de Sobretiempos</h4>
            </div>
            <div class="card-content">
              <!--  Inicio del contenido -->

              <a href="{{ route('overtimes.create') }}" class="btn btn-primary">Registrar Sobretiempo</a>
              @include('flash::message')


            <!--  Inicio del contenido -->
  
    
      <table class="table table-striped">
          <!-- Encabezado de la Tabla -->
          <thead>
              <th>ID</th>
              <th>Nombre</th>
              <th>Apellido</th>
              <th>Fecha del Sobretiempo</th>
              <th>Total Horas Trabajadas</th>
              <th>Accion</th>
          </thead>
          
          <!-- Cuerpo de la Tabla -->
          <tbody>
              @foreach($overtimes as $overtime)
                  <tr>
                    <td>{{ $overtime->id }}</td>
                    <td>{{ $overtime->user->name }}</td>
                    <td>{{ $overtime->user->lastname }}</td>
                    <td>{{ \Carbon\Carbon::parse($overtime->dateofwork)->format('d/m/Y')}}</td>
                    <td>{{ $overtime->totalhours }}</td>
                    <td>
                      <!-- Botones Editar / Borrar -->
                      <a href="{{ route('overtimes.edit', $overtime->id) }}" class="fa fa-pencil"> </a> | <a href="{{ route('admin.overtimes.destroy', $overtime->id) }}" onclick="return confirm('Segruro deseas Borrarlo')" class="fa fa-trash"></a>
                    </td>
                  </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-center">
              {!! $overtimes->render() !!}
        </div>
    <!--  Fin del Contenido -->
  </div>
</div>
</div>
</div>
</div>
</div>

@endsection