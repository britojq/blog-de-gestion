@extends('admin.template.maintimepicker')

@section('title', 'Agregar Sobretiempo') 

@section('solicitudes')
    class="active"
@endsection

@section('content')


<div class="content">
      <div class="container-fluid">
	<!-- PAGE CONTENT -->
	<!-- ALERTS & ERRORS -->
        @include('flash::message')
        @include('admin.template.partials.errors')
        <!-- END ALERTS & ERRORS -->
        <div class="row">
        {!! Form::open(['route' => ['overtimes.update', $overtimes], 'method' => 'PUT']) !!}
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                    <h4 class="title">Editar Sobretiempo</h4>
                    </div>
                    <div class="card-content">
                        <div class="row">
	

		<div class="form-group">
			{!! Form::Label('location_id', 'Lugar de Solicitud', ['class' => 'col-sm-2 control-label']) !!}
		 	<div class="col-sm-4">
				{!! Form::Select('location_id', $locations, $overtimes->location_id, ['class' => 'form-control select-locations', 'placeholder' => 'Selecione una Opcion..',  'required']) !!}
			</div>	
			
			{!! Form::Label('currentdate', 'Fecha de Solicitud', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
					{!! Form::Date('currentdate',  $overtimes->currentdate, ['class' => 'form-control datetimepickerF', 'placeholder' => 'seleccione', 'required']) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
			</div>	
		</div>
</div>
<!-- -->
<br>
<!--  -->
 <div class="row">
		<div class="form-group">
			{!! Form::Label('ceco_id', 'Centro de Costo', ['class' => 'col-sm-2 control-label']) !!}
		 	<div class="col-sm-4">
				{!! Form::Select('ceco_id', $cecos, $overtimes->ceco_id, ['class' => 'form-control select-locations', 'placeholder' => 'Selecione una Opcion..',  'required']) !!}
			</div>	
			
			{!! Form::Label('gerency_id', 'Gerencia', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
					{!! Form::Select('gerency_id', $gerencys, $overtimes->gerency_id, ['class' => 'form-control select-locations', 'placeholder' => 'Selecione una Opcion..',  'required']) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
			</div>	
		</div>
</div>
<!-- -->
<br>
<!--  -->
 <div class="row">
		<div class="form-group">
			{!! Form::Label('unit_id', 'Unidad', ['class' => 'col-sm-2 control-label']) !!}
		 	<div class="col-sm-4">
				{!! Form::Select('unit_id', $units, $overtimes->unit_id, ['class' => 'form-control select-locations', 'placeholder' => 'Selecione una Opcion..',  'required']) !!}
			</div>	
			
			{!! Form::Label('division_id', 'Division', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
					{!! Form::Select('division_id', $divisions, $overtimes->division_id, ['class' => 'form-control select-locations', 'placeholder' => 'Selecione una Opcion..',  'required']) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
			</div>	
		</div>
</div>
<!-- -->
<br>

<!--  -->
 <div class="row">
		<div class="form-group">
			{!! Form::Label('departament_id', 'Departamento', ['class' => 'col-sm-2 control-label']) !!}
		 	<div class="col-sm-4">
				{!! Form::Select('departament_id', $departaments, $overtimes->departament_id, ['class' => 'form-control select-locations', 'placeholder' => 'Selecione una Opcion..',  'required']) !!}
			</div>	
			
			{!! Form::Label('nominearea_id', 'Area de Nomina', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
					{!! Form::Select('nominearea_id', $nomineareas, $overtimes->nominearea_id, ['class' => 'form-control select-locations', 'placeholder' => 'Selecione una Opcion..',  'required']) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
			</div>	
		</div>
</div>
<!-- -->
<br>

<!-- -->
	<div class="row">	
		<div class="form-group">
 			{!! Form::Label('start_date1', 'Fecha Inicio:', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
				 		{!! Form::Date('start_date1',  $overtimes->start_date1, ['class' => 'form-control datetimepickerF', 'placeholder' => 'seleccione', 'required']) !!}
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
				</div>

 			{!! Form::Label('end_date1', 'Fecha Culminacion:', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
				 		{!! Form::Date('end_date1',  $overtimes->end_date1, ['class' => 'form-control datetimepickerF', 'placeholder' => 'seleccione', 'required']) !!}
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
				</div>	
		</div>
	</div>
<!-- -->
<br>

<!-- 
Auth::user()->lastname 
-->
 <div class="row">
		<div class="form-group">
			{!! Form::Label('name', 'Cedula', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
						{!! Form::Text('name', Auth::user()->cedula, null, ['class' => 'form-control', 'placeholder' => 'Escriba Nombre', 'required', 'disabled']) !!}
						
					</div>
				</div>
		
			{!! Form::Label('lastname', 'Numero Personal', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
						{!! Form::Text('lastname',  Auth::user()->personalnumber, null, ['class' => 'form-control', 'placeholder' => 'Escriba Apellido', 'required', 'disabled']) !!}
						
					</div>
				</div>	
		</div>
</div>
<!-- -->
<br>
<!-- 
Auth::user()->lastname 
-->
 <div class="row">
		<div class="form-group">
			{!! Form::Label('name', 'Nombre', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
						{!! Form::Text('name', Auth::user()->name, null, ['class' => 'form-control', 'placeholder' => 'Escriba Nombre', 'required', 'disabled']) !!}
						
					</div>
				</div>
		
			{!! Form::Label('lastname', 'Apellido', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
						{!! Form::Text('lastname',  Auth::user()->lastname, null, ['class' => 'form-control', 'placeholder' => 'Escriba Apellido', 'required', 'disabled']) !!}
						
					</div>
				</div>	
		</div>
</div>
<!-- -->
<br>
<!-- -->
 <div class="row">	
		<div class="form-group">
			{!! Form::Label('dateofwork', 'Fecha Trabajo:', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
				 		{!! Form::Date('dateofwork',  $overtimes->dateofwork, ['class' => 'form-control datetimepickerF', 'placeholder' => 'seleccione', 'required']) !!}
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
				</div>
		
			{!! Form::Label('dayofweek', 'Dia de la Semana', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
						{!! Form::select('type', ['lunes' => 'Lunes', 'martes' => 'Martes', 'miercoles' => 'Miercoles', 'jueves' => 'Jueves', 'viernes' => 'Viernes', 'sabado' => 'Sabado', 'domingo' => 'Domingo'], null, ['class' => 'form-control', 'placeholder' => 'Selecione una Opcion..', 'required']) !!}
					</div>
				</div>	
		</div>
</div>
<!-- -->
<br>

<!-- -->
	<div class="row">	
		<div class="form-group">
 			{!! Form::Label('start_hour', 'Hora Inicio:', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
				 	<div class="input-group">
				 		{!! Form::Time('start_hour',  $overtimes->start_hour, ['class' => 'form-control datetimepickerT', 'placeholder' => 'seleccione', 'id' => 'timepicker', 'required']) !!}
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
				</div>

				{!! Form::Label('end_hour', 'Hora Fin', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
				 		{!! Form::Time('end_hour',  $overtimes->end_hour, ['class' => 'form-control datetimepickerT', 'placeholder' => 'seleccione', 'id' => 'timepicker2', 'required']) !!}
						
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
				</div>
						
		</div>
	</div>
<!-- -->
<br>
<!--  -->
 <div class="row">
		<div class="form-group">
			{!! Form::Label('totalhours', 'Total de Horas', ['class' => 'col-sm-2 control-label']) !!}
		 	<div class="col-sm-4">
				{!! Form::Select('totalhours', $departaments, $overtimes->total_hours, ['class' => 'form-control select-locations', 'placeholder' => 'Selecione una Opcion..',  'required']) !!}
			</div>	
			
			{!! Form::Label('codenomine_id', 'Codigo de Nomina', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
					{!! Form::Select('codenomine_id', $codenomines, $overtimes->codenomine_id, ['class' => 'form-control select-locations', 'placeholder' => 'Selecione una Opcion..',  'required']) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
			</div>	
		</div>
</div>
<!-- -->
<br>

<!-- -->
	<div class="row">	
		<div class="form-group">
 			{!! Form::Label('justification', 'Justificacion:', ['class' => 'col-sm-2 control-label']) !!}
			 	
			 		<div class="input-group">
				 		{!! Form::TextArea('justification',  $overtimes->justification, ['class' => 'form-control', 'placeholder' => 'seleccione', 'rows' => '5' , 'required', ]) !!}
					</div>
				

 			
		</div>
	</div>
<!-- -->

<br>
<center><div class="form-group">
		{!! Form::Submit('Editar Sobretiempo', ['class' => 'btn btn-primary']) !!}
</div></center>
<br>
	{!! Form::close() !!}

	<script type="text/javascript">
		
		$('#timepicker').timepicker({
	    'minTime': '5:00am',
	    'maxTime': '9:00pm',
	    'showDuration': true
		});

	</script>

	<script type="text/javascript">

	    $('#timepicker2').timepicker({
	    'minTime': '5:00am',
	    'maxTime': '9:00pm',
	    'showDuration': true
		});

	</script>

	<script>
	    $('.datepicker').datepicker({
	        format: "yyyy/mm/dd",
	        language: "es",
	        autoclose: true
	    });
	</script>

@endsection

@section('js')
	<script>
		$('.select-tag').chosen({
			placeholder_text_multiple: 'Seleccione un maximo de 3 Tags', max_selected_options: 3, no_results_text: "No se encuentra este tags"
		});

		$('.select-category').chosen({
			placeholder_text_single: 'Seleccione una Categoria', no_results_text: "No se encuentra esta Categoria"
		});
	
		$('.textarea-content').trumbowyg({
			lang: 'es',
		});

	</script>

@endsection