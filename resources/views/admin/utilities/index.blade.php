@extends('admin.template.main')

@section('title', 'Solicitar Permiso') 
 
@section('utilidades')
    class="active"
@endsection

@section('content')
	@if (session('status'))
    	<div class="alert alert-success">
        	{{ session('status') }}
    	</div>
    @endif

@endsection