@extends('admin.template.main')

@section('title', 'Lista de Usuarios')

@section('paneladmin')
    class="active"
@endsection

@section('content')

  @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
  @endif
  
  <div class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header" data-background-color="purple">
                <h4 class="title">Listado de Usuarios</h4>
              </div>
              <div class="card-content">
              <!--  Inicio del contenido -->
              <div class="pull-right margin-top-20">
                <a href="{{ route('users.create') }}" class="btn btn-primary">Registrar Usuario</a>
              </div>
              @include('flash::message')
              <!--  Inicio del contenido -->  

              <table class="table table-striped">
                <div class="row margin-top-12">
                  <div class="col-lg-12">
                    <div class="card">
                      <!-- Encabezado de la Tabla -->

                      <table class="table table-striped">
                          <thead>
                            <th>ID</th>
                            <th>NOMBRE</th>
                            <th>APELLIDO</th>
                            <th>CORREO</th>
                            <th>CELULAR</th>
                            <th>OFICINA</th>
                            <th>TIPO</th>
                            <th>ACCION</th>
                          </thead>
                          
                          <tbody>
                            @foreach($users as $user)
                              <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->lastname }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->phonecel }}</td>
                                <td>{{ $user->phoneofc }}</td>
                                <td>
                                  @if($user->type == "admin")
                                    <a href="#" class="btn btn-primary btn-xs"> {{ $user->type }}</a>
                                  @else
                                    <a href="#" class="btn btn-success btn-xs"> {{ $user->type }}</a>
                                  @endif              
                                </td>
                                  <td>
                                    |
                                    <a href="{{ route('users.show', $user->id) }}" class="fa fa-eye"> </a> 
                                    |
                                    <a href="{{ route('users.edit', $user->id) }}" class="fa fa-pencil"> </a> 
                                    | 
                                    <a href="{{ route('admin.users.destroy', $user->id) }}" onclick="return confirm('Segruro deseas Borrarlo')" class="fa fa-trash"></a>
                                  </td>
                                </tr>
                            @endforeach
                          </tbody>
                      </table>
                  
                <center> {!! $users->render() !!} </center>
                <!-- /CONTENT -->
              </div>
            </div>
          </div>
        </div>
      </div>
    
  </div>
</div>
</div>
</div>
</div>
</div>


<!-- -->

@endsection