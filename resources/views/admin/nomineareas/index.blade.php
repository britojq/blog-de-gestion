@extends('admin.template.main')

@section('title', 'Listado de Area de Nomina')

@section('paneladmin')
    class="active"
@endsection

@section('content')
<div class="content">
  <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header" data-background-color="purple">
              <h4 class="title">Listado de Areas de Nomina</h4>
            </div>
            <div class="card-content">
              <!--  Inicio del contenido -->

              

            <!--  Inicio del contenido -->
  <!--  Inicio del contenido -->
    <a href="{{ route('nomineareas.create') }}" class="btn btn-xs btn-primary">Registrar Area de Nomina</a>
     @include('flash::message')
    <div class="row margin-top-12">
  <div class="col-lg-12">
    <div class="card">
      <!-- CONTENT -->
      <table class="table table-striped">
          <!-- Encabezado de la Tabla -->
          <thead>
              <th>ID</th>
              <th>Area de Nomina</th>
              <th>Accion</th>
          </thead>
          
          <!-- Cuerpo de la Tabla -->
          <tbody>
              @foreach($nomineareas as $nominearea)
                  <tr>
                    <td>{{ $nominearea->id }}</td>
                    <td>{{ $nominearea->name }}</td>
                    <td>
                      <!-- Botones Editar / Borrar -->
                      <a href="{{ route('nomineareas.edit', $nominearea->id) }}" class="fa fa-pencil"> </a> | <a href="{{ route('admin.nomineareas.destroy', $nominearea->id) }}" onclick="return confirm('Segruro deseas Borrarlo')" class="fa fa-trash"></a>
                    </td>
                  </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-center">
              {!! $nomineareas->render() !!}
        </div>
    <!-- /CONTENT -->
    </div>
  </div>
</div>
<!-- -->
</div>
</div>
</div>
</div>
</div>
</div>

@endsection