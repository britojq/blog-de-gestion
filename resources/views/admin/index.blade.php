@extends('admin.template.main')

@section('title', 'Pagina principal')

@section('principal')
    class="active"
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
                    
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header card-chart" data-background-color="green">
                                    <div class="ct-chart" id="dailySalesChart"></div>
                                </div>
                                <div class="card-content">
                                    <h4 class="title">Avance diario en Proyectos</h4>
                                    <p class="category">
                                        <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> de Avance hasta la fecha.</p>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="fa fa-line-chart"></i> Actualizado hace  4 minutos
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header card-chart" data-background-color="green">
                                    <div class="ct-chart" id="emailsSubscriptionChart"></div>
                                </div>
                                <div class="card-content">
                                    <h4 class="title">Gestion Mensual de la Unidad</h4>
                                    <p class="category">Medido en base a los datos ingresados</p>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="fa fa-clock-o"></i>
                                         Actualizado hace 2 dias
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header card-chart" data-background-color="green">
                                    <div class="ct-chart" id="completedTasksChart"></div>
                                </div>
                                <div class="card-content">
                                    <h4 class="title">Horario de solicitud Viaticos</h4>
                                    <p class="category">Actualizado hace 2 dias</p>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="fa fa-th-large"></i> Actualizado hace 2 dias
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--    -->
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="card card-stats">
                                <div class="card-header" data-background-color="purple">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                                <div class="card-content">
                                    <p class="category">Sobretiempos</p>
                                    <h3 class="title">120
                                    </h3>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="fa fa-th-large"></i>
                                        <a href="#pablo">Ver</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="card card-stats">
                                <div class="card-header" data-background-color="green">
                                    <i class="fa fa-wrench"></i>
                                </div>
                                <div class="card-content">
                                    <p class="category">Proyectos</p>
                                    <h3 class="title">23%</h3>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="fa fa-th-large"></i>
                                        <a href="#pablo">Ver</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="card card-stats">
                                <div class="card-header" data-background-color="blue">
                                    <i class="fa fa-road"></i>
                                </div>
                                <div class="card-content">
                                    <p class="category">Viaticos</p>
                                    <h3 class="title">75</h3>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="fa fa-th-large"></i>
                                        <a href="#pablo">Ver</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="card card-stats">
                                <div class="card-header" data-background-color="orange">
                                    <i class="fa fa-file-text"></i>
                                </div>
                                <div class="card-content">
                                    <p class="category">Permisos</p>
                                    <h3 class="title">+245</h3>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="fa fa-th-large"></i>
                                        <a href="#">Ver</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--    -->

                    <div class="row">


                        
                        <div class="col-lg-12 col-md-12">
                            <div class="card">
                                <div class="card-header" data-background-color="blue">
                                    <h3 class="title">Listado de Permisos</h4>
                                    <h4>Permisos solicitados mes en curso</h4>
                                </div>
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <td>#</td>
                                                <td>Usuario</td>
                                                <td>Estado del Permiso</td>
                                                <td>Fecha de Permiso</td>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <tr>
                                                <td><a href="#">190</a></td>
                                                <td>Nombre Usuario</td>
                                                <td>
                                                    <a href="#" class="btn btn-warning btn-xs btn-round">PENDIENTE</a>
                                                </td>
                                                <td>
                                                    01/01/2018
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><a href="#">190</a></td>
                                                <td>Nombre Usuario</td>
                                                <td>
                                                    <a href="#" class="btn btn-success btn-xs btn-round">APROBADO</a>
                                                </td>
                                                <td>
                                                    01/01/2018
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><a href="#">190</a></td>
                                                <td>Nombre Usuario</td>
                                                <td>
                                                    <a href="#" class="btn btn-warning btn-xs btn-round">PENDIENTE</a>
                                                </td>
                                                <td>
                                                    01/01/2018
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><a href="#">190</a></td>
                                                <td>Nombre Usuario</td>
                                                <td>
                                                    <a href="#" class="btn btn-danger btn-xs btn-round">NEGADO</a>
                                                </td>
                                                <td>
                                                    01/01/2018
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
    
@endsection