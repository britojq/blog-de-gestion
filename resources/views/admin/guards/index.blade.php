@extends('admin.template.main')

@section('title', 'Listado de Guardias')

@section('paneladmin')
    class="active"
@endsection

@section('content')
<div class="content">
  <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header" data-background-color="purple">
              <h4 class="title">Listado de Guardias</h4>
            </div>
            <div class="card-content">
              <!--  Inicio del contenido -->

              <a href="{{ route('guards.create') }}" class="btn btn-primary">Registrar Guardia</a>
               @include('flash::message')

            <!--  Inicio del contenido -->
  <!--  Inicio del contenido -->
    
      <table class="table table-striped">
          <!-- Encabezado de la Tabla -->
          <thead>
              <th>ID</th>
              <th>Nombre</th>
              <th>Apellido</th>
              <th>Desde</th>
              <th>Hasta</th>
              <th>Accion</th>
          </thead>
          
          <!-- Cuerpo de la Tabla -->
          <tbody>
              @foreach($guards as $guard)
                  <tr>
                    <td>{{ $guard->id }}</td>
                    <td>{{ $guard->user->name }}</td>
                    <td>{{ $guard->user->lastname }}</td>
                    <td>{{ \Carbon\Carbon::parse($guard->from_date)->format('d/m/Y')}}</td>
                    <td>{{ \Carbon\Carbon::parse($guard->to_date)->format('d/m/Y')}}</td>
                    <td>
                      <!-- Botones Editar / Borrar -->
                      <a href="{{ route('guards.edit', $guard->id) }}" class="fa fa-pencil"> </a> | <a href="{{ route('admin.guards.destroy', $guard->id) }}" onclick="return confirm('Segruro deseas Borrarlo')" class="fa fa-trash"></a>
                    </td>
                  </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-center">
              {!! $guards->render() !!}
        </div>
    <!--  Fin del Contenido -->
    </div>
  </div>
</div>
</div>
</div>
</div>

@endsection