@extends('admin.template.maintimepicker')

@section('title', 'Editar Guardia') 

@section('paneladmin')
    class="active"
@endsection 

@section('content')
<div class="content">
  <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header" data-background-color="purple">
              <h4 class="title">Modificar Fecha de la Guardia</h4>
            </div>
            <div class="card-content">
              <!--  Inicio del contenido -->
              <!-- ALERTS & ERRORS -->
        @include('flash::message')
        @include('admin.template.partials.errors')
        <!-- END ALERTS & ERRORS -->

	{!! Form::open(['action' => ['GuardsController@update', $guard], 'method' => 'PUT']) !!}

	<div class="row">	
		<div class="form-group">
 			{!! Form::Label('from_date', 'Inicio de Guardia:', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
			 		{!! Form::Date('from_date',  $guard->from_date, ['class' => 'form-control datetimepickerF', 'placeholder' => 'seleccione', 'required']) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
		</div>
		<div class="form-group">
			{!! Form::Label('to_date', 'Final de Guardia:', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
			 		{!! Form::Date('to_date',  $guard->to_date, ['class' => 'form-control datetimepickerF', 'placeholder' => 'seleccione', 'required']) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>				
			</div>	
		</div>
	</div>
<!-- -->
<br>
<br>
<center><div class="form-group">
		{!! Form::Submit('Editar', ['class' => 'btn btn-primary']) !!}
</div></center>
<br>

	{!! Form::close() !!}

  
@endsection

