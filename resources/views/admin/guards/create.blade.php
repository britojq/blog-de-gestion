@extends('admin.template.maintimepicker')

@section('paneladmin')
    class="active"
@endsection

@section('title', 'Agregar Guardia') 
    
@section('content')
<div class="content">
  <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header" data-background-color="purple">
              <h4 class="title">Agregar Guardia</h4>
            </div>
            <div class="card-content">
              <!--  Inicio del contenido -->
              <!-- ALERTS & ERRORS -->
        @include('flash::message')
        @include('admin.template.partials.errors')
        <!-- END ALERTS & ERRORS -->
	
	{!! Form::open(['route' => 'guards.store', 'method' => 'POST', 'files' => true]) !!}

<div class="row">	
		<div class="form-group">
				
			<div class="form-group">
				{!! Form::Label('users_id1', 'Nombre', ['class' => 'col-sm-2 control-label']) !!}
				<div class="col-sm-4">
					{!! Form::Select('users_id1', $users, null, ['class' => 'form-control select-user col-sm-4', 'required']) !!}
				</div>
			
				{!! Form::Label('users_id2', 'Apellido', ['class' => 'col-sm-2 control-label']) !!}
				<div class="col-sm-4">
					{!! Form::Select('users_id2', $users2, null, ['class' => 'form-control select-user', 'required']) !!}
				</div>
			</div>

		</div>
</div>
<br>
<!-- -->
	<div class="row">	
		<div class="form-group">
 			{!! Form::Label('from_date', 'Inicio de Guardia:', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
			 		{!! Form::Date('from_date',  null, ['class' => 'form-control datetimepickerF', 'placeholder' => 'seleccione', 'required']) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
		</div>
		<div class="form-group">
			{!! Form::Label('to_date', 'Final de Guardia:', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
			 		{!! Form::Date('to_date',  null, ['class' => 'form-control datetimepickerF', 'placeholder' => 'seleccione', 'required']) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>				
			</div>	
		</div>
	</div>
<!-- -->
<br>

<br>
<center><div class="form-group">
		{!! Form::Submit('Agregar Guardia', ['class' => 'btn btn-primary']) !!}
</div></center>
<br>

	{!! Form::close() !!}

  

@endsection

