@extends('admin.template.main')

@section('title', 'Listado de Permisos')

@section('solicitudes')
    class="active"
@endsection

  @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
  @endif

@section('content')

<div class="content">
  <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header" data-background-color="purple">
              <h4 class="title">Listado de Permisos</h4>
            </div>
            <div class="card-content">
              <!--  Inicio del contenido -->

              <a href="{{ route('permises.create') }}" class="btn btn-primary ">Registrar permiso</a>
              @include('flash::message')

            <!--  Inicio del contenido -->
    
            <table class="table table-striped">
                <!-- Encabezado de la Tabla -->
                <thead>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Fecha</th>
                    <th>Desde</th>
                    <th>Desde Hora</th>
                    <th>Hasta</th>
                    <th>Hasta Hora</th>
                    <th>Estado</th>
                    <th>Accion</th>
                </thead>
               
                <!-- Cuerpo de la Tabla -->
                <tbody>
                    @foreach($permises as $permise)
                        <tr>
                          <td>{{ $permise->id }}</td>
                          <td>{{ $permise->user->name }}</td>
                          <td>{{ $permise->user->lastname }}</td>
                          <td>{{ \Carbon\Carbon::parse($permise->datesolicit)->format('d/m/Y')}}</td>
                          <td>{{ \Carbon\Carbon::parse($permise->start_date)->format('d/m/Y')}}</td>
                          <td>{{ $permise->start_hour }}</td>
                          <td>{{ \Carbon\Carbon::parse($permise->end_date)->format('d/m/Y')}}</td>
                          <td>{{ $permise->end_hour }}</td>
                          <td>
                            @if($permise->status == "aprovado")
                              <a href="#" class="btn btn-success btn-xs"> {{ $permise->status }}</a>
                            @endif
                            @if($permise->status == "negado")
                              <a href="#" class="btn btn-danger btn-xs"> {{ $permise->status }}</a>
                            @endif
                            @if($permise->status == "pendiente")
                              <a href="#" class="btn btn-warning btn-xs"> {{ $permise->status }}</a>
                            @endif
                          </td>
                          <td>
                            <!-- Botones Editar / Borrar -->
                            <a href="{{ route('permises.edit', $permise->id) }}" class="fa fa-pencil"> </a> | <a href="{{ route('admin.permises.destroy', $permise->id) }}" onclick="return confirm('Segruro deseas Borrarlo')" class="fa fa-trash"></a>
                          </td>
                        </tr>
                      @endforeach
                  </tbody>
            </table>
            <div class="text-center">
              {!! $permises->render() !!}
            </div>
      
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!--  Fin del Contenido -->
@endsection