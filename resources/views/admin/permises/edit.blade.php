@extends('admin.template.maintimepicker')

@section('title', 'Modificar Permiso') 

@section('solicitudes')
    class="active"
@endsection
   
@section('content')
	
	


<!-- PAGE CONTENT -->

	<div class="content">
      <div class="container-fluid">
	<!-- PAGE CONTENT -->
	<!-- ALERTS & ERRORS -->
        @include('flash::message')
        @include('admin.template.partials.errors')
        <!-- END ALERTS & ERRORS -->
        <div class="row">
        {!! Form::open(['route' => ['permises.update', $permise], 'method' => 'PUT']) !!}
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                    <h4 class="title">Editar del Permiso</h4>
                    </div>
                    <div class="card-content">
                        

<!--  -->
 <div class="row">
		<div class="form-group">
			{!! Form::Label('location_id', 'Lugar de Solicitud', ['class' => 'col-sm-2 control-label']) !!}
		 	<div class="col-sm-4">
				{!! Form::Select('location_id', $locations, $permise->location_id, ['class' => 'form-control select-locations', 'placeholder' => 'Selecione una Opcion..',  'required']) !!}
			</div>	
			
			{!! Form::Label('datesolicit', 'Fecha de Solicitud', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
					{!! Form::Date('datesolicit', $permise->datesolicit, ['class' => 'form-control datetimepickerF', 'placeholder' => 'seleccione', 'required']) !!}
					<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
				</div>
			</div>	
		</div>
</div>
<!-- -->
<br>


<!-- 
Auth::user()->lastname 
-->
 

<br>

<div class="row">	
	<div class="form-group">

		{!! Form::Label('permisemotive_id', 'Motivo Permiso', ['class' => 'col-sm-2 control-label']) !!}
		 	<div class="col-sm-4">
				{!! Form::Select('permisemotive_id', $permisemotives, $permise->permisemotive_id, ['class' => 'form-control select-permisemotives','placeholder' => 'Selecione una Opcion..', 'required']) !!}
			</div>	

		{!! Form::Label('permisetype_id', 'Tipo de Permiso', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
					{!! Form::Select('permisetype_id', $permisetypes, $permise->permisetype_id, ['class' => 'form-control select-permisetypes', 'placeholder' => 'Selecione una Opcion..', 'required']) !!}
				</div>

		</div>	
  </div>
<!-- -->

<br>
<!-- -->
	<div class="row">	
		<div class="form-group">
 			{!! Form::Label('start_hour', 'Hora Inicio:', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
				 	<div class="input-group">
				 		{!! Form::Time('start_hour', $permise->start_hour, ['class' => 'form-control datetimepickerT', 'placeholder' => 'seleccione', 'id' => 'timepicker', 'required']) !!}
						
					</div>
				</div>

				{!! Form::Label('end_hour', 'Hora Final', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
				 		{!! Form::Time('end_hour', $permise->end_hour, ['class' => 'form-control datetimepickerT', 'placeholder' => 'seleccione', 'id' => 'timepicker2', 'required']) !!}
						
					</div>
				</div>
						
		</div>
	</div>
<!-- -->

<br>

<!-- -->
	<div class="row">	
		<div class="form-group">
 			{!! Form::Label('start_date', 'Fecha Inicio:', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
				 		{!! Form::Date('start_date', $permise->start_date, ['class' => 'form-control datetimepickerF', 'placeholder' => 'seleccione', 'required']) !!}
					</div>
				</div>

 			{!! Form::Label('end_date', 'Fecha Final:', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
				 		{!! Form::Date('end_date', $permise->end_date, ['class' => 'form-control datetimepickerF', 'placeholder' => 'seleccione', 'required']) !!}
					</div>
				</div>	
		</div>
	</div>
<!-- -->
<br>
<!-- -->
	<div class="row">	
		<div class="form-group">
 			{!! Form::Label('observmotiv', 'Observaciones:', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-10">
			 		<div class="input-group">
				 		{!! Form::TextArea('observmotiv',  $permise->observmotiv, null, ['class' => 'form-control', 'placeholder' => 'seleccione',  'required', ]) !!}
					</div>
				</div>

 			
		</div>
	</div>
<!-- -->

<br>
<center><div class="form-group">
		{!! Form::Submit('Modificar Permiso', ['class' => 'btn btn-primary']) !!}
</div></center>
<br>
	{!! Form::close() !!}

	

	

	

@endsection

