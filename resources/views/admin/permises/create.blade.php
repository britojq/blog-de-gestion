@extends('admin.template.maintimepicker')

@section('title', 'Solicitar Permiso') 

@section('solicitudes')
    class="active"
@endsection
 
@section('content')
	@if (session('status'))
    	<div class="alert alert-success">
        	{{ session('status') }}
    	</div>
    @endif
	<!-- PAGE CONTENT -->

	<div class="content">
      <div class="container-fluid">
	<!-- PAGE CONTENT -->
	<!-- ALERTS & ERRORS -->
        @include('flash::message')
        @include('admin.template.partials.errors')
        <!-- END ALERTS & ERRORS -->
        <div class="row">
        {!! Form::open(['route' => 'permises.store', 'method' => 'POST', 'files' => true]) !!}
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                    <h4 class="title">Solicitud de  Permiso</h4>
                    </div>
                    <div class="card-content">
                        <div class="row">
							<!-- LEFT -->
	                        <div class="col-md-6">
	                            <div class="form-group row margin-top-10">
	                                <div class="col-md-4">
	                                      {!! Form::Label('location_id', 'Lugar de Solicitud', ['class' => 'control-label']) !!}     
	                                </div>
	                                <div class="col-md-8">
										<div class="input-group">
											{!! Form::Select('location_id', $locations, null, ['class' => 'form-control select-locations', 'placeholder' => 'Selecione una Opcion..',  'required']) !!}	
										</div>
	                                </div>
	                            </div>                                      
	                        </div>                
	                        <!-- /LEFT -->
	     
	        
	                     	<!-- RIGHT -->
	                     	<div class="col-md-6">
	                      		<div class="form-group row margin-top-10">
	                                <div class="col-md-4">
	                                    {!! Form::Label('datesolicit', 'Fecha de Solicitud', ['class' => 'control-label']) !!}     
	                                </div>
	                                <div class="col-md-8">
	                                    <div class="input-group">
										{!! Form::Date('datesolicit',  null, ['class' => 'form-control datetimepickerF', 'placeholder' => 'seleccione', 'required']) !!}	
	                                </div>
	                            </div>
	                        </div>
						</div>
							<!-- /RIGHT -->
					</div>



<!-- -->
<br>


<div class="row">	
	<div class="form-group">

		{!! Form::Label('permisemotive_id', 'Motivo Permiso', ['class' => 'col-sm-2 control-label']) !!}
		 	<div class="col-sm-4">
				{!! Form::Select('permisemotive_id', $permisemotives, null, ['class' => 'form-control select-permisemotives','placeholder' => 'Selecione una Opcion..', 'required']) !!}
			</div>	

		{!! Form::Label('permisetype_id', 'Tipo de Permiso', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
					{!! Form::Select('permisetype_id', $permisetypes, null, ['class' => 'form-control select-permisetypes', 'placeholder' => 'Selecione una Opcion..', 'required']) !!}
				</div>

		</div>	
  </div>
<!-- -->

<br>
<!-- -->
	<div class="row">	
		<div class="form-group">
 			{!! Form::Label('start_hour', 'Hora Inicio:', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
				 	<div class="input-group">
				 		{!! Form::Time('start_hour',  null, ['class' => 'form-control datetimepickerT', 'placeholder' => 'seleccione', 'id' => 'timepicker', 'required']) !!}
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
				</div>

				{!! Form::Label('end_hour', 'Hora Final', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
				 		{!! Form::Time('end_hour',  null, ['class' => 'form-control datetimepickerT', 'placeholder' => 'seleccione', 'id' => 'timepicker2', 'required']) !!}
						
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
				</div>
						
		</div>
	</div>
<!-- -->

<br>

<!-- -->
	<div class="row">	
		<div class="form-group">
 			{!! Form::Label('start_date', 'Fecha Inicio:', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
				 		{!! Form::Date('start_date',  null, ['class' => 'form-control datetimepickerF', 'placeholder' => 'seleccione', 'required']) !!}
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
				</div>

 			{!! Form::Label('end_date', 'Fecha Final:', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
				 		{!! Form::Date('end_date',  null, ['class' => 'form-control datetimepickerF', 'placeholder' => 'seleccione', 'required']) !!}
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
				</div>	
		</div>
	</div>
<!-- -->
<br>
<!-- -->
	<div class="row">	
		<div class="form-group">
 			{!! Form::Label('observmotiv', 'Observaciones:', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-10">
			 		<div class="input-group">
				 		{!! Form::TextArea('observmotiv',  null, ['class' => 'form-control', 'placeholder' => 'seleccione',  'required', ]) !!}
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
				</div>

 			
		</div>
	</div>
<!-- -->



<br>
<center><div class="form-group">
		{!! Form::Submit('Registrar Permiso', ['class' => 'btn btn-primary']) !!}
</div></center>
<br>
	{!! Form::close() !!}

	



@endsection