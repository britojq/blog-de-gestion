@extends('admin.template.main')

@section('title', 'Lista de Categorias')

@section('blog')
    class="active"
@endsection

@section('content')
<div class="content">
  <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header" data-background-color="purple">
              <h4 class="title">Listado de Categorias</h4>
            </div>
            <div class="card-content">
              <!--  Inicio del contenido -->

              

            <!--  Inicio del contenido -->
  <!--  Inicio del contenido -->
    <a href="{{ route('categories.create') }}" class="btn btn-info btn-xs">Registrar Nueva Categoria</a>
     @include('flash::message')
<div class="row margin-top-12">
  <div class="col-lg-12">
    <div class="card">
      <!-- CONTENT -->

      <table class="table table-striped">
          <!-- Encabezado de la Tabla -->
          <thead>
              <th>ID</th>
              <th>Nombre de la categoria</th>
              <th>Accion</th>
          </thead>
          
          <!-- Cuerpo de la Tabla -->
          <tbody>
              @foreach($categories as $category)
                  <tr>
                  <td>{{ $category->id }}</td>
                  <td>{{ $category->name }}</td>
                  <td>
                    <!-- Botones Editar / Borrar -->
                    <a href="{{ route('categories.edit', $category->id) }}" class="fa fa-pencil"> </a> | <a href="{{ route('admin.categories.destroy', $category->id) }}" onclick="return confirm('Segruro deseas Borrarlo')" class="fa fa-trash"></a>
                    </td>
                    </tr>
                @endforeach
            </tbody>
	      </table>
        <div class="text-center">
             	{!! $categories->render() !!}
        </div>
    <!-- /CONTENT -->
    </div>
  </div>
</div>
<!-- -->
</div>
</div>
</div>
</div>
</div>
</div>

@endsection