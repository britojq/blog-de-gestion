@extends('admin.template.main')

@section('title', 'Listado Tipos de Permiso')

@section('paneladmin')
    class="active"
@endsection

@section('content')
<div class="content">
  <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header" data-background-color="purple">
              <h4 class="title">Listado de Tipos de Permisos</h4>
            </div>
            <div class="card-content">
              <!--  Inicio del contenido -->
                <a href="{{ route('permisetypes.create') }}" class="btn btn-xs btn-primary">Registrar Tipo de Permiso</a>
                 @include('flash::message')
              

            <!--  Inicio del contenido -->
  <!--  Inicio del contenido -->
    
    <div class="row margin-top-12">
  <div class="col-lg-12">
    <div class="card">
      <!-- CONTENT -->

      <table class="table table-striped">
          <!-- Encabezado de la Tabla -->
          <thead>
              <th>ID</th>
              <th>Tipos de Permiso</th>
              <th>Accion</th>
          </thead>
          
          <!-- Cuerpo de la Tabla -->
          <tbody>
              @foreach($permisetypes as $permisetype)
                  <tr>
                    <td>{{ $permisetype->id }}</td>
                    <td>{{ $permisetype->name }}</td>
                    <td>
                      <!-- Botones Editar / Borrar -->
                      <a href="{{ route('permisetypes.edit', $permisetype->id) }}" class="fa fa-pencil"> </a> | <a href="{{ route('admin.permisetypes.destroy', $permisetype->id) }}" onclick="return confirm('Segruro deseas Borrarlo')" class="fa fa-trash"></a>
                    </td>
                  </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-center">
              {!! $permisetypes->render() !!}
        </div>
    <!-- /CONTENT -->
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<!-- -->
@endsection