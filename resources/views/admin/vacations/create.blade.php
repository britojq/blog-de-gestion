@extends('admin.template.maintimepicker')

@section('title', 'SOLICITUD DE VACACIONES') 

@section('solicitudes')
    class="active"
@endsection

@section('content')

<div class="content">
      <div class="container-fluid">
	<!-- PAGE CONTENT -->
	<!-- ALERTS & ERRORS -->
        @include('flash::message')
        @include('admin.template.partials.errors')
        <!-- END ALERTS & ERRORS -->
        <div class="row">
        {!! Form::open(['route' => 'vacations.store', 'method' => 'POST', 'files' => true]) !!}
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                    <h4 class="title">Solicitud de Vacaciones</h4>
                    </div>
                    <div class="card-content">
                        <div class="row">
	
		<div class="form-group">
			<center>
			{!! Form::Label('location_id', 'SOLICITUD DE VACACIONES', ['class' => 'col-sm-6 control-label']) !!}
		 	</center>
			
			{!! Form::Label('datesolicit', 'Fecha de Solicitud', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
					{!! Form::Date('datesolicit',  null, ['class' => 'form-control datetimepickerF', 'placeholder' => 'seleccione', 'required']) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
			</div>	
		</div>
</div>
<!-- -->
<br>


<!-- 
Auth::user()->lastname 
-->
 <div class="row">
		<div class="form-group">
			{!! Form::Label('name', 'Nombre', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
						{!! Form::Text('name', Auth::user()->name, null, ['class' => 'form-control', 'placeholder' => 'Escriba Nombre', 'required', 'disabled']) !!}
						
					</div>
				</div>
		
			{!! Form::Label('lastname', 'Apellido', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 		<div class="input-group">
						{!! Form::Text('lastname',  Auth::user()->lastname, null, ['class' => 'form-control', 'placeholder' => 'Escriba Apellido', 'required', 'disabled']) !!}
						
					</div>
				</div>	
		</div>
</div>
<!-- -->
<br>
<!-- -->
<div class="row">	
	<div class="form-group">
		{!! Form::Label('cedula', 'Cedula', ['class' => 'col-sm-2 control-label']) !!}
			 <div class="col-sm-4">
			 	<div class="input-group">
					{!! Form::Text('cedula', Auth::user()->cedula, null, ['class' => 'form-control', 'placeholder' => 'Escriba Cedula', 'required', 'disabled']) !!}
				</div>
			</div>

		{!! Form::Label('carge_id', 'Telefono', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 	<div class="input-group">
					{!! Form::Text('cedula', Auth::user()->phonecel, null, ['class' => 'form-control', 'placeholder' => 'Escriba Cedula', 'required', 'disabled']) !!}
				</div>
			</div>
		</div>	
  </div>
<!-- -->
<br>

<!-- -->
<div class="row">	
	<div class="form-group">
		{!! Form::Label('cedula', 'Correo', ['class' => 'col-sm-2 control-label']) !!}
			 <div class="col-sm-4">
			 	<div class="input-group">
					{!! Form::Text('cedula', Auth::user()->email, null, ['class' => 'form-control', 'placeholder' => 'Escriba Cedula', 'required', 'readonly']) !!}
				</div>
			</div>

		{!! Form::Label('carge_id', 'Numero Personal', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
			 	<div class="input-group">
					{!! Form::Text('cedula', Auth::user()->personalnumber, null, ['class' => 'form-control', 'placeholder' => 'Escriba Cedula', 'required', 'disabled']) !!}
				</div>
			</div>
		</div>	
  </div>
<!-- -->
<br>

<!-- -->
<div class="row">	
	<div class="form-group">

		{!! Form::Label('unit_id', 'Unidad', ['class' => 'col-sm-2 control-label']) !!}
		 	<div class="col-sm-4">
				{!! Form::Select('unit_id', $units, null, ['class' => 'form-control select-units','placeholder' => 'Selecione una Opcion..', 'required']) !!}
			</div>	

		{!! Form::Label('unitcode_id', 'Gerencia', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
					{!! Form::Select('unitcode_id', $gerencys, null, ['class' => 'form-control select-unitcodes', 'placeholder' => 'Selecione una Opcion..', 'required']) !!}
				</div>

		</div>	
  </div>
<!-- -->
<br>


<br>

<!-- -->
<div class="row">	
	<div class="form-group">

	{!! Form::Label('dayofweek', 'Seleccione la Zona', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-4">
						{!! Form::select('zone', ['caracas' => 'CARACAS (Caracas, Miranda y Vargas)', 'Lara' => 'LARA (Yaracuy, Portuguesa, Cojedes, y Lara)', 'zulia' => 'ZULIA (Falcón, Zulia y Trujillo)', 'tachira' => 'TACHIRA (Mérida, Táchira y Trujillo)', 'bolivar' => 'BOLIVAR (Monagas, Delta Amacuro y Bolívar)', 'anzoategui' => 'ANZOATEGUI (Sucre y Anzoátegui)', 'carabobo' => 'CARABOBO (Carabobo y Aragua)', 'apure' => 'APURE (Guárico, Apure y Amazonas)', 'nueva esparta' => 'NUEVA ESPARTA'], null, ['class' => 'form-control', 'placeholder' => 'Selecione una Opcion..', 'required']) !!}
					
				</div>
	{!! Form::Label('typenomine_id', 'Tipo de Nomina', ['class' => 'col-sm-2 control-label']) !!}
		 	<div class="col-sm-4">
				{!! Form::Select('typenomine_id', $typenomines, null, ['class' => 'form-control select-typenomines', 'placeholder' => 'Selecione una Opcion..',  'required']) !!}
			</div>	

</div>
</div>
<!-- -->

<br>

<!-- -->
	<div class="row">	
		<div class="form-group">
		<!-- -->
			<div class="panel panel-info">
			  <div class="panel-heading">
			  	<div class="radio-inline">
		  			<label><input type="radio" name="optradio">OPCION A | 15 Dias Habiles</label>
		  			<br>
				</div> 		  	
			  </div>
		<!-- -->
		<!-- -->
			  <div class="panel-body">
					<div class="row">
					    <div class="col-sm-4"> 
					    <center>(NO APLICA FRACCION)</center>
					    </div>
					    <div class="col-sm-8">
					      {!! Form::Label('start_date', 'Desde:', ['class' => 'col-sm-2 control-label']) !!}
						 	<div class="col-sm-4">
						 		<div class="input-group">
							 		{!! Form::Date('start_date',  null, ['class' => 'form-control datetimepickerF', 'placeholder' => 'seleccione', 'required']) !!}
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>

				 			{!! Form::Label('end_date', 'Hasta:', ['class' => 'col-sm-2 control-label']) !!}
							 	<div class="col-sm-4">
							 		<div class="input-group">
								 		{!! Form::Date('end_date',  null, ['class' => 'form-control datetimepickerF', 'placeholder' => 'seleccione', 'required']) !!}
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>	
					    </div>
				  </div>
			  </div>
		<!-- -->
			</div>
		</div>
	</div>
<!-- -->
<br>

<!-- -->
	<div class="row">	
		<div class="form-group">
		<!-- -->
			<div class="panel panel-info">
			  <div class="panel-heading">
			  	<div class="radio-inline">
		  			<label><input type="radio" name="optradio">OPCION B | Dias Continuos</label>
		  			<br>
				</div> 
			  </div>
		<!-- -->
		<!-- -->
			  <div class="panel-body">
					<div class="row">
					    <div class="col-sm-4"> 
					    	<center>
					    	SIN FRACCION
					    	</center>
					    </div>
					    <div class="col-sm-8">
					      {!! Form::Label('start_date', 'Desde:', ['class' => 'col-sm-2 control-label']) !!}
						 	<div class="col-sm-4">
						 		<div class="input-group">
							 		{!! Form::Date('start_date',  null, ['class' => 'form-control datetimepickerF', 'placeholder' => 'seleccione', 'required']) !!}
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>

				 			{!! Form::Label('end_date', 'Hasta:', ['class' => 'col-sm-2 control-label']) !!}
							 	<div class="col-sm-4">
							 		<div class="input-group">
								 		{!! Form::Date('end_date',  null, ['class' => 'form-control datetimepickerF', 'placeholder' => 'seleccione', 'required']) !!}
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>	
					    </div>
				  </div>
			  </div>
		<!-- -->
			</div>
		</div>
	</div>
<!-- -->
<br>

<!-- -->
	<div class="row">	
		<div class="form-group">
		<!-- -->
			<div class="panel panel-info">
			  <div class="panel-heading">
			  	<div class="radio-inline">
		  			<label><input type="radio" name="optradio">OPCION C | Compensatorio | Con Fraccion</label>
		  			<br>
				</div> 		  	
			  </div>
		<!-- -->
		<!-- -->
			  <div class="panel-body">
					<div class="row">
					    <div class="col-sm-4"> 
					    <center>PRIMERA FRACCION</center>
					    </div>
					    <div class="col-sm-8">
					      {!! Form::Label('start_date', 'Desde:', ['class' => 'col-sm-2 control-label']) !!}
						 	<div class="col-sm-4">
						 		<div class="input-group">
							 		{!! Form::Date('start_date',  null, ['class' => 'form-control datetimepickerF', 'placeholder' => 'seleccione', 'required']) !!}
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>

				 			{!! Form::Label('end_date', 'Hasta:', ['class' => 'col-sm-2 control-label']) !!}
							 	<div class="col-sm-4">
							 		<div class="input-group">
								 		{!! Form::Date('end_date',  null, ['class' => 'form-control datetimepickerF', 'placeholder' => 'seleccione', 'required']) !!}
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>	
					    </div>
				  </div>
			  </div>
		<!-- -->
		<!-- -->
			  <div class="panel-body">
					<div class="row">
					    <div class="col-sm-4"> 
					    <center>SEGUNDA FRACCION</center>
					    </div>
					    <div class="col-sm-8">
					      {!! Form::Label('start_date', 'Desde:', ['class' => 'col-sm-2 control-label']) !!}
						 	<div class="col-sm-4">
						 		<div class="input-group">
							 		{!! Form::Date('start_date',  null, ['class' => 'form-control datetimepickerF', 'placeholder' => 'seleccione', 'required']) !!}
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>

				 			{!! Form::Label('end_date', 'Hasta:', ['class' => 'col-sm-2 control-label']) !!}
							 	<div class="col-sm-4">
							 		<div class="input-group">
								 		{!! Form::Date('end_date',  null, ['class' => 'form-control datetimepickerF', 'placeholder' => 'seleccione', 'required']) !!}
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>	
					    </div>
				  </div>
			  </div>
		<!-- -->
			</div>
		</div>
	</div>
<!-- -->
<br>

















<!-- -->
	<div class="row">	
		<div class="form-group">
 			{!! Form::Label('observmotiv', 'Observacion / Justificacion:', ['class' => 'col-sm-2 control-label']) !!}
			 	<div class="col-sm-10">
			 		<div class="input-group">
				 		{!! Form::TextArea('observmotiv',  null, ['class' => 'form-control', 'placeholder' => 'escriba la justificacion<',  'required', ]) !!}
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
				</div>

 			
		</div>
	</div>
<!-- -->



<br>
<center><div class="form-group">
		{!! Form::Submit('Registrar Vacacion', ['class' => 'btn btn-primary']) !!}
</div></center>
<br>
	{!! Form::close() !!}



	

	

@endsection

