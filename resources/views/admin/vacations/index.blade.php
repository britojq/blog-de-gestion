@extends('admin.template.main')

@section('title', 'Listado de Permisos')

@section('solicitudes')
    class="active"
@endsection

@section('content')
<div class="content">
  <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header" data-background-color="purple">
              <h4 class="title">Listado de Permisos</h4>
            </div>
            <div class="card-content">
            <!--  Inicio del contenido -->
              <a href="{{ route('vacations.create') }}" class="btn btn-primary btn-xs">Registrar Vacaciones</a>
               @include('flash::message')
            <!--  Inicio del contenido -->

            <table class="table table-striped">
                <!-- Encabezado de la Tabla -->
                <thead>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    
                </thead>
                <!-- Cuerpo de la Tabla -->
                <tbody>
                    @foreach($vacations as $vacation)
                        <tr>
                          <td>{{ $vacation->id }}</td>
                          <td>{{ $vacation->user_id }}</td>
                          <td>{{ $vacation->user_id }}</td>
                          <td>
                            <!-- Botones Editar / Borrar -->
                            <a href="{{ route('vacations.edit', $vacation->id) }}" class="fa fa-pencil"> </a> | <a href="{{ route('admin.vacations.destroy', $vacation->id) }}" onclick="return confirm('Segruro deseas Borrarlo')" class="fa fa-trash"></a>
                          </td>
                        </tr>
                      @endforeach
                  </tbody>
              </table>
              <div class="text-center">
                    {!! $vacations->render() !!}
              </div>
          <!--  Fin del Contenido -->
        </div>
      </div>
    </div>
  </div>
</div>
</div>

@endsection