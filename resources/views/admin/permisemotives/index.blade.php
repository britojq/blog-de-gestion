@extends('admin.template.main')

@section('title', 'Listado de Motivos de Permiso')

@section('paneladmin')
    class="active"
@endsection

@section('content')
<div class="content">
  <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header" data-background-color="purple">
              <h4 class="title">Listado Motivos de Permiso</h4>
            </div>
            <div class="card-content">
              <!--  Inicio del contenido -->

              

            <!--  Inicio del contenido -->
  <!--  Inicio del contenido -->
    <a href="{{ route('permisemotives.create') }}" class="btn btn-xs btn-primary">Registrar Motivo de Permiso</a>
     @include('flash::message')
<div class="row margin-top-12">
  <div class="col-lg-12">
    <div class="card">
      <!-- CONTENT -->
      <table class="table table-striped">
          <!-- Encabezado de la Tabla -->
          <thead>
              <th>ID</th>
              <th>Motivos de Permiso</th>
              <th>Accion</th>
          </thead>
          
          <!-- Cuerpo de la Tabla -->
          <tbody>
              @foreach($permisemotives as $permisemotive)
                  <tr>
                    <td>{{ $permisemotive->id }}</td>
                    <td>{{ $permisemotive->name }}</td>
                    <td>
                      <!-- Botones Editar / Borrar -->
                      <a href="{{ route('permisemotives.edit', $permisemotive->id) }}" class="fa fa-pencil"> </a> | <a href="{{ route('admin.permisemotives.destroy', $permisemotive->id) }}" onclick="return confirm('Segruro deseas Borrarlo')" class="fa fa-trash"></a>
                    </td>
                  </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-center">
              {!! $permisemotives->render() !!}
        </div>
    <!-- /CONTENT -->
    </div>
  </div>
</div>
<!-- -->
</div>
</div>
</div>
</div>
</div>
</div>

@endsection