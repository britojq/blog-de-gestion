@extends('admin.template.main')

@section('title', 'Agregar Tipo de Permiso') 

@section('paneladmin')
    class="active"
@endsection
  
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-block">
            	<!-- CONTENT -->
            	<!-- ALERTS & ERRORS -->
        @include('flash::message')
        @include('admin.template.partials.errors')
        <!-- END ALERTS & ERRORS -->

				{!! Form::open(['route' => 'typenomines.store', 'method' => 'POST']) !!}

					<div class="form-group row margin-top-30">
			                        <div class="col-md-2">
			                        	{!! Form::Label('name', 'Tipo de Nomina:', ['class' => 'control-label col-form-label']) !!}
			                            
			                        </div>
			                        
			                        <div class="col-md-10">
			                            {!! Form::Text('name', null, ['class' => 'form-control', 'placeholder' => 'Escriba el tipo de Nomina', 'required']) !!}
			                        </div>
			                    </div>
			                                        
								<div class="pull-right">
									<button type="reset" class="btn btn-secondary">Resetear
									<i class="fa fa-refresh position-right"></i>
								</button>

								<button type="submit" class="btn btn-primary">Registrar
									<i class="fa fa-arrow-right position-right"></i>
									</button>
								</div>

				{!! Form::close() !!}
                <!-- /CONTENT -->         
			</div>
		</div>
	</div>   
</div>

@endsection
