@extends('admin.template.main')

@section('paneladmin')
    class="active"
@endsection

@section('title', 'Listado Tipos de Nomina')

@section('content')
<div class="content">
  <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header" data-background-color="purple">
              <h4 class="title">Listado de Tipos de Nomina</h4>
            </div>
            <div class="card-content">
              <!--  Inicio del contenido -->
                <a href="{{ route('typenomines.create') }}" class="btn btn-xs btn-primary">Registrar Tipo de Nomina</a>
                 @include('flash::message')
              

            <!--  Inicio del contenido -->
 
    
    <div class="row margin-top-12">
  <div class="col-lg-12">
    <div class="card">
      <!-- CONTENT -->
      <table class="table table-striped">
          <!-- Encabezado de la Tabla -->
          <thead>
              <th>ID</th>
              <th>Tipos de Nomina</th>
              <th>Accion</th>
          </thead>
          
          <!-- Cuerpo de la Tabla -->
          <tbody>
              @foreach($typenomines as $typenomine)
                  <tr>
                    <td>{{ $typenomine->id }}</td>
                    <td>{{ $typenomine->name }}</td>
                    <td>
                      <!-- Botones Editar / Borrar -->
                      <a href="{{ route('typenomines.edit', $typenomine->id) }}" class="fa fa-pencil"> </a> | <a href="{{ route('admin.typenomines.destroy', $typenomine->id) }}" onclick="return confirm('Segruro deseas Borrarlo')" class="fa fa-trash"></a>
                    </td>
                  </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-center">
              {!! $typenomines->render() !!}
        </div>
    <!-- /CONTENT -->
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<!-- -->
@endsection