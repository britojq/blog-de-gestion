@extends('admin.template.main')

@section('title', 'Listado de Codigos de Nomina')

@section('paneladmin')
    class="active"
@endsection

@section('content')

<div class="content">
  <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header" data-background-color="purple">
              <h4 class="title">Listado de Codigos de Nomina</h4>
            </div>
            <div class="card-content">
              <!--  Inicio del contenido -->

              <a href="{{ route('codenomines.create') }}" class="btn btn-xs btn-primary">Registrar Codigo de Nomina</a>
               @include('flash::message')

            <!--  Inicio del contenido -->
  <!--  Inicio del contenido -->
    
<div class="row margin-top-12">
  <div class="col-lg-12">
    <div class="card">
      <!-- CONTENT -->
      
      <table class="table table-striped">
          <!-- Encabezado de la Tabla -->
          <thead>
              <th>ID</th>
              <th>Codigo de Nomina</th>
              <th>Accion</th>
          </thead>
          
          <!-- Cuerpo de la Tabla -->
          <tbody>
              @foreach($codenomines as $codenomine)
                  <tr>
                    <td>{{ $codenomine->id }}</td>
                    <td>{{ $codenomine->name }}</td>
                    <td>
                      <!-- Botones Editar / Borrar -->
                      <a href="{{ route('codenomines.edit', $codenomine->id) }}" class="fa fa-pencil"> </a> | <a href="{{ route('admin.codenomines.destroy', $codenomine->id) }}" onclick="return confirm('Segruro deseas Borrarlo')" class="fa fa-trash"></a>
                    </td>
                  </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-center">
              {!! $codenomines->render() !!}
        </div>
     <!-- /CONTENT -->
    </div>
  </div>
</div>
<!-- -->
</div>
</div>
</div>
</div>
</div>
</div>
@endsection