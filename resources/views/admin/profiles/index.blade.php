@extends('admin.template.main')

@section('title', 'Lista de Usuarios')

@section('content')

  @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
  @endif


  <a href="{{ route('users.create') }}" class="btn btn-xs btn-primary">Registrar Usuario</a>



  <!-- BUSCADOR DE TAGS-->
  {!! Form::open(['route' => 'users.index', 'method' => 'GET', 'class' => 'nav-form pull-right']) !!}

    <div class="input-group">
        {!! Form::text('uid', null, ['class' => 'form-control', 'placeholder' => 'Buscar UID', 'aria-describedby' => 'search']) !!}
     <span class="input-group-addon" id="search" >
        <span class="glyphicon glyphicon-search" id="search" aria-hidden="true"></span>
      </span>
    </div>

  {!! Form::close() !!}


<div class="row margin-top-12">
  <div class="col-lg-12">
    <div class="card">
      <!-- CONTENT -->


<!-- /CONTENT -->
    </div>
  </div>
</div>
<!-- -->

@endsection