@extends('admin.template.main')

@section('title', 'Ficha de Datos' ) 

@section('perfil')
    class="active"
@endsection

@section('content')
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-9">
            <div class="card">
              <div class="card-header" data-background-color="purple">
                <h4 class="title">Perfil del Usuario</h4>
              </div>
              <div class="card-content">
                <form>
                  <div class="row">
                    <div class="col-md-5">
                      <div class="form-group label-floating">
                        <label class="control-label">Ceculda</label>
                        <input type="text" class="form-control" value="{{ $users->cedula }}" readonly="readonly">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group label-floating">
                        <label class="control-label">UID</label>
                          <input type="text" class="form-control" value="{{ $users->uid }}" readonly="readonly">
                      </div>
                    </div>
                      <div class="col-md-4">
                        <div class="form-group label-floating">
                          <label class="control-label">Correo Electronico</label>
                          <input type="text" class="form-control" value="{{ $users->email }}" readonly="readonly">
                        </div>
                      </div>
                    </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group label-floating">
                        <label class="control-label">Nombres</label>
                        <input type="text" class="form-control" value="{{ $users->name }}" readonly="readonly">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group label-floating">
                        <label class="control-label">Apellidos</label>
                        <input type="text" class="form-control" value="{{ $users->lastname }}" readonly="readonly">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group label-floating">
                        <label class="control-label">Fecha de Nacimiento</label>
                        <input type="text" class="form-control" value="{{ $users->dob }}" readonly="readonly">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group label-floating">
                        <label class="control-label">Sexo</label>
                        <input type="text" class="form-control" value="{{ $users->gender }}" readonly="readonly">
                      </div>
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group label-floating">
                        <label class="control-label">Telefono Habitacion</label>
                        <input type="text" class="form-control" value="{{ $users->phonehab }}" readonly="readonly">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group label-floating">
                        <label class="control-label">Telefono Celular</label>
                        <input type="text" class="form-control" value="{{ $users->phonecel }}" readonly="readonly">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group label-floating">
                        <label class="control-label">Telefono Oficina</label>
                        <input type="text" class="form-control" value="{{ $users->phoneofc }}" readonly="readonly">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group label-floating">
                        <label class="control-label">Fecha de Ingreso</label>
                        <input type="text" class="form-control" value="{{ $users->dateingres }}" readonly="readonly">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group label-floating">
                        <label class="control-label">Cargo</label>
                        <input type="text" class="form-control" value="{{ $users->carge->name }}" readonly="readonly">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group label-floating">
                        <label class="control-label">NUMERO PERSONAL</label>
                        <input type="text" class="form-control" value="{{ $users->personalnumber }}" readonly="readonly">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group label-floating">
                        <label class="control-label">TIPO DE NOMINA</label>
                        <input type="text" class="form-control" value="{{ $users->typenomine->name }}" readonly="readonly">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group label-floating">
                        <label class="control-label">UBICACION</label>
                        <input type="text" class="form-control" value="{{ $users->location->name }}" readonly="readonly">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group label-floating">
                        <label class="control-label">TIPO DE USUARIO</label>
                        <input type="text" class="form-control" value="{{ $users->type }}" readonly="readonly">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group label-floating">
                        <label class="control-label">GERENCIA</label>
                        <input type="text" class="form-control" value="{{ $users->gerency->name }}" readonly="readonly"> 
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group label-floating">
                        <label class="control-label">UNIDAD</label>
                        <input type="text" class="form-control" value="{{ $users->unit->name }}" readonly="readonly"> 
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group label-floating">
                        <label class="control-label">DIVISION</label>
                        <input type="text" class="form-control" value="{{ $users->division->name }}" readonly="readonly"> 
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group label-floating">
                        <label class="control-label">DEPARTAMENTO</label>
                        <input type="text" class="form-control" value="{{ $users->departament->name }}" readonly="readonly">
                      </div>
                    </div>
                  </div>
                </form>
                <div class="pull-right margin-top-20">
                  <a class="btn btn-primary btn btn-secondary" href="{{ route('admin.index') }}" role="button">Regresar</a>
                </div>
            </div>
          </div>
        </div>
        
        <div class="col-md-3">
          <div class="card card-profile">
            <div class="card-avatar">
              <a href="#pablo">
                <img class="img" src="{{ asset('plugins/admin/assets/img/faces/profile-picture.jpg') }}" />
              </a>
            </div>
            <div class="content">
              <h6 class="category text-gray"> {{ $users->carge->name }}</h6>
              <h4 class="card-title">{{ Auth::user()->name }} . {{ Auth::user()->lastname }}</h4>
              <p class="card-content">
                {{ $users->location->name }}
              </p>
              <a href="#pablo" class="btn btn-primary btn-round">{{ $users->phonecel }}</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection