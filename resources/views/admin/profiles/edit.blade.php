@extends('admin.template.main')

@section('title', 'Editar Usuario' ) 

@section('content')

    @if (session('status'))
        <div class="alert alert-success">
        {{ session('status') }}
        </div>
    @endif


<!-- PAGE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-block">
                        <legend class="text-bold margin-top-40">Informacion Personal</legend>
                            
                            <!-- FORM -->
                            {!! Form::open(['action' => ['ProfilesController@update', $users], 'method' => 'PUT']) !!}
    
                                    
                                <!-- /ONE -->
                                    <div class="row">
                                        <!-- LEFT -->
                                        <div class="col-md-6">
                                            <div class="form-group row margin-top-10">
                                                <div class="col-md-4">
                                                    {!! Form::Label('name', 'Nombre', ['class' => 'control-label col-form-label']) !!}
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="input-group">
                                                        {!! Form::Text('name',  $users->name, ['class' => 'form-control', 'placeholder' => 'Escriba Nombre', 'required']) !!}
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-user"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    {!! Form::Label('lastname', 'Apellido', ['class' => 'control-label col-form-label']) !!}
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="input-group">
                                                        {!! Form::Text('lastname',  $users->lastname, ['class' => 'form-control', 'placeholder' => 'Escriba Apellido', 'required']) !!}
                                                        <span class="input-group-addon">
                                                                <i class="fa fa-user"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    {!! Form::Label('cedula', 'Cedula', ['class' => 'control-label col-form-label']) !!}
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="input-group">
                                                        {!! Form::Text('cedula',  $users->cedula, ['class' => 'form-control', 'placeholder' => 'Escriba Cedula', 'required']) !!}
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-address-card-o"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                                
                                                
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <label class="control-label col-form-label">Fecha de Nacimiento</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input type="date" class="form-control pickadate-locale" placeholder="Haz Click Aqui">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <label class="control-label col-form-label">Genero</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <label class="custom-control custom-radio">
                                                        <input id="radio1" name="radio" type="radio" class="custom-control-input">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">Masculino</span>
                                                    </label>
                                                        <label class="custom-control custom-radio">
                                                        <input id="radio2" name="radio" type="radio" class="custom-control-input">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">Femenino</span>
                                                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /LEFT -->
                                            
                                            <!-- RIGHT -->
                                            <div class="col-md-6">
                                                <div class="form-group row margin-top-10">
                                                    <div class="col-md-4">
                                                        {!! Form::Label('email', 'Correo', ['class' => 'control-label col-form-label']) !!}
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="input-group">
                                                            {!! Form::Email('email',  $users->email, ['class' => 'form-control', 'placeholder' => 'ejemplo@gmail.com', 'required']) !!}
                                                            <span class="input-group-addon">
                                                                <i class="fa fa-envelope-o"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        {!! Form::Label('password', 'Clave:', ['class' => 'control-label col-form-label']) !!}
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="input-group">
                                                            {!! Form::Password('password', ['class' => 'form-control', 'placeholder' => '***********', 'required']) !!}
                                                            <span class="input-group-addon">
                                                                <i class="fa fa-key"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        {!! Form::Label('phonecel', 'Telefono Celular', ['class' => 'control-label col-form-label']) !!}
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="input-group">
                                                            {!! Form::Date('phonecel',  $users->phonecel, ['class' => 'form-control', 'placeholdephonehabr' => '02222222222', 'required']) !!}
                                                            <span class="input-group-addon">
                                                                    <i class="fa fa-mobile"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        {!! Form::Label('phoneofc', 'Telefono Oficina', ['class' => 'control-label col-form-label']) !!}
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="input-group">
                                                            {!! Form::Text('phoneofc',  $users->phoneofc, ['class' => 'form-control', 'placeholder' => 'Selecciona', 'required']) !!}
                                                            <span class="input-group-addon">
                                                                    <i class="fa fa-phone-square"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        {!! Form::Label('phonehab', 'Telefono Hab', ['class' => 'control-label col-form-label']) !!}
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="input-group">
                                                            {!! Form::Date('phonehab', $users->phonehab, ['class' => 'form-control', 'placeholdephonehabr' => '02222222222', 'required']) !!}
                                                            <span class="input-group-addon">
                                                                    <i class="fa fa-phone"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>                                                
                                            </div>
                                            <!-- /RIGHT -->
                                        </div>
                                    <!-- /ONE -->
                                    
                                    <!-- TWO -->
                                        <legend class="text-bold margin-top-40">Informacion Corporativa</legend>
                                        <!-- LEFT -->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row margin-top-10">
                                                    <div class="col-md-4">
                                                        {!! Form::Label('dateingres', 'Fecha de Ingreso', ['class' => 'control-label col-form-label']) !!}
                                                    </div>
                                                    <div class="col-md-8">
                                                        {!! Form::Date('dateingres',  $users->dateingres, ['class' => 'form-control pickadate-dropdown', 'placeholder' => 'seleccione', 'required']) !!}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        {!! Form::Label('gerency_id', 'Gerencia', ['class' => 'control-label col-form-label']) !!}
                                                    </div>
                                                    <div class="col-md-8">
                                                        {!! Form::Select('gerency_id', $gerencys, $users->gerency_id, ['class' => 'form-control select-gerencys', 'placeholder' => 'Selecione una Opcion..',  'required']) !!}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        {!! Form::Label('unit_id', 'Unidad', ['class' => 'control-label col-form-label']) !!}
                                                    </div>
                                                    <div class="col-md-8">
                                                        {!! Form::Select('unit_id', $units, $users->unit_id, ['class' => 'form-control select-units','placeholder' => 'Selecione una Opcion..', 'required']) !!}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        {!! Form::Label('division_id', 'Division', ['class' => 'control-label col-form-label']) !!}
                                                    </div>
                                                    <div class="col-md-8">
                                                        {!! Form::Select('division_id', $divisions, $users->division_id, ['class' => 'form-control select-divisions', 'placeholder' => 'Selecione una Opcion..', 'required']) !!}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        {!! Form::Label('departament_id', 'Departamento', ['class' => 'control-label col-form-label']) !!}
                                                    </div>
                                                    <div class="col-md-8">
                                                        {!! Form::Select('departament_id', $departaments, $users->departament_id, ['class' => 'form-control select-departements','placeholder' => 'Selecione una Opcion..', 'required']) !!}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        {!! Form::Label('typenomine_id', 'Tipo de Nomina', ['class' => 'control-label col-form-label']) !!}
                                                    </div>
                                                    <div class="col-md-8">
                                                        {!! Form::Select('typenomine_id', $typenomines, $users->typenomine_id, ['class' => 'form-control select-typenomines', 'placeholder' => 'Selecione una Opcion..',  'required']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /LEFT -->
                                            <!-- RIGHT -->
                                            <div class="col-md-6">
                                                <div class="form-group row margin-top-10">
                                                    <div class="col-md-4">
                                                        {!! Form::Label('carge_id', 'Cargo', ['class' => 'control-label col-form-label']) !!}
                                                    </div>
                                                    <div class="col-md-8">
                                                        {!! Form::Select('carge_id', $carges, $users->carge_id, ['class' => 'form-control select-carge', 'placeholder' => 'Selecione una Opcion..',  'required']) !!}
                                                    </div>
                                                </div>
                                                <div class="form-group row margin-top-10">
                                                    <div class="col-md-4">
                                                        {!! Form::Label('uid', 'UID:', ['class' => 'control-label col-form-label']) !!}
                                                    </div>
                                                    <div class="col-md-8">
                                                        {!! Form::Text('uid',  $users->uid, ['class' => 'form-control', 'placeholder' => 'A1111111  O  J111111', 'required']) !!}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        {!! Form::Label('personalnumber', 'Numero Personal', ['class' => 'control-label col-form-label']) !!}
                                                    </div>
                                                    <div class="col-md-8">
                                                        {!! Form::Text('personalnumber',  $users->personalnumber, ['class' => 'form-control', 'placeholder' => '0000123456', 'required']) !!}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        {!! Form::Label('unitcode_id', 'Codigo de Unidad', ['class' => 'control-label col-form-label']) !!}
                                                    </div>
                                                    <div class="col-md-8">
                                                        {!! Form::Select('unitcode_id', $unitcodes, $users->unitcode_id, ['class' => 'form-control select-unitcodes', 'placeholder' => 'Selecione una Opcion..', 'required']) !!}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        {!! Form::Label('location_id', 'Ubicacion', ['class' => 'control-label col-form-label']) !!}
                                                    </div>
                                                    <div class="col-md-8">
                                                        {!! Form::Select('location_id', $locations, $users->location_id, ['class' => 'form-control select-locations', 'placeholder' => 'Selecione una Opcion..',  'required']) !!}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        {!! Form::Label('type', 'Tipo de Usuario', ['class' => 'control-label col-form-label']) !!}
                                                    </div>
                                                    <div class="col-md-8">
                                                        {!! Form::select('type', ['member' => 'Miembro', 'Admin' => 'Administrador'], null, ['class' => 'form-control', 'placeholder' => 'Selecione una Opcion..', 'required']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="pull-right margin-top-20">
                                            <a class="btn btn-primary btn btn-secondary" href="{{ route('users.index') }}" role="button">Regresar</a>
                                            
                                            <button type="reset" class="btn btn-secondary">
                                                Resetear
                                                <i class="fa fa-refresh position-right"></i>
                                            </button>

                                            <button type="submit" class="btn btn-primary">
                                                Registrar
                                                <i class="fa fa-arrow-right position-right"></i>
                                            </button>
                                            
                                            
                                        </div>
                                    
                                    
                                    {!! Form::close() !!}
                                    <!-- /FORM -->
                                </div>
                            </div>
                        </div>
                    </div>


    

@endsection