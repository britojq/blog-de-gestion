@extends('admin.template.main')

@section('title', 'Listado de Departamentos')

@section('paneladmin')
    class="active"
@endsection

@section('content')
<div class="content">
  <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header" data-background-color="purple">
              <h4 class="title">Listado de Permisos</h4>
            </div>
            <div class="card-content">
              <!--  Inicio del contenido -->

              <a href="{{ route('departaments.create') }}" class="btn btn-xs btn-primary">Registrar Departamento</a>
               @include('flash::message')

            <!--  Inicio del contenido -->
  


      <table class="table table-striped">
          <!-- Encabezado de la Tabla -->
          <thead>
              <th>ID</th>
              <th>Departamento</th>
              <th>Accion</th>
          </thead>
          
          <!-- Cuerpo de la Tabla -->
          <tbody>
              @foreach($departaments as $departament)
                  <tr>
                    <td>{{ $departament->id }}</td>
                    <td>{{ $departament->name }}</td>
                    <td>
                      <!-- Botones Editar / Borrar -->
                      <a href="{{ route('departaments.edit', $departament->id) }}" class="fa fa-pencil"> </a> | <a href="{{ route('admin.departaments.destroy', $departament->id) }}" onclick="return confirm('Segruro deseas Borrarlo')" class="fa fa-trash"></a>
                    </td>
                  </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-center">
              {!! $departaments->render() !!}
        </div>
    <!-- /CONTENT -->
    </div>
  </div>
</div>
<!-- -->
</div>
</div>
</div>
</div>
</div>
</div>

@endsection