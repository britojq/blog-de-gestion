@extends('admin.template.main')

@section('paneladmin')
    class="active"
@endsection

@section('title', 'Listado Unidades')

@section('content')
<div class="content">
  <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header" data-background-color="purple">
              <h4 class="title">Listado de Permisos</h4>
            </div>
            <div class="card-content">
            <!--  Inicio del contenido -->
              <a href="{{ route('units.create') }}" class="btn btn-primary">Registrar Unidad</a>
               @include('flash::message')
            <!--  Inicio del contenido -->  
            <table class="table table-striped">
              <div class="row margin-top-12">
                <div class="col-lg-12">
                  <div class="card">
                    <!-- Encabezado de la Tabla -->
                    <thead>
                        <th>ID</th>
                        <th>Unidades</th>
                        <th>Accion</th>
                    </thead>
                    
                    <!-- Cuerpo de la Tabla -->
                    <tbody>
                        @foreach($units as $unit)
                            <tr>
                              <td>{{ $unit->id }}</td>
                              <td>{{ $unit->name }}</td>
                              <td>
                                <!-- Botones Editar / Borrar -->
                                <a href="{{ route('units.edit', $unit->id) }}" class="fa fa-pencil"> </a> | <a href="{{ route('admin.units.destroy', $unit->id) }}" onclick="return confirm('Segruro deseas Borrarlo')" class="fa fa-trash"></a>
                              </td>
                            </tr>
                          @endforeach
                    </tbody>
            </table>
            <div class="text-center">
                  {!! $units->render() !!}
            </div>
            <!-- /CONTENT -->
          </div>
        </div>
      </div>
  </div>
</div>
</div>
</div>
</div>
</div>

<!-- -->
@endsection