@extends('admin.template.main')

@section('title', 'Listado de Gerencias')

@section('paneladmin')
    class="active"
@endsection

@section('content')
<div class="content">
  <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header" data-background-color="purple">
              <h4 class="title">Listado de Gerencias</h4>
            </div>
            <div class="card-content">
              <!--  Inicio del contenido -->

              <a href="{{ route('gerencys.create') }}" class="btn btn-xs btn-primary">Registrar Gerencia</a>
               @include('flash::message')

            <!--  Inicio del contenido -->

  <!--  Inicio del contenido -->
    

<div class="row margin-top-12">
  <div class="col-lg-12">
    <div class="card">
      <!-- CONTENT -->

      <table class="table table-striped">
          <!-- Encabezado de la Tabla -->
          <thead>
              <th>ID</th>
              <th>Gerencia</th>
              <th>Accion</th>
          </thead>
          
          <!-- Cuerpo de la Tabla -->
          <tbody>
              @foreach($gerencys as $gerency)
                  <tr>
                    <td>{{ $gerency->id }}</td>
                    <td>{{ $gerency->name }}</td>
                    <td>
                      <!-- Botones Editar / Borrar -->
                      <a href="{{ route('gerencys.edit', $gerency->id) }}" class="fa fa-pencil"> </a> | <a href="{{ route('admin.gerencys.destroy', $gerency->id) }}" onclick="return confirm('Segruro deseas Borrarlo')" class="fa fa-trash"></a>
                    </td>
                  </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-center">
              {!! $gerencys->render() !!}
        </div>
    <!-- /CONTENT -->
    </div>
  </div>
</div>
<!-- -->
</div>
</div>
</div>
</div>
</div>
</div>

@endsection