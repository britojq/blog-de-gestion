@extends('admin.template.main')

@section('title', 'Agregar Codigo de Horario') 

@section('paneladmin')
    class="active"
@endsection

@section('content')
<!-- ALERTS & ERRORS -->
        @include('flash::message')
        @include('admin.template.partials.errors')
        <!-- END ALERTS & ERRORS -->
	{!! Form::open(['route' => 'journalcodes.store', 'method' => 'POST']) !!}

		<div class="form-group">
			{!! Form::Label('name', 'Codigo de Horario', ['class' => 'col-sm-3 control-label']) !!}
		</div>	
		 
		 <div class="col-sm-6">
			{!! Form::Text('name', null, ['class' => 'form-control', 'placeholder' => 'Codigo de Horario', 'required']) !!}
		
		</div>

		<div class="form-group">
			{!! Form::Submit('Registrar', ['class' => 'btn btn-primary', 'class' => 'col-md-offset-6 col-md-6']) !!}
		</div>
	
	{!! Form::close() !!}
@endsection
