@extends('admin.template.main')

@section('title', 'Editar Codigo de Horario')

@section('paneladmin')
    class="active"
@endsection

@section('content')
<!-- ALERTS & ERRORS -->
        @include('flash::message')
        @include('admin.template.partials.errors')
        <!-- END ALERTS & ERRORS -->

	{!! Form::open(['action' => ['JournalcodesController@update', $journalcode], 'method' => 'PUT']) !!}

		<div class="form-group">
			{!! Form::Label('name', 'Codigo de Horario', ['class' => 'col-sm-3 control-label']) !!}
		 <div class="col-sm-6">
			{!! Form::Text('name', $journalcode->name, ['class' => 'form-control', 'placeholder' => 'Codigo de Horario', 'required']) !!}
		</div>	
		</div>

		<div class="form-group">
			{!! Form::Submit('Editar', ['class' => 'btn btn-primary', 'class' => 'col-md-offset-6 col-md-6']) !!}
		</div>
	
	{!! Form::close() !!}


@endsection