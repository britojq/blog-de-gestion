@extends('admin.template.main')

@section('title', 'Listado de Asistencia')

@section('solicitudes')
    class="active"
@endsection

@section('content')
<div class="content">
  <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header" data-background-color="purple">
              <h4 class="title">Listado de Asistencia</h4>
            </div>
            <div class="card-content">
              <!--  Inicio del contenido -->
                <a href="{{ route('timetrackings.create') }}" class="btn btn-primary btn-xs">Registrar Asistencia</a>
                @include('flash::message')
            <!--  Inicio del contenido -->
  
    
      <table class="table table-striped">
          <!-- Encabezado de la Tabla -->
          <thead>
              <th>ID</th>
              <th>Nombre</th>
              <th>Apellido</th>
              <th>Fecha Marcaje</th>
              <th>Hora Entrada</th>
              <th>Hora Salida</th>
              <th>Accion</th>
          </thead>
          
          <!-- Cuerpo de la Tabla -->
          <tbody>
              @foreach($timetrackings as $timetracking)
                  <tr>
                    <td>{{ $timetracking->id }}</td>
                    <td>{{ $timetracking->user->name }}</td>
                    <td>{{ $timetracking->user->lastname }}</td>
                    <td>{{ \Carbon\Carbon::parse($timetracking->current_date)->format('d/m/Y')}}</td>
                    <td>{{ $timetracking->time_in }}</td>
                    <td>{{ $timetracking->time_out }}</td>
                    <td>
                      <!-- Botones Editar / Borrar -->
                      <a href="{{ route('timetrackings.edit', $timetracking->id) }}" class="fa fa-pencil"> </a> | <a href="{{ route('admin.timetrackings.destroy', $timetracking->id) }}" onclick="return confirm('Segruro deseas Borrarlo')" class="fa fa-trash"></a>
                    </td>
                  </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-center">
              {!! $timetrackings->render() !!}
        </div>
    <!--  Fin del Contenido -->
  </div>
</div>
</div>
</div>
</div>
</div>

@endsection