@extends('admin.template.maintimepicker')

@section('title', 'Agregar Asistencia') 

@section('solicitudes')
    class="active"
@endsection
  
@section('content')
	
	<div class="content">
      <div class="container-fluid">
	<!-- PAGE CONTENT -->
	<!-- ALERTS & ERRORS -->
        @include('flash::message')
        @include('admin.template.partials.errors')
        <!-- END ALERTS & ERRORS -->
        <div class="row">
       {!! Form::open(['route' => 'timetrackings.store', 'method' => 'POST', 'files' => true]) !!}
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                    <h4 class="title">Registrar Asistencia</h4>
                    </div>
                    <div class="card-content">
							 <div class="row">
									<div class="form-group">
										
										{!! Form::Label('current_date', 'Fecha Asistencia', ['class' => 'col-sm-2 control-label']) !!}
										 	<div class="col-sm-4">
										 		<div class="input-group">
												{!! Form::Date('current_date',  null, ['class' => 'form-control datetimepickerF', 'placeholder' => 'seleccione', 'required']) !!}
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											</div>
										</div>	
									</div>
							</div>
							<!-- -->
							<br>
							<!-- -->
								<div class="row">	
									<div class="form-group">
							 			{!! Form::Label('time_in', 'Hora Entrada:', ['class' => 'col-sm-2 control-label']) !!}
										 	<div class="col-sm-4">
											 	<div class="input-group">
											 		{!! Form::Time('time_in',  null, ['class' => 'form-control datetimepickerT', 'placeholder' => 'seleccione', 'id' => 'timepicker', 'required']) !!}
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>

											{!! Form::Label('time_out', 'Hora Salida', ['class' => 'col-sm-2 control-label']) !!}
										 	<div class="col-sm-4">
										 		<div class="input-group">
											 		{!! Form::Time('time_out',  null, ['class' => 'form-control datetimepickerT', 'placeholder' => 'seleccione', 'id' => 'timepicker2', 'required']) !!}
													
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>
													
									</div>
								</div>
							<!-- -->


							<br>
							<center><div class="form-group">
									{!! Form::Submit('Registrar Asistencia', ['class' => 'btn btn-primary']) !!}
							</div></center>
							<br>
		{!! Form::close() !!}

	<script type="text/javascript">
		
		$('#timepicker').timepicker({
	    'minTime': '5:00am',
	    'maxTime': '9:00pm',
	    'showDuration': true
		});

	</script>

	<script type="text/javascript">

	    $('#timepicker2').timepicker({
	    'minTime': '5:00am',
	    'maxTime': '9:00pm',
	    'showDuration': true
		});

	</script>

	<script>
	    $('.datepicker').datepicker({
	        format: "yyyy/mm/dd",
	        language: "es",
	        autoclose: true
	    });
	</script>

@endsection

@section('js')
	<script>
		$('.select-tag').chosen({
			placeholder_text_multiple: 'Seleccione un maximo de 3 Tags', max_selected_options: 3, no_results_text: "No se encuentra este tags"
		});

		$('.select-category').chosen({
			placeholder_text_single: 'Seleccione una Categoria', no_results_text: "No se encuentra esta Categoria"
		});
	
		$('.textarea-content').trumbowyg({
			lang: 'es',
		});

	</script>

@endsection