@extends('admin.template.main')

@section('title', 'Listado de Imagenes')

@section('blog')
    class="active"
@endsection

@section('content')
<div class="content">
  <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header" data-background-color="purple">
              <h4 class="title">Listado de Imagenes</h4>
            </div>
            <div class="card-content">
              <!--  Inicio del contenido -->
			<div class="row">
				@foreach ($images as $images)
					<div class="col-md-6">
						
						<div class="panel panel-default">
						  <div class="panel-body">
						    <img src="/images/articles/{{ $images->name }}" class="img-responsive">
						  </div>
						  
						  <div class="panel-footer">
						  	Asociada con : {{ $images->article->title }}
						  </div>

						</div>
					</div>
					
				@endforeach
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>


@endsection