@extends('admin.template.main')

@section('title', 'Panel de Solicitudes')

@section('solicitudes')
    class="active"
@endsection

@section('content')

<!-- CONTENIDO -->
    <div class="content">
        <div class="container-fluid">
            <!-- BLOQUE -->
                <div class="card card-nav-tabs">
                    <div class="card-header" data-background-color="purple" style="height: 6rem;">
                        <h5>ADMINISTRACION</h5>
                    </div>

                    <div class="card-body ">
                        <div class="tab-content text-center">
                        <br>
                        <div class="row col-lg-12">
                            <div class="col-lg-2 col-md-4 col-sm-4">
                                <div class="card card-stats">
                                    <div class="card-header" data-background-color="green">
                                        <i class="fa fa-user-plus"></i>
                                    </div>
                                    <div class="card-footer">
                                        <div class="stats">
                                            <a href="{{ route('vacations.index') }}">VACACIONES</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-4 col-sm-4">
                                <div class="card card-stats">
                                    <div class="card-header" data-background-color="green">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <div class="card-footer">
                                        <div class="stats">
                                            <a href="{{ route('permises.index') }}">PERMISOS</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-4 col-sm-4">
                                <div class="card card-stats">
                                    <div class="card-header" data-background-color="green">
                                        <i class="fa fa-reorder"></i>
                                    </div>
                                    <div class="card-footer">
                                        <div class="stats">
                                            <a href="{{ route('overtimes.index') }}">SOBRETIEMPOS</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-4 col-sm-4">
                                <div class="card card-stats">
                                    <div class="card-header" data-background-color="green">
                                        <i class="fa fa-file-text"></i>
                                    </div>
                                    <div class="card-footer">
                                        <div class="stats">
                                            <a href="{{ route('timetrackings.index') }}">ASISTENCIA</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-4 col-sm-4">
                                <div class="card card-stats">
                                    <div class="card-header" data-background-color="green">
                                        <i class="fa fa-file-text"></i>
                                    </div>
                                    <div class="card-footer">
                                        <div class="stats">
                                            <a href="#">VIATICOS</a>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>
                    <!-- BLOQUE -->
                </div>
            </div>
            <!-- FIN CONTENIDO -->
            </div>

@endsection