@extends('admin.template.main')

@section('paneladmin')
    class="active"
@endsection

@section('title', 'Listado Codigos de Unidad')

@section('content')
<div class="content">
  <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header" data-background-color="purple">
              <h4 class="title">Listado de Codigos de Unidad</h4>
            </div>
            <div class="card-content">
              <!--  Inicio del contenido -->
                <a href="{{ route('unitcodes.create') }}" class="btn btn-xs btn-primary">Registrar Codigos de Unidad</a>
                 @include('flash::message')
              <!--  Inicio del contenido -->
            <div class="row margin-top-12">
              <div class="col-lg-12">
                <div class="card">
                <!-- CONTENT -->
                  <table class="table table-striped">
                  <!-- Encabezado de la Tabla -->
                    <thead>
                        <th>ID</th>
                        <th>Codigos de Unidad</th>
                        <th>Accion</th>
                    </thead>
                    <!-- Cuerpo de la Tabla -->
                    <tbody>
                        @foreach($unitcodes as $unitcode)
                            <tr>
                              <td>{{ $unitcode->id }}</td>
                              <td>{{ $unitcode->name }}</td>
                              <td>
                                <!-- Botones Editar / Borrar -->
                                <a href="{{ route('unitcodes.edit', $unitcode->id) }}" class="fa fa-pencil"> </a> | <a href="{{ route('admin.unitcodes.destroy', $unitcode->id) }}" onclick="return confirm('Segruro deseas Borrarlo')" class="fa fa-trash"></a>
                              </td>
                            </tr>
                          @endforeach
                    </tbody>
                  </table>
                  <div class="text-center">
                        {!! $unitcodes->render() !!}
                  </div>
                  <!-- /CONTENT -->
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<!-- -->
@endsection