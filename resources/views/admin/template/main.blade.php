<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="plugins/admin/assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="{{ asset('plugins/admin/assets/img/favicon.png') }}" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>@yield('title', 'Default') | Sistema </title>
    <!--                                             -->
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="{{ asset('plugins/admin/assets/css/bootstrap.min.css') }}" rel="stylesheet" />

    <!--  Material Dashboard CSS    -->
    <link href="{{ asset('plugins/admin/assets/css/material-dashboard.css?v=1.2.0') }}" rel="stylesheet" />
    
    <!-- DESCARGAR -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>

    <!-- BLOG -->
    <link rel="stylesheet" href="{{asset('plugins/admin/chosen/chosen.css') }}">
    <link rel="stylesheet" href="{{asset('plugins/admin/trumbowyg/ui/trumbowyg.css') }}">
    <!-- BLOG -->

    
</head>

<body>

    <div class="content">
        <div class="container-fluid">
            <!-- NAVIGATION MENU -->
            @include('admin.template.partials.nav')
            <!-- NAVIGATION MENU -->

            <!-- PAGE CONTENT -->    
            @include('flash::message')
            
            @yield('content')
            <!-- /PAGE CONTENT -->
        </div>
    </div>
    
</body>
    
    <!--   Core JS Files   -->
    <script src="{{ asset('plugins/admin/assets/js/jquery-3.2.1.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/admin/assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/admin/assets/js/material.min.js') }}" type="text/javascript"></script>
    <!--  Charts Plugin -->
    <script src="{{ asset('plugins/admin/assets/js/chartist.min.js') }}"></script>
    <!--  Dynamic Elements plugin -->
    <script src="{{ asset('plugins/admin/assets/js/arrive.min.js') }}"></script>
    <!--  PerfectScrollbar Library -->
    <script src="{{ asset('plugins/admin/assets/js/perfect-scrollbar.jquery.min.js') }}"></script>
    <!--  Notifications Plugin    -->
    <script src="{{ asset('plugins/admin/assets/js/bootstrap-notify.js') }}"></script>
    <!--  Notifications Plugin    -->
    <script src="{{ asset('plugins/admin/assets/js/bootstrap-notify.js') }}"></script>
    <!-- Material Dashboard javascript methods -->
    <script src="{{ asset('plugins/admin/assets/js/material-dashboard.js?v=1.2.0') }}"></script>
    <!-- Material Dashboard DEMO methods, don't include it in your project! -->
    <script src="{{ asset('plugins/admin/assets/js/demo.js') }}"></script>


    <script src="{{ asset('plugins/chosen/chosen.jquery.js') }}"></script>
    <script src="{{ asset('plugins/trumbowyg/trumbowyg.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            // Javascript method's body can be found in assets/js/demos.js
            demo.initDashboardPageCharts();

        });
    </script>

    <script>
        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    </script>
    @yield('js')
</html>