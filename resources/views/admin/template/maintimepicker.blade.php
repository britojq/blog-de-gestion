<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="plugins/admin/assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="{{ asset('plugins/admin/assets/img/favicon.png') }}" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>@yield('title', 'Default') | Sistema </title>
    
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- Bootstrap core CSS     -->
    <link href="{{ asset('plugins/admin/assets/css/bootstrap.min.css') }}" rel="stylesheet" />

    <!--  Material Dashboard CSS    -->
    <link href="{{ asset('plugins/admin/assets/css/material-dashboard.css?v=1.2.0') }}" rel="stylesheet" />
    
    <!-- DESCARGAR -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="{{ asset('plugins/admin/assets/css/bootstrap-datetimepicker.min.css') }}" />


    
</head>
<body>

    <div class="content">
        <div class="container-fluid">
            <!-- NAVIGATION MENU -->
            @include('admin.template.partials.navtimepicker')
            <!-- NAVIGATION MENU -->

            <!-- PAGE CONTENT -->    
            @include('flash::message')
           
            @yield('content')
            <!-- /PAGE CONTENT -->
        </div>
    </div>
     

    <!--   Core JS Files   -->
    
    <!-- VERIFICAR JQUERY YA QUE NO ES COMPATIBLE EL MENU-->
    <script src="{{ asset('plugins/admin/assets/js/jquery-3.2.1.min.js') }}" type="text/javascript"></script>

    <!--  PerfectScrollbar Library -->
    <script src="{{ asset('plugins/admin/assets/js/perfect-scrollbar.jquery.min.js') }}"></script>
    <!--  Bootstrap Library -->
    <script src="{{ asset('plugins/admin/assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <!--  Material Library -->
    <script src="{{ asset('plugins/admin/assets/js/material.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/admin/assets/js/bootstrap-material-design.js') }}"></script>
    
    <!-- Material Dashboard javascript methods -->
    <script src="{{ asset('plugins/admin/assets/js/material-dashboard.js?v=1.2.0') }}"></script>
    <!--  Notifications Plugin    -->
    <script src="{{ asset('plugins/admin/assets/js/bootstrap-notify.js') }}"></script>
    
    <!--   Popper   -->
    <script src="{{ asset('plugins/admin/assets/js/core/popper.min.js') }}"></script>
    
    <!--  Plugin for Date Time Picker and Full Calendar Plugin  -->
    <script src="{{ asset('plugins/admin/assets/js/plugins/moment.min.js') }}"></script>
    <script src="{{ asset('plugins/admin/assets/js/plugins/es.js') }}"></script>
    
    <!--    Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
    <script src="{{ asset('plugins/admin/assets/js/plugins/bootstrap-datetimepicker.min.js') }}"></script>

    <!-- Material Kit Core initialisations of plugins and Bootstrap Material Design Library -->
    <script src="{{ asset('plugins/admin/assets/js/material-kit.js?v=2.0.2') }}"></script>

<script>
            $('.datetimepickerT').datetimepicker({
        icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove',
                },
        format: 'LT'
            });
    </script>

    <script>
            $('.datetimepickerF').datetimepicker({
        icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove',
                },
        format: 'L'
            });
            
    </script>
    <script>
        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    </script>
    
</body>

</html>