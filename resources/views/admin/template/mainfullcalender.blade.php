
<!doctype html>

<html lang="es">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="plugins/admin/assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="{{ asset('plugins/admin/assets/img/favicon.png') }}" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
     <title>@yield('title', 'Default') | Sistema </title>
    <!--                                             -->
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- Bootstrap core CSS     -->
    <link href="{{ asset('plugins/admin/assets/css/bootstrap.min.css') }}" rel="stylesheet" />

    <!--  Material Dashboard CSS    -->
    <link href="{{ asset('plugins/admin/assets/css/material-dashboard.css?v=1.2.0') }}" rel="stylesheet" />
    
    <!-- DESCARGAR -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
    <script src="{{ asset('plugins/admin/assets/js/jquery-3.2.1.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/admin/assets/css/bootstrap.min.css') }}">
    
    <!-- -->
    <script src="{{ asset('plugins/admin/fullcalender/moment.min.js') }}"></script>

    <script src="{{ asset('plugins/admin/fullcalender/fullcalendar.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('plugins/admin/fullcalender/fullcalendar.min.css') }}"/>
    <!-- -->
</head>

<body>

    <div class="content">
        <div class="container-fluid">
            <!-- NAVIGATION MENU -->
            @include('admin.template.partials.nav')
            <!-- NAVIGATION MENU -->
            @yield('content')
            <!-- /PAGE CONTENT -->
        </div>
    </div>
            
</body>
<!--   Core JS Files   -->
   
    <script src="{{ asset('plugins/admin/assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/admin/assets/js/material.min.js') }}" type="text/javascript"></script>
    
    <!--  Dynamic Elements plugin -->
    <script src="{{ asset('plugins/admin/assets/js/arrive.min.js') }}"></script>
    <!--  PerfectScrollbar Library -->
    <script src="{{ asset('plugins/admin/assets/js/perfect-scrollbar.jquery.min.js') }}"></script>
    
    
    <!-- Material Dashboard javascript methods -->
    <script src="{{ asset('plugins/admin/assets/js/material-dashboard.js?v=1.2.0') }}"></script>
    



</html>
