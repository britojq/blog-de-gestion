<div class="wrapper">
        <div class="sidebar" data-color="purple" data-image="plugins/admin/assets/img/sidebar-1.jpg">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

        Tip 2: you can also add an image using data-image tag
    -->
            
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <li @yield('principal')> 
                        <a href="/admin">
                            <i class="fa fa-th-large"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <li @yield('perfil')>
                        <a href="{{ route('profiles.show', $id=Auth::user()->id) }}">
                            <i class="fa fa-user"></i>
                            <p>Perfil del Usuario</p>
                        </a>
                    </li>
                    <li @yield('solicitudes')>
                        <a href="{{ route('solicits.index') }}">
                            <i class="fa fa-file-text"></i>
                            <p>Solicitudes</p>
                        </a>
                    </li>
                    <li @yield('blog')>
                        <a href="{{ route('articles.index') }}">
                            <i class="fa fa-wikipedia-w"></i>
                            <p>Blog</p>
                        </a>
                    </li>
                    <li @yield('proyectos')>
                        <a href="{{ route('projects.index') }}">
                            <i class="fa fa-line-chart"></i>
                            <p>Proyectos</p>
                        </a>
                    </li>
                    <li @yield('calender')>
                        <a href="{{ route('calenders.index') }}">
                            <i class="fa fa-calendar"></i>
                            <p>Calendario</p>
                        </a>
                    </li>
                    <li @yield('utilidades')>
                        <a href="{{ route('utilities.index') }}">
                            <i class="fa fa-superpowers"></i>
                            <p>Utilidades</p>
                        </a>
                    </li>
                    @if(Auth::user()->type == "admin")
                     <li @yield('paneladmin')>
                        <a href="{{ route('components.index') }}">
                            <i class="fa fa-gears"></i>
                            <p>Panel de Admin</p>
                        </a>
                    </li>
                    @endif
                    
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"> @yield('title', 'Default') | Sistema  </a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a class="nav-link dropdown-toggle dropdown-has-after" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false">
                                     {{ Auth::user()->name }} . {{ Auth::user()->lastname }} 
                                </a>  
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-user-circle"></i>
                                    <p class="hidden-lg hidden-md">Usuario</p>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="/">Principal del Blog</a>
                                    </li>
                                    <li>

                                        <a href="{{ route('profiles.show', $id=Auth::user()->id) }}">Perfil</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">Salir</a>
                                    </li>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                    </form>
                                </ul>
                            </li>
                            
                        </ul>
                    </div>
                </div>
            </nav>
