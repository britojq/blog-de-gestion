	@if(count($errors) > 0)
		<div class="alert alert-primary alert-with-icon" data-notify="container">
           <ul>
				@foreach($errors->all() as $error)
					<li> {{ $error }} </li>
				@endforeach
			</ul>                             
        </div>
	@endif