<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head> 

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

     <link href="{{ asset('plugins/admin/assets/css/bootstrap.min.css') }}" rel="stylesheet" />
     <!--  Material Dashboard CSS    -->
    <link href="{{ asset('plugins/admin/assets/css/material-dashboard.css?v=1.2.0') }}" rel="stylesheet" />

    <!-- DESCARGAR -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
     
    
    
    
    <!-- Jquery  -->
    <script src="{{ asset('plugins/admin/assets/js/jquery-3.2.1.min.js') }}" type="text/javascript"></script>
    <!-- 
    <script src="{{asset('plugins/jquery/js/jquery-1.11.3.min.js')}}"></script>-->
    
    

    <!-- full calender flies-->

    <link rel="stylesheet" href="{{asset('plugins/admin/fullcalender/fullcalendar.min.css') }}">

    <!-- Languaje -->
    <script src="{{asset('plugins/admin/datepicker/locales/bootstrap-datepicker.es.min.js')}}"></script>
    


</head>

<body>
    <!-- Menu de Navegacion -->
    
    <!-- Fin Menu de Navegacion -->

    <!-- Inicio Panel  Central de Contenido -->  
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">@yield('title', 'Default')</div>
                          <div class="panel-body">
                            <section>
                               @include('flash::message')
                               @include('admin.template.partials.errors')
                               <!-- Contenido a mostrar -->
                               @yield('content')
                            </section>

                            
                            

          


                            <script>
                                  $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
                            </script>
                            @yield('js')


              <!-- Fin Contenido del panel  -->
              </div>
           </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Fin Panel  -->
</body>
<!--   Core JS Files   -->
    <script src="{{ asset('plugins/admin/assets/js/jquery-3.2.1.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/admin/assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/admin/assets/js/material.min.js') }}" type="text/javascript"></script>
    <!-- Material Dashboard javascript methods -->
    <script src="{{ asset('plugins/admin/assets/js/material-dashboard.js?v=1.2.0') }}"></script>
</html>
