@extends('admin.template.main')

@section('title', 'Lista de Tags')

@section('paneladmin')
    class="active"
@endsection

@section('content')
<div class="content">
  <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header" data-background-color="purple">
              <h4 class="title">Listado de Permisos</h4>
            </div>
            <div class="card-content">
              <!--  Inicio del contenido -->

              <a href="{{ route('tags.create') }}" class="btn btn-xs btn-info">Registrar Nuevo Tag</a>
               @include('flash::message')

            <!--  Inicio del contenido -->


<!-- BUSCADOR DE TAGS-->
  {!! Form::open(['route' => 'tags.index', 'method' => 'GET', 'class' => 'nav-form pull-right']) !!}

    <div class="input-group">
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Buscar Tag', 'aria-describedby' => 'search']) !!}
     <span class="input-group-addon" id="search" >
        <span class="glyphicon glyphicon-search" id="search" aria-hidden="true"></span>
      </span>
    </div>

  {!! Form::close() !!}

<!-- FIN BUSCADOR DE TAGS-->
<div class="row margin-top-12">
  <div class="col-lg-12">
    <div class="card">
      <!-- CONTENT -->
 	<table class="table table-striped">
      <thead>
        <th>ID</th>
        <th>Nombre del Tag</th>
      	<th>Accion</th>
      </thead>

    	<tbody>
        @foreach($tags as $tag)
          <tr>
            <td>{{ $tag->id }}</td>
            <td>{{ $tag->name }}</td>
            <td>
            <a href="{{ route('tags.edit', $tag->id) }}" class="fa fa-pencil"> </a> | <a href="{{ route('admin.tags.destroy', $tag->id) }}" onclick="return confirm('Segruro deseas Borrarlo')" class="fa fa-trash"></a>
            </td>
          </tr>
        @endforeach
      </tbody>
	 
   </table>

  <div class="text-center">
    {!! $tags->render() !!}
  </div>
        <!-- /CONTENT -->
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<!-- -->
@endsection