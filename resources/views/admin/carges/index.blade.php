@extends('admin.template.main')

@section('title', 'Listado de Cargos')

@section('paneladmin')
    class="active"
@endsection

@section('content')
<div class="content">
  <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header" data-background-color="purple">
              <h4 class="title">Listado de Cargos</h4>
            </div>
            <div class="card-content">
              <!--  Inicio del contenido -->

              

            <!--  Inicio del contenido -->

<!--  -->
<a href="{{ route('carges.create') }}" class="btn btn-xs btn-primary">Registrar Cargo</a>
 @include('flash::message')
<div class="row margin-top-12">
  <div class="col-lg-12">
    <div class="card">
      <!-- CONTENT -->

      <table class="table table-striped table-responsive">
          <thead>
              <th>ID</th>
              <th>Cargo</th>
              <th>Accion</th>
          </thead>
          
          <tbody>
              @foreach($carges as $carge)
                  <tr>
                    <td>{{ $carge->id }}</td>
                    <td>{{ $carge->name }}</td>
                    <td>
                      <a href="{{ route('carges.edit', $carge->id) }}" class="fa fa-pencil"> </a> | <a href="{{ route('admin.carges.destroy', $carge->id) }}" onclick="return confirm('Segruro deseas Borrarlo')" class="fa fa-trash"></a>
                    </td>
                  </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-center">
              {!! $carges->render() !!}
        </div>
        
    <!-- /CONTENT -->
    </div>
  </div>
</div>
<!-- -->
</div>
</div>
</div>
</div>
</div>
</div>

@endsection