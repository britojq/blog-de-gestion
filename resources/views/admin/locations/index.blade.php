@extends('admin.template.main')

@section('title', 'Listado de Ubicaciones')

@section('paneladmin')
    class="active"
@endsection

@section('content')
<div class="content">
  <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header" data-background-color="purple">
              <h4 class="title">Listado de Ubicaciones</h4>
            </div>
            <div class="card-content">
              <!--  Inicio del contenido -->

              <a href="{{ route('locations.create') }}" class="btn btn-xs btn-primary">Registrar Ubicacion</a>
               @include('flash::message')

            <!--  Inicio del contenido -->
  <!--  Inicio del contenido -->
    
    <div class="row margin-top-12">
  <div class="col-lg-12">
    <div class="card">
      <!-- CONTENT -->
      <table class="table table-striped">
          <!-- Encabezado de la Tabla -->
          <thead>
              <th>ID</th>
              <th>Ubicacion</th>
              <th>Accion</th>
          </thead>
          
          <!-- Cuerpo de la Tabla -->
          <tbody>
              @foreach($locations as $location)
                  <tr>
                    <td>{{ $location->id }}</td>
                    <td>{{ $location->name }}</td>
                    <td>
                      <!-- Botones Editar / Borrar -->
                      <a href="{{ route('locations.edit', $location->id) }}" class="fa fa-pencil"> </a> | <a href="{{ route('admin.locations.destroy', $location->id) }}" onclick="return confirm('Segruro deseas Borrarlo')" class="fa fa-trash"></a>
                    </td>
                  </tr>
                @endforeach
            </tbody>
        </table>
        <div class="text-center">
              {!! $locations->render() !!}
        </div>
    <!-- /CONTENT -->
    </div>
  </div>
</div>
<!-- -->
</div>
</div>
</div>
</div>
</div>
</div>

@endsection