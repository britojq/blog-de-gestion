@extends('admin.template.mainfullcalender')
@section('calender')
    class="active"
@endsection 
@section('title', 'Calendario')
@section('content')
	@if (session('status'))
    	<div class="alert alert-success">
        	{{ session('status') }}
    	</div>
    @endif

    <div class="content">
  <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header" data-background-color="purple">
              <h4 class="title">Listado de Permisos</h4>
            </div>
            <div class="card-content">
              <!--  Inicio del contenido -->
                {!! $calendar->calendar() !!}
                {!! $calendar->script() !!}
              

                          


@endsection