@extends('front.template.main')

@section('title', $article->title)

@section('content')
	
	
	<div class="row">
		<div class="col-md-8">
			<div class="card">
				<div class="card-body">
					<div class="panel panel-primary">
		  				<div class="panel-heading">
		  					<h3 class="title-from left"> {{ $article->title }}</h3>
		  				</div>
							
						<div class="panel-body">
							{!! $article->content !!}
						</div>

						<div class="panel-footer">
							<div class="row">
								<div class="col-sm-4 pull-left">
									Autor: {{ $article->user->name }}
								</div>
								
								<div class="col-sm-4 pull-right">
								Agregado: {{ $article->created_at->diffForHumans() }}
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-4 aside">
			@include('front.partials.aside')
		</div>
	
	</div>

  

@endsection