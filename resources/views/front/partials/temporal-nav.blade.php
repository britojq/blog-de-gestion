<nav class="navbar navbar-default  navbar-fixed-top" color-on-scroll="200">
        <!-- if you want to keep the navbar hidden you can add this class to the navbar "navbar-burger"-->
        <div class="container">
            <div class="navbar-header">
                <button id="menu-toggle" type="button" class="navbar-toggle" data-toggle="collapse" data-target="#">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar bar1"></span>
                    <span class="icon-bar bar2"></span>
                    <span class="icon-bar bar3"></span>
                </button>
                <a href="/" class="navbar-brand">
                    <i class="fa fa-home"></i>
                </a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right navbar-uppercase">
                    
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        @guest
                            <i class="fa fa-user-o"></i>
                        @else
                            <i class="fa fa-user-o"></i>
                            <!-- <i class="fa fa-user-o">{{ Auth::user()->name }}|{{ Auth::user()->lastname }}</i> -->
                        @endguest
                        </a>
                        <ul class="dropdown-menu dropdown-danger">
                        @guest
                            <li>
                                <a href="{{ route('login') }}"><i class="fa fa-user-circle"></i> Iniciar Sesion</a>
                            </li>
                            <li>
                                <a href="{{ route('register') }}"><i class="fa fa-vcard-o"></i> Registrarse</a>
                            </li>
                         @else
                            <li>
                                <a href="{{ route('admin.index') }}"><i class="fa fa-wrench"></i> Panel Admin</a>
                            </li>
                            <li>
                                 <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Salir
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                            </li>
                        </ul>
                        @endguest
                    </li>
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
    </nav>