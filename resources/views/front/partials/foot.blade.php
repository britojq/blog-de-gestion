<!-- Footer -->
	<footer class="footer footer-color-black" data-color="black">
        <div class="container">
            
            <hr>
            <div class="copyright">
                 © <script> document.write(new Date().getFullYear()) </script> Sitio diseñado por Jose A. Brito
            </div>
        </div>
    </footer>
    