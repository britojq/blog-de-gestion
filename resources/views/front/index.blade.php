
@extends('front.template.main')
<!-- @section('title', 'Ultimos Articulos') -->
@section('content')
<h2 class="title-front left">{{ trans('app.title_last_articles') }}</h2>
<div class="row">
	<div class="col-md-8">
		<div class="row">
			
			@foreach ($articles as $article)
			<div class="card">
			  <div class="card-body">
			    
			  
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">
					    <a href="{{ route('front.view.article', $article->id)}}">
						<h6 class="text-center">{{ $article->title }}</h6>
					</a>
					</div>
					<a href="{{ route('front.view.article', $article->id)}}" class="thumbnail" >
					@foreach($article->images as $image)
						<center><img class="img-responsive img-article" style="width:350px;height:150px;" src="{{ asset('images/articles/' . $image->name) }}" alt="..."></center>
					@endforeach
					</a>
					

					<div class="panel-footer">
						<h6>
						<i class="fa fa-holder-open-o"></i> <a href="{{ route('front.search.category', $article->category->name) }}">Categoria: {{ $article->category->name }} </a>
						<!-- 
						<div class="pull-right">
							<i class="fa fa-clock-o">Agregado: {{ $article->created_at->diffForHumans() }}  </i>
						</div>-->
						</h6>
					</div>
				</div>
			</div>
			</div>
			</div>
			@endforeach
			
		</div>	
		<center>{!! $articles->render () !!}</center>

	</div>

	<div class="col-md-4 aside">
		@include('front.partials.aside')
	</div>


</div>

@endsection