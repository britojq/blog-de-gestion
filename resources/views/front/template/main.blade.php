<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

  <head>  
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', 'Default') | Sistema </title>  
    <link rel="stylesheet" href="{{asset('plugins/chosen/chosen.css') }}">
    <link rel="stylesheet" href="{{asset('plugins/trumbowyg/ui/trumbowyg.css') }}">
    <!-- <link href="{{ asset('plugins/css/app.css') }}" rel="stylesheet"> -->
    <link href="{{ asset('plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
    
  </head>

  <body>
    <!-- Menu de Navegacion -->
    @include('front.partials.nav')
    <!-- Fin Menu de Navegacion -->
    <!-- Inicio Panel  Central de Contenido -->       
    </div>
        <div class="container">
            <div class="row">
                <div class="col-md-14">
                    
                            <section>
                               @yield('content')
                            </section>

                              <script src="{{ asset('plugins/jquery/js/jquery-3.2.1.js') }}"></script>
                              <script src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}"></script>
                              <script src="{{ asset('plugins/chosen/chosen.jquery.js') }}"></script>
                              <script src="{{ asset('plugins/trumbowyg/trumbowyg.js') }}"></script>

                              <script>
                                    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
                              </script>
                              @yield('js')

                              <!-- Fin Contenido del panel  -->
             
           </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Fin Panel  -->

    <!-- Pie de la Pagina -->
    @include('front.partials.foot')
    <!-- Fin Pie de la Pagina --> 
  </body>
</html>
