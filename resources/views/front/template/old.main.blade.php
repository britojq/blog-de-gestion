<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>  
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', 'Default') | Sistema </title>
    <!--<title>{{ config('app.name', 'Sistema - Bienvenido :') }}</title>-->

    <!-- Styles -->
   <!-- <link rel="stylesheet" href="{{asset('plugins/journal/bootstrap.css') }}">
    <link rel="stylesheet" href="{{asset('plugins/journal/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{asset('plugins/journal/_bootswatch.css') }}">
    <link rel="stylesheet" href="{{asset('plugins/journal/_variables.css') }}">-->
    

    
    <link rel="stylesheet" href="{{asset('plugins/chosen/chosen.css') }}">
    <link rel="stylesheet" href="{{asset('plugins/trumbowyg/ui/trumbowyg.css') }}">
    <link href="{{ asset('plugins/css/app.css') }}" rel="stylesheet"> 
</head>

<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                       <!--  @yield('title', 'Default') --> Bienvenido |
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Ingresar</a></li>
                           <!--  <li><a href="{{ route('register') }}">Registrarse</a></li> -->
                        @else
                                <ul class="nav navbar-nav">
                                  @if(Auth::user()->admin())
                                    <li><a href="{{ route('users.index') }}" >| Usuarios</a></li>
                                  @endif
                                  <li><a href="{{ route('categories.index') }}">| Categorias</a></li>
                                  <li><a href="{{ route('articles.index') }}">| Articulos</a></li>
                                  <li><a href="{{ route('admin.image.index') }}">| Imagenes</a></li>
                                  <li><a href="{{ route('tags.index') }}">| Tags</a></li>
                                </ul>

                               <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Salir
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

 <!-- Inicio Panel  Central de Contenido -->       
    </div>
        <div class="container">
            <div class="row">
                <div class="col-md-14">
                    <div class="panel panel-primary">
                        <div class="panel-heading">@yield('title', 'Default')</div>
                          <div class="panel-body">
                            <section>
                               @include('flash::message')
                               @include('admin.template.partials.errors')
                               <!-- Contenido a mostrar en el panel -->
                               @yield('content')
                            </section>

                              <script src="{{ asset('plugins/jquery/js/jquery-3.2.1.js') }}"></script>
                              <script src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}"></script>
                              <script src="{{ asset('plugins/chosen/chosen.jquery.js') }}"></script>
                              <script src="{{ asset('plugins/trumbowyg/trumbowyg.js') }}"></script>

                              <script>
                                    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
                              </script>
                              @yield('js')

                              <!-- Fin Contenido del panel  -->
              </div>
           </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Fin Panel  -->

</body>
</html>
