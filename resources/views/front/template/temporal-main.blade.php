<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>@yield('title', 'Default') | Sistema </title> 
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

    <link href="{{asset('plugins/front/assets/css/bootstrap.css') }}" rel="stylesheet" />
    <link href="{{asset('plugins/front/assets/css/gaia.css') }}" rel="stylesheet"/>

    <!--     Fonts and icons     -->
    <link href='https://fonts.googleapis.com/css?family=Cambo|Poppins:400,600' rel='stylesheet' type='text/css'>
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/fonts/pe-icon-7-stroke.css" rel="stylesheet">
</head>

<body>
    <div class="page-header header-filter clear-filter" data-parallax="true" style="background-image: url('/front/plugins/assets/img/entrada.jpg');">
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
          
          </div>
        </div>
      </div>
    </div>
    <!-- Menu de Navegacion -->
    @include('front.partials.nav')
    <!-- Fin Menu de Navegacion -->
    <!-- Inicio Panel  Central de Contenido -->  
        <div class="container">
            <div class="row">
                <div class="col-md-14">  
                    <section>
                        @yield('content')
                    </section>
                      <!-- Fin Contenido del panel  -->
                </div>
            </div>
        </div>
    


  <!-- Fin Panel  -->
    <script src="{{ asset('plugins/front/assets/js/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/front/assets/js/bootstrap.js') }}" type="text/javascript"></script>
                            
    <!--  js library for devices recognition -->
    <script type="text/javascript" src="{{ asset('plugins/front/assets/js/modernizr.js') }}"></script>

    <!--   file where we handle all the script from the Gaia - Bootstrap Template   -->
    <script type="text/javascript" src="{{ asset('plugins/front/assets/js/gaia.js') }}"></script>

                              

    <script>
        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    </script>
    <!-- Pie de la Pagina -->
    @include('front.partials.foot')
    <!-- Fin Pie de la Pagina --> 
  </body>
</html>